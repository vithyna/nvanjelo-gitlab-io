#!/bin/bash
#looks for \ characters (from latex) in the front matter of .md outputs and (for now) removes them.
#also changes the example.org urls to the correct directory references
#probably in the future we can have it properly fix them into readable titles and summaries, but for now, I just need the \ to be gone!
cd /home/anarendran/Documents/doks_install/my-doks-site/content/en/documentation/code/Classes;
for file in *.md; do
	for i in {1..10}; do
		string=$(sed -n -e "$i"p $file)
		if [[ $string == *"title"* || $string == *"description"* ]]; then
			sed -i -e ''$i's/\\/avoid/g' $file
		fi
	done
	#sed -i -e 's/http:\/\/example.org/\/documentation\/code/g' $file
done
#mv _index.md index_classes.md


cd ../Modules

for file in *.md; do
        for i in {1..10}; do
                string=$(sed -n -e "$i"p $file)
                if [[ $string == *"title"* || $string == *"description"* ]]; then
                        sed -i -e ''$i's/\\/avoid/g' $file
                fi
        done
#	sed -i -e 's/http:\/\/example.org/\/documentation\/code/g' $file
done
#mv _index.md index_modules.md

#cd ../Files

#for file in *.md; do
#	sed -i -e 's/http:\/\/example.org/\/documentation\/code/g' $file
#done
#mv _index.md index_files.md


#cd ../Namespaces

#for file in *.md; do
#        sed -i -e 's/http:\/\/example.org/\/documentation\/code/g' $file
#done
#mv _index.md index_namespaces.md

#cd ../Pages

#for file in *.md; do
#        sed -i -e 's/http:\/\/example.org/\/documentation\/code/g' $file
#done
#mv _index.md index_pages.md

cd ../
rm -f _index.md
rm -rf Examples

cd ../python/Classes
for i in {1..10}; do
      string=$(sed -n -e "$i"p index_classespy.md)
      if [[ $string == *"title"* ]]; then
           sed -i -e ''$i's/Classes/Classpy/g' index_classespy.md
      fi
done

cd ../Examples
for i in {1..10}; do
      string=$(sed -n -e "$i"p index_examplespy.md)
      if [[ $string == *"title"* ]]; then
           sed -i -e ''$i's/Examples/Examplepy/g' index_examplespy.md
      fi
done



cd ../Files
for i in {1..10}; do
      string=$(sed -n -e "$i"p index_filespy.md)
      if [[ $string == *"title"* ]]; then
           sed -i -e ''$i's/Files/Filepy/g' index_filespy.md
      fi
done


cd ../Modules
for i in {1..10}; do
      string=$(sed -n -e "$i"p index_groupspy.md)
      if [[ $string == *"title"* ]]; then
           sed -i -e ''$i's/Modules/Modulepy/g' index_groupspy.md
      fi
done


cd ../Namespaces
for i in {1..10}; do
      string=$(sed -n -e "$i"p index_namespacespy.md)
      if [[ $string == *"title"* ]]; then
           sed -i -e ''$i's/Namespaces/Namespacepy/g' index_namespacespy.md
      fi
done



cd ../Pages
for i in {1..10}; do
      string=$(sed -n -e "$i"p index_pagespy.md)
      if [[ $string == *"title"* ]]; then
           sed -i -e ''$i's/Pages/Pagepy/g' index_pagespy.md
      fi
done


cd ../../yoda_py/Classes
for i in {1..10}; do
      string=$(sed -n -e "$i"p index_classesypy.md)
      if [[ $string == *"title"* ]]; then
           sed -i -e ''$i's/Classes/Classypy/g' index_classesypy.md
      fi
done

cd ../Examples
for i in {1..10}; do
      string=$(sed -n -e "$i"p index_examplesypy.md)
      if [[ $string == *"title"* ]]; then
           sed -i -e ''$i's/Examples/Exampleypy/g' index_examplesypy.md
      fi
done



cd ../Files
for i in {1..10}; do
      string=$(sed -n -e "$i"p index_filesypy.md)
      if [[ $string == *"title"* ]]; then
           sed -i -e ''$i's/Files/Fileypy/g' index_filesypy.md
      fi
done


cd ../Modules
for i in {1..10}; do
      string=$(sed -n -e "$i"p index_groupsypy.md)
      if [[ $string == *"title"* ]]; then
           sed -i -e ''$i's/Modules/Moduleypy/g' index_groupsypy.md
      fi
done


cd ../Namespaces
for i in {1..10}; do
      string=$(sed -n -e "$i"p index_namespacesypy.md)
      if [[ $string == *"title"* ]]; then
           sed -i -e ''$i's/Namespaces/Namespaceypy/g' index_namespacesypy.md
      fi
done



cd ../Pages
for i in {1..10}; do
      string=$(sed -n -e "$i"p index_pagesypy.md)
      if [[ $string == *"title"* ]]; then
           sed -i -e ''$i's/Pages/Pageypy/g' index_pagesypy.md
      fi
done





#other changes needed are:
#	possibly rename _index to something else in all the directories?
#	add front matter to the rivet documentation main page _index file
#	rewtite the aforementioned _index file now know as index_mainpage. So of the doxybook imports, _index can just be deleted really. add an rm later

