#!/bin/bash

#cleans up all doxybook generated content for c++ docs (for now)

rm -rf ../content/en/documentation/code/Classes/*
rm -rf ../content/en/documentation/code/Files/*
rm -rf ../content/en/documentation/code/images/*
rm -rf ../content/en/documentation/code/Modules/*
rm -rf ../content/en/documentation/code/Namespaces/*
rm -rf ../content/en/documentation/code/Pages/*

#now adding python cleanup too, but will need to be changed if adding a mainpage later.
rm -rf ../content/en/documentation/python/*
