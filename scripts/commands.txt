~/Documents/doxybook_binary/bin/doxybook2 --input ~/Documents/doxygen_trials/xml --output ~/Documents/doks_install/my-doks-site/content/en/documentation/code --config ~/Documents/doks_install/my-doks-site/scripts/config.json --templates ~/Documents/doks_install/my-doks-site/scripts/templates

git push --set-upstream origin local_testing

git push --set-upstream origin master

~/Documents/doxybook_binary/bin/doxybook2 --input ~/Documents/doxygen_trials/rivetpy/xml --output ~/Documents/doks_install/my-doks-site/content/en/documentation/python --config ~/Documents/doks_install/my-doks-site/scripts/configpy.json --templates ~/Documents/doks_install/my-doks-site/scripts/templatespy

~/Documents/doxybook_binary/bin/doxybook2 --input ~/Documents/doxygen_trials/yodapy/xml --output ~/Documents/doks_install/my-doks-site/content/en/documentation/yoda_py --config ~/Documents/doks_install/my-doks-site/scripts/configyodapy.json --templates ~/Documents/doks_install/my-doks-site/scripts/templatesyodapy
