---

title: 'file /home/anarendran/Documents/temp/rivet/include/Rivet/Math/Vectors.hh'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/include/Rivet/Math/Vectors.hh






## Source code

```cpp
#ifndef RIVET_MATH_VECTORS
#define RIVET_MATH_VECTORS

#include "Rivet/Math/MathConstants.hh"
#include "Rivet/Math/VectorN.hh"
#include "Rivet/Math/Vector2.hh"
#include "Rivet/Math/Vector3.hh"
#include "Rivet/Math/Vector4.hh"

#endif
```


-------------------------------

Updated on 2022-08-07 at 20:17:18 +0100
