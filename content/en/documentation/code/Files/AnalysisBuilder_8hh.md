---

title: 'file /home/anarendran/Documents/temp/rivet/include/Rivet/AnalysisBuilder.hh'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/include/Rivet/AnalysisBuilder.hh



## Namespaces

| Name           |
| -------------- |
| **[Rivet](/documentation/code/namespaces/namespacerivet/)**  |




## Source code

```cpp
// -*- C++ -*-
#ifndef RIVET_AnalysisBuilder_HH
#define RIVET_AnalysisBuilder_HH

#include "Rivet/Config/RivetCommon.hh"
#include "Rivet/AnalysisLoader.hh"
#include "Rivet/Tools/Logging.hh"

namespace Rivet {


  // Forward declaration
  class Analysis;



  class AnalysisBuilderBase {
  public:

    AnalysisBuilderBase() = default;

    AnalysisBuilderBase(const string& alias)
      : _alias(alias) {   }

    virtual ~AnalysisBuilderBase() = default;

    virtual unique_ptr<Analysis> mkAnalysis() const = 0;

    string name() const {
      auto a = mkAnalysis();
      return a->name();
    }

    const string& alias() const {
      return _alias;
    }

  protected:

    void _register() {
      AnalysisLoader::_registerBuilder(this);
    }

  private:

    string _alias;

  };


  template <typename T>
  class AnalysisBuilder : public AnalysisBuilderBase {
  public:

    AnalysisBuilder() {
      _register();
    }

    AnalysisBuilder(const string& alias)
      : AnalysisBuilderBase(alias)
    {
      _register();
    }

    unique_ptr<Analysis> mkAnalysis() const {
      return unique_ptr<T>(new T);
    }

  };


}

#endif
```


-------------------------------

Updated on 2022-08-07 at 20:17:18 +0100
