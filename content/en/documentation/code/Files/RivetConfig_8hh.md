---

title: 'file /home/anarendran/Documents/temp/rivet/include/Rivet/Config/RivetConfig.hh'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/include/Rivet/Config/RivetConfig.hh



## Defines

|                | Name           |
| -------------- | -------------- |
|  | **[RIVET_BUGREPORT](/documentation/code/files/rivetconfig_8hh/#define-rivet-bugreport)**  |
|  | **[RIVET_NAME](/documentation/code/files/rivetconfig_8hh/#define-rivet-name)**  |
|  | **[RIVET_STRING](/documentation/code/files/rivetconfig_8hh/#define-rivet-string)**  |
|  | **[RIVET_TARNAME](/documentation/code/files/rivetconfig_8hh/#define-rivet-tarname)**  |
|  | **[RIVET_VERSION](/documentation/code/files/rivetconfig_8hh/#define-rivet-version)**  |
|  | **[RIVET_VERSION_CODE](/documentation/code/files/rivetconfig_8hh/#define-rivet-version-code)**  |
|  | **[RIVET_ENABLE_HEPMC_3](/documentation/code/files/rivetconfig_8hh/#define-rivet-enable-hepmc-3)**  |
|  | **[RIVET_SUPPORTS_MERGING_IN_MEMORY](/documentation/code/files/rivetconfig_8hh/#define-rivet-supports-merging-in-memory)**  |




## Macros Documentation

### define RIVET_BUGREPORT

```cpp
#define RIVET_BUGREPORT "rivet@projects.hepforge.org"
```


### define RIVET_NAME

```cpp
#define RIVET_NAME "Rivet"
```


### define RIVET_STRING

```cpp
#define RIVET_STRING "Rivet 3.1.6"
```


### define RIVET_TARNAME

```cpp
#define RIVET_TARNAME "Rivet"
```


### define RIVET_VERSION

```cpp
#define RIVET_VERSION "3.1.6"
```


### define RIVET_VERSION_CODE

```cpp
#define RIVET_VERSION_CODE 30106
```


### define RIVET_ENABLE_HEPMC_3

```cpp
#define RIVET_ENABLE_HEPMC_3 1
```


### define RIVET_SUPPORTS_MERGING_IN_MEMORY

```cpp
#define RIVET_SUPPORTS_MERGING_IN_MEMORY 1
```


## Source code

```cpp
/* include/Rivet/Config/RivetConfig.hh.  Generated from RivetConfig.hh.in by configure.  */
#ifndef RIVET_CONFIG_RIVETCONFIG_HH
#define RIVET_CONFIG_RIVETCONFIG_HH


/* Define to the address where bug reports for this package should be sent. */
#define RIVET_BUGREPORT "rivet@projects.hepforge.org"

/* Define to the full name of this package. */
#define RIVET_NAME "Rivet"

/* Define to the full name and version of this package. */
#define RIVET_STRING "Rivet 3.1.6"

/* Define to the one symbol short name of this package. */
#define RIVET_TARNAME "Rivet"

/* Define to the version of this package. */
#define RIVET_VERSION "3.1.6"

/* Define to the version of this package as a comparable integer. */
#define RIVET_VERSION_CODE 30106

/* Define if version 3 of HepMC is used. */
#define RIVET_ENABLE_HEPMC_3 1

/* Define if version 2.06.10 of HepMC is used. */
/* #undef RIVET_ENABLE_HEPMC_20610 */

/* Define if Rivet supports in-memory merging of AnalysisHandlers. */
#define RIVET_SUPPORTS_MERGING_IN_MEMORY 1

#endif
```


-------------------------------

Updated on 2022-08-07 at 20:17:18 +0100
