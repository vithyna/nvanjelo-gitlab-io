---

title: 'file /home/anarendran/Documents/temp/rivet/include/Rivet/Math/Matrices.hh'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/include/Rivet/Math/Matrices.hh






## Source code

```cpp
#ifndef RIVET_MATH_MATRICES
#define RIVET_MATH_MATRICES

#include "Rivet/Math/MathConstants.hh"
#include "Rivet/Math/MatrixN.hh"
#include "Rivet/Math/Matrix3.hh"
#include "Rivet/Math/LorentzTrans.hh"

#endif
```


-------------------------------

Updated on 2022-08-07 at 20:17:18 +0100
