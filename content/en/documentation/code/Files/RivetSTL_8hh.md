---

title: 'file /home/anarendran/Documents/temp/rivet/include/Rivet/Tools/RivetSTL.hh'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/include/Rivet/Tools/RivetSTL.hh



## Namespaces

| Name           |
| -------------- |
| **[Rivet](/documentation/code/namespaces/namespacerivet/)**  |
| **[std](/documentation/code/namespaces/namespacestd/)** <br>STL namespace.  |




## Source code

```cpp
#ifndef RIVET_RivetSTL_HH
#define RIVET_RivetSTL_HH

#include <string>
#include <array>
#include <vector>
#include <list>
#include <set>
#include <map>
#include <memory>
#include <functional>
#include <ostream>
#include <sstream>
// #include <tuple>
// #include <utility>
// #include <algorithm>
// #include <cassert>
// #include <typeinfo>
// #include <iomanip>
// #include <cmath>
// #include <limits>

namespace Rivet {


  // using namespace std;
  using std::string;
  using std::to_string;

  using std::array;
  using std::vector;
  using std::list;
  using std::set;
  using std::multiset;
  using std::map;
  using std::multimap;
  using std::pair;
  using std::make_pair;

  using std::unique_ptr;
  using std::shared_ptr;
  using std::make_shared;
  using std::dynamic_pointer_cast;

  using std::initializer_list;

  using std::function;



  template<typename T>
  inline std::ostream& operator<<(std::ostream& os, const std::vector<T>& vec) {
    os << "[ ";
    for (size_t i=0; i<vec.size(); ++i) {
      os << vec[i] << " ";
    }
    os << "]";
    return os;
  }

  template<typename T>
  inline std::ostream& operator<<(std::ostream& os, const std::list<T>& vec) {
    os << "[ ";
    for (size_t i=0; i<vec.size(); ++i) {
      os << vec[i] << " ";
    }
    os << "]";
    return os;
  }



  typedef vector<std::string> strings;
  typedef vector<double> doubles;
  typedef vector<float> floats;
  typedef vector<int> ints;




  inline bool contains(const std::string& s, const std::string& sub) {
    return s.find(sub) != string::npos;
  }

  template <typename T>
  inline bool contains(const std::initializer_list<T>& il, const T& x) {
    return find(begin(il), end(il), x) != end(il);
  }

  template <typename T>
  inline bool contains(const std::vector<T>& v, const T& x) {
    return find(begin(v), end(v), x) != end(v);
  }

  template <typename T>
  inline bool contains(const std::list<T>& l, const T& x) {
    return find(begin(l), end(l), x) != end(l);
  }

  template <typename T>
  inline bool contains(const std::set<T>& s, const T& x) {
    return find(begin(s), end(s), x) != end(s);
  }

  template <typename K, typename T>
  inline bool has_key(const std::map<K, T>& m, const K& key) {
    return m.find(key) != end(m);
  }

  template <typename K, typename T>
  inline bool has_value(const std::map<K, T>& m, const T& val) {
    for (typename std::map<K,T>::const_iterator it = begin(m); it != end(m); ++it) {
      if (it->second == val) return true;
    }
    return false;
  }



}

namespace std {




  template <typename T>
  inline void operator += (std::vector<T>& v, const T& x) { v.push_back(x); }

  template <typename T>
  inline void operator += (std::vector<T>& v1, const std::vector<T>& v2) {
    for (const auto& x : v2) v1.push_back(x);
  }

  template <typename T>
  inline std::vector<T> operator + (const std::vector<T>& v1, const std::vector<T>& v2) {
    std::vector<T> rtn(v1);
    rtn += v2;
    return rtn;
  }


  template <typename T>
  inline void operator += (std::set<T>& s1, const std::set<T>& s2) {
    for (const auto& x : s2) s1.insert(x);
  }

  template <typename T>
  inline std::set<T> operator + (const std::set<T>& s1, const std::set<T>& s2) {
    std::set<T> rtn(s1);
    rtn += s2;
    return rtn;
  }





  template<typename T, typename... U>
  inline uintptr_t get_address(std::function<T(U...)> f) {
    typedef T(fnType)(U...);
    fnType ** fnPointer = f.template target<fnType*>();
    return (fnPointer != nullptr) ? reinterpret_cast<uintptr_t>(*fnPointer) : 0;
  }



}

#endif
```


-------------------------------

Updated on 2022-08-07 at 20:17:18 +0100
