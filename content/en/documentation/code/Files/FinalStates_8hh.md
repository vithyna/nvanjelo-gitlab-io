---

title: 'file /home/anarendran/Documents/temp/rivet/include/Rivet/Projections/FinalStates.hh'
description: "Convenience include of all FinalState projection headers. "

---

# /home/anarendran/Documents/temp/rivet/include/Rivet/Projections/FinalStates.hh

Convenience include of all FinalState projection headers. 




## Source code

```cpp
// -*- C++ -*-
#ifndef RIVET_FinalStates_HH
#define RIVET_FinalStates_HH


#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/ChargedFinalState.hh"
#include "Rivet/Projections/NeutralFinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/NonPromptFinalState.hh"
#include "Rivet/Projections/VisibleFinalState.hh"

#endif
```


-------------------------------

Updated on 2022-08-07 at 20:17:18 +0100
