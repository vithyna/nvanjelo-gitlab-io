---

title: 'file /home/anarendran/Documents/temp/rivet/include/Rivet/Projections/UnstableFinalState.hh'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/include/Rivet/Projections/UnstableFinalState.hh






## Source code

```cpp
// -*- C++ -*-
#ifndef RIVET_UnstableFinalState_HH
#define RIVET_UnstableFinalState_HH

#include "Rivet/Projections/UnstableParticles.hh"
#pragma message "UnstableFinalState.hh is deprecated. Please use UnstableParticles.hh instead"

#endif
```


-------------------------------

Updated on 2022-08-07 at 20:17:18 +0100
