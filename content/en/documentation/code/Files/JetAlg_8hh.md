---

title: 'file /home/anarendran/Documents/temp/rivet/include/Rivet/Projections/JetAlg.hh'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/include/Rivet/Projections/JetAlg.hh



## Namespaces

| Name           |
| -------------- |
| **[Rivet](/documentation/code/namespaces/namespacerivet/)**  |




## Source code

```cpp
// -*- C++ -*-
#ifndef RIVET_JetAlg_HH
#define RIVET_JetAlg_HH

#include "Rivet/Projections/JetFinder.hh"

namespace Rivet {


  using JetAlg = JetFinder;


}

#endif
```


-------------------------------

Updated on 2022-08-07 at 20:17:18 +0100
