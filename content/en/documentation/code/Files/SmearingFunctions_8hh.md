---

title: 'file /home/anarendran/Documents/temp/rivet/include/Rivet/Tools/SmearingFunctions.hh'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/include/Rivet/Tools/SmearingFunctions.hh






## Source code

```cpp
// -*- C++ -*-
#ifndef RIVET_SmearingFunctions_HH
#define RIVET_SmearingFunctions_HH

#include "Rivet/Tools/MomentumSmearingFunctions.hh"
#include "Rivet/Tools/ParticleSmearingFunctions.hh"
#include "Rivet/Tools/JetSmearingFunctions.hh"
#include "Rivet/Tools/ExptSmearingFunctions.hh"

#endif
```


-------------------------------

Updated on 2022-08-07 at 20:17:18 +0100
