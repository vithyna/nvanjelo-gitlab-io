---

title: 'file /home/anarendran/Documents/temp/rivet/include/Rivet/Projections/DirectFinalState.hh'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/include/Rivet/Projections/DirectFinalState.hh



## Namespaces

| Name           |
| -------------- |
| **[Rivet](/documentation/code/namespaces/namespacerivet/)**  |




## Source code

```cpp
// -*- C++ -*-
#ifndef RIVET_DirectFinalState_HH
#define RIVET_DirectFinalState_HH

#include "Rivet/Projections/PromptFinalState.hh"

namespace Rivet {

  using DirectFinalState = PromptFinalState;

}

#endif
```


-------------------------------

Updated on 2022-08-07 at 20:17:18 +0100
