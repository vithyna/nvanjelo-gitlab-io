---

title: 'file /home/anarendran/Documents/temp/rivet/include/Rivet/Config/DummyConfig.hh'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/include/Rivet/Config/DummyConfig.hh



## Defines

|                | Name           |
| -------------- | -------------- |
|  | **[HAVE_BACKTRACE](/documentation/code/files/dummyconfig_8hh/#define-have-backtrace)**  |
|  | **[HAVE_CXX14](/documentation/code/files/dummyconfig_8hh/#define-have-cxx14)**  |
|  | **[HAVE_DECL_STRERROR_R](/documentation/code/files/dummyconfig_8hh/#define-have-decl-strerror-r)**  |
|  | **[HAVE_DLFCN_H](/documentation/code/files/dummyconfig_8hh/#define-have-dlfcn-h)**  |
|  | **[HAVE_EXECINFO_H](/documentation/code/files/dummyconfig_8hh/#define-have-execinfo-h)**  |
|  | **[HAVE_INTTYPES_H](/documentation/code/files/dummyconfig_8hh/#define-have-inttypes-h)**  |
|  | **[HAVE_LIBZ](/documentation/code/files/dummyconfig_8hh/#define-have-libz)**  |
|  | **[HAVE_PYTHON](/documentation/code/files/dummyconfig_8hh/#define-have-python)**  |
|  | **[HAVE_STDINT_H](/documentation/code/files/dummyconfig_8hh/#define-have-stdint-h)**  |
|  | **[HAVE_STDIO_H](/documentation/code/files/dummyconfig_8hh/#define-have-stdio-h)**  |
|  | **[HAVE_STDLIB_H](/documentation/code/files/dummyconfig_8hh/#define-have-stdlib-h)**  |
|  | **[HAVE_STRERROR_R](/documentation/code/files/dummyconfig_8hh/#define-have-strerror-r)**  |
|  | **[HAVE_STRINGS_H](/documentation/code/files/dummyconfig_8hh/#define-have-strings-h)**  |
|  | **[HAVE_STRING_H](/documentation/code/files/dummyconfig_8hh/#define-have-string-h)**  |
|  | **[HAVE_SYS_STAT_H](/documentation/code/files/dummyconfig_8hh/#define-have-sys-stat-h)**  |
|  | **[HAVE_SYS_TYPES_H](/documentation/code/files/dummyconfig_8hh/#define-have-sys-types-h)**  |
|  | **[HAVE_UNISTD_H](/documentation/code/files/dummyconfig_8hh/#define-have-unistd-h)**  |
|  | **[LT_OBJDIR](/documentation/code/files/dummyconfig_8hh/#define-lt-objdir)**  |
|  | **[PACKAGE](/documentation/code/files/dummyconfig_8hh/#define-package)**  |
|  | **[PACKAGE_BUGREPORT](/documentation/code/files/dummyconfig_8hh/#define-package-bugreport)**  |
|  | **[PACKAGE_NAME](/documentation/code/files/dummyconfig_8hh/#define-package-name)**  |
|  | **[PACKAGE_STRING](/documentation/code/files/dummyconfig_8hh/#define-package-string)**  |
|  | **[PACKAGE_TARNAME](/documentation/code/files/dummyconfig_8hh/#define-package-tarname)**  |
|  | **[PACKAGE_URL](/documentation/code/files/dummyconfig_8hh/#define-package-url)**  |
|  | **[PACKAGE_VERSION](/documentation/code/files/dummyconfig_8hh/#define-package-version)**  |
|  | **[RIVET_BUGREPORT](/documentation/code/files/dummyconfig_8hh/#define-rivet-bugreport)**  |
|  | **[RIVET_ENABLE_HEPMC_3](/documentation/code/files/dummyconfig_8hh/#define-rivet-enable-hepmc-3)**  |
|  | **[RIVET_NAME](/documentation/code/files/dummyconfig_8hh/#define-rivet-name)**  |
|  | **[RIVET_STRING](/documentation/code/files/dummyconfig_8hh/#define-rivet-string)**  |
|  | **[RIVET_SUPPORTS_MERGING_IN_MEMORY](/documentation/code/files/dummyconfig_8hh/#define-rivet-supports-merging-in-memory)**  |
|  | **[RIVET_TARNAME](/documentation/code/files/dummyconfig_8hh/#define-rivet-tarname)**  |
|  | **[RIVET_VERSION](/documentation/code/files/dummyconfig_8hh/#define-rivet-version)**  |
|  | **[RIVET_VERSION_CODE](/documentation/code/files/dummyconfig_8hh/#define-rivet-version-code)**  |
|  | **[STDC_HEADERS](/documentation/code/files/dummyconfig_8hh/#define-stdc-headers)**  |
|  | **[STRERROR_R_CHAR_P](/documentation/code/files/dummyconfig_8hh/#define-strerror-r-char-p)**  |
|  | **[VERSION](/documentation/code/files/dummyconfig_8hh/#define-version)**  |
|  | **[backtrace_size_t](/documentation/code/files/dummyconfig_8hh/#define-backtrace-size-t)**  |




## Macros Documentation

### define HAVE_BACKTRACE

```cpp
#define HAVE_BACKTRACE 1
```


### define HAVE_CXX14

```cpp
#define HAVE_CXX14 1
```


### define HAVE_DECL_STRERROR_R

```cpp
#define HAVE_DECL_STRERROR_R 1
```


### define HAVE_DLFCN_H

```cpp
#define HAVE_DLFCN_H 1
```


### define HAVE_EXECINFO_H

```cpp
#define HAVE_EXECINFO_H 1
```


### define HAVE_INTTYPES_H

```cpp
#define HAVE_INTTYPES_H 1
```


### define HAVE_LIBZ

```cpp
#define HAVE_LIBZ 1
```


### define HAVE_PYTHON

```cpp
#define HAVE_PYTHON "3.10"
```


### define HAVE_STDINT_H

```cpp
#define HAVE_STDINT_H 1
```


### define HAVE_STDIO_H

```cpp
#define HAVE_STDIO_H 1
```


### define HAVE_STDLIB_H

```cpp
#define HAVE_STDLIB_H 1
```


### define HAVE_STRERROR_R

```cpp
#define HAVE_STRERROR_R 1
```


### define HAVE_STRINGS_H

```cpp
#define HAVE_STRINGS_H 1
```


### define HAVE_STRING_H

```cpp
#define HAVE_STRING_H 1
```


### define HAVE_SYS_STAT_H

```cpp
#define HAVE_SYS_STAT_H 1
```


### define HAVE_SYS_TYPES_H

```cpp
#define HAVE_SYS_TYPES_H 1
```


### define HAVE_UNISTD_H

```cpp
#define HAVE_UNISTD_H 1
```


### define LT_OBJDIR

```cpp
#define LT_OBJDIR ".libs/"
```


### define PACKAGE

```cpp
#define PACKAGE "Rivet"
```


### define PACKAGE_BUGREPORT

```cpp
#define PACKAGE_BUGREPORT "rivet@projects.hepforge.org"
```


### define PACKAGE_NAME

```cpp
#define PACKAGE_NAME "Rivet"
```


### define PACKAGE_STRING

```cpp
#define PACKAGE_STRING "Rivet 3.1.6"
```


### define PACKAGE_TARNAME

```cpp
#define PACKAGE_TARNAME "Rivet"
```


### define PACKAGE_URL

```cpp
#define PACKAGE_URL ""
```


### define PACKAGE_VERSION

```cpp
#define PACKAGE_VERSION "3.1.6"
```


### define RIVET_BUGREPORT

```cpp
#define RIVET_BUGREPORT "rivet@projects.hepforge.org"
```


### define RIVET_ENABLE_HEPMC_3

```cpp
#define RIVET_ENABLE_HEPMC_3 1
```


### define RIVET_NAME

```cpp
#define RIVET_NAME "Rivet"
```


### define RIVET_STRING

```cpp
#define RIVET_STRING "Rivet 3.1.6"
```


### define RIVET_SUPPORTS_MERGING_IN_MEMORY

```cpp
#define RIVET_SUPPORTS_MERGING_IN_MEMORY 1
```


### define RIVET_TARNAME

```cpp
#define RIVET_TARNAME "Rivet"
```


### define RIVET_VERSION

```cpp
#define RIVET_VERSION "3.1.6"
```


### define RIVET_VERSION_CODE

```cpp
#define RIVET_VERSION_CODE 30106
```


### define STDC_HEADERS

```cpp
#define STDC_HEADERS 1
```


### define STRERROR_R_CHAR_P

```cpp
#define STRERROR_R_CHAR_P 1
```


### define VERSION

```cpp
#define VERSION "3.1.6"
```


### define backtrace_size_t

```cpp
#define backtrace_size_t int
```


## Source code

```cpp
/* include/Rivet/Config/DummyConfig.hh.  Generated from DummyConfig.hh.in by configure.  */
/* include/Rivet/Config/DummyConfig.hh.in.  Generated from configure.ac by autoheader.  */

/* Defined if backtrace() could be fully identified. */
#define HAVE_BACKTRACE 1

/* define if the compiler supports basic C++14 syntax */
#define HAVE_CXX14 1

/* Define to 1 if you have the declaration of `strerror_r', and to 0 if you
   don't. */
#define HAVE_DECL_STRERROR_R 1

/* Define to 1 if you have the <dlfcn.h> header file. */
#define HAVE_DLFCN_H 1

/* Define to 1 if you have the <execinfo.h> header file. */
#define HAVE_EXECINFO_H 1

/* Define to 1 if you have the <inttypes.h> header file. */
#define HAVE_INTTYPES_H 1

/* Define to 1 if you have `z' library (-lz) */
#define HAVE_LIBZ 1

/* If available, contains the Python version number currently in use. */
#define HAVE_PYTHON "3.10"

/* Define to 1 if you have the <stdint.h> header file. */
#define HAVE_STDINT_H 1

/* Define to 1 if you have the <stdio.h> header file. */
#define HAVE_STDIO_H 1

/* Define to 1 if you have the <stdlib.h> header file. */
#define HAVE_STDLIB_H 1

/* Define if you have `strerror_r'. */
#define HAVE_STRERROR_R 1

/* Define to 1 if you have the <strings.h> header file. */
#define HAVE_STRINGS_H 1

/* Define to 1 if you have the <string.h> header file. */
#define HAVE_STRING_H 1

/* Define to 1 if you have the <sys/stat.h> header file. */
#define HAVE_SYS_STAT_H 1

/* Define to 1 if you have the <sys/types.h> header file. */
#define HAVE_SYS_TYPES_H 1

/* Define to 1 if you have the <unistd.h> header file. */
#define HAVE_UNISTD_H 1

/* Define to the sub-directory where libtool stores uninstalled libraries. */
#define LT_OBJDIR ".libs/"

/* Name of package */
#define PACKAGE "Rivet"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "rivet@projects.hepforge.org"

/* Define to the full name of this package. */
#define PACKAGE_NAME "Rivet"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "Rivet 3.1.6"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "Rivet"

/* Define to the home page for this package. */
#define PACKAGE_URL ""

/* Define to the version of this package. */
#define PACKAGE_VERSION "3.1.6"

/* "Rivet contact email address" */
#define RIVET_BUGREPORT "rivet@projects.hepforge.org"

/* Defined if version 2.06.10 of HepMC is used. */
/* #undef RIVET_ENABLE_HEPMC_20610 */

/* Defined if version 3 of HepMC is used. */
#define RIVET_ENABLE_HEPMC_3 1

/* "Rivet name string" */
#define RIVET_NAME "Rivet"

/* "Rivet name and version string" */
#define RIVET_STRING "Rivet 3.1.6"

/* "Rivet memory-based merging support" */
#define RIVET_SUPPORTS_MERGING_IN_MEMORY 1

/* "Rivet short name string" */
#define RIVET_TARNAME "Rivet"

/* "Rivet version string" */
#define RIVET_VERSION "3.1.6"

/* "Rivet version int" */
#define RIVET_VERSION_CODE 30106

/* Define to 1 if all of the C90 standard headers exist (not just the ones
   required in a freestanding environment). This macro is provided for
   backward compatibility; new code need not use it. */
#define STDC_HEADERS 1

/* Define to 1 if strerror_r returns char *. */
#define STRERROR_R_CHAR_P 1

/* Version number of package */
#define VERSION "3.1.6"

/* Defined to return type of backtrace(). */
#define backtrace_size_t int
```


-------------------------------

Updated on 2022-08-07 at 20:17:18 +0100
