---

title: 'file /home/anarendran/Documents/temp/rivet/include/Rivet/Projections/IndirectFinalState.hh'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/include/Rivet/Projections/IndirectFinalState.hh



## Namespaces

| Name           |
| -------------- |
| **[Rivet](/documentation/code/namespaces/namespacerivet/)**  |




## Source code

```cpp
// -*- C++ -*-
#ifndef RIVET_IndirectFinalState_HH
#define RIVET_IndirectFinalState_HH

#include "Rivet/Projections/NonPromptFinalState.hh"

namespace Rivet {

  using IndirectFinalState = NonPromptFinalState;

}

#endif
```


-------------------------------

Updated on 2022-08-07 at 20:17:18 +0100
