---

title: 'file /home/anarendran/Documents/temp/rivet/include/Rivet/Projections/Smearing.hh'
description: "Convenience include of all Smearing projection headers. "

---

# /home/anarendran/Documents/temp/rivet/include/Rivet/Projections/Smearing.hh

Convenience include of all Smearing projection headers. 




## Source code

```cpp
// -*- C++ -*-
#ifndef RIVET_Smearing_HH
#define RIVET_Smearing_HH


#include "Rivet/Projections/SmearedParticles.hh"
#include "Rivet/Projections/SmearedJets.hh"
#include "Rivet/Projections/SmearedMET.hh"

#endif
```


-------------------------------

Updated on 2022-08-07 at 20:17:18 +0100
