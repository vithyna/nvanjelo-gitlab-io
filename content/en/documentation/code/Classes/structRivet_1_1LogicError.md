---

title: 'struct Rivet::LogicError'
description: "Error specialisation for places where alg logic has failed. "

---

# Rivet::LogicError



<a href="/documentation/code/classes/structrivet_1_1error/">Error</a> specialisation for places where alg logic has failed. 


`#include <Exceptions.hh>`

Inherits from [Rivet::Error](/documentation/code/classes/structrivet_1_1error/), std::runtime_error

## Public Functions

|                | Name           |
| -------------- | -------------- |
| | **[LogicError](/documentation/code/classes/structrivet_1_1logicerror/#function-logicerror)**(const std::string & what) |

## Additional inherited members

**Public Functions inherited from [Rivet::Error](/documentation/code/classes/structrivet_1_1error/)**

|                | Name           |
| -------------- | -------------- |
| | **[Error](/documentation/code/classes/structrivet_1_1error/#function-error)**(const std::string & what) |


## Public Functions Documentation

### function LogicError

```cpp
inline LogicError(
    const std::string & what
)
```


-------------------------------

Updated on 2022-08-07 at 20:17:17 +0100
