---

title: 'struct Rivet::RangeError'
description: "Error for e.g. use of invalid bin ranges. "

---

# Rivet::RangeError



<a href="/documentation/code/classes/structrivet_1_1error/">Error</a> for e.g. use of invalid bin ranges. 


`#include <Exceptions.hh>`

Inherits from [Rivet::Error](/documentation/code/classes/structrivet_1_1error/), std::runtime_error

## Public Functions

|                | Name           |
| -------------- | -------------- |
| | **[RangeError](/documentation/code/classes/structrivet_1_1rangeerror/#function-rangeerror)**(const std::string & what) |

## Additional inherited members

**Public Functions inherited from [Rivet::Error](/documentation/code/classes/structrivet_1_1error/)**

|                | Name           |
| -------------- | -------------- |
| | **[Error](/documentation/code/classes/structrivet_1_1error/#function-error)**(const std::string & what) |


## Public Functions Documentation

### function RangeError

```cpp
inline RangeError(
    const std::string & what
)
```


-------------------------------

Updated on 2022-08-07 at 20:17:17 +0100
