---

title: 'struct Rivet::LookupError'
description: "Error relating to looking up analysis objects in the register. "

---

# Rivet::LookupError



<a href="/documentation/code/classes/structrivet_1_1error/">Error</a> relating to looking up analysis objects in the register. 


`#include <Exceptions.hh>`

Inherits from [Rivet::Error](/documentation/code/classes/structrivet_1_1error/), std::runtime_error

## Public Functions

|                | Name           |
| -------------- | -------------- |
| | **[LookupError](/documentation/code/classes/structrivet_1_1lookuperror/#function-lookuperror)**(const std::string & what) |

## Additional inherited members

**Public Functions inherited from [Rivet::Error](/documentation/code/classes/structrivet_1_1error/)**

|                | Name           |
| -------------- | -------------- |
| | **[Error](/documentation/code/classes/structrivet_1_1error/#function-error)**(const std::string & what) |


## Public Functions Documentation

### function LookupError

```cpp
inline LookupError(
    const std::string & what
)
```


-------------------------------

Updated on 2022-08-07 at 20:17:17 +0100
