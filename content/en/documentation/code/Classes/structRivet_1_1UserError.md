---

title: 'struct Rivet::UserError'
description: "Error specialisation for where the problem is between the chair and the computer. "

---

# Rivet::UserError



<a href="/documentation/code/classes/structrivet_1_1error/">Error</a> specialisation for where the problem is between the chair and the computer. 


`#include <Exceptions.hh>`

Inherits from [Rivet::Error](/documentation/code/classes/structrivet_1_1error/), std::runtime_error

## Public Functions

|                | Name           |
| -------------- | -------------- |
| | **[UserError](/documentation/code/classes/structrivet_1_1usererror/#function-usererror)**(const std::string & what) |

## Additional inherited members

**Public Functions inherited from [Rivet::Error](/documentation/code/classes/structrivet_1_1error/)**

|                | Name           |
| -------------- | -------------- |
| | **[Error](/documentation/code/classes/structrivet_1_1error/#function-error)**(const std::string & what) |


## Public Functions Documentation

### function UserError

```cpp
inline UserError(
    const std::string & what
)
```


-------------------------------

Updated on 2022-08-07 at 20:17:17 +0100
