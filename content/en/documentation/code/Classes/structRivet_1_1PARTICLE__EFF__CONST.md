---

title: 'struct Rivet::PARTICLE_EFF_CONST'
description: "Take a Particle and return a constant number. "

---

# Rivet::PARTICLE_EFF_CONST

**Module:** **[Detector smearing & efficiency functions](/documentation/code/modules/group__smearing/)** **/** **[Generic jet filtering, efficiency and smearing utils](/documentation/code/modules/group__smearing__particle/)**



Take a <a href="/documentation/code/classes/classrivet_1_1particle/">Particle</a> and return a constant number. 


`#include <ParticleSmearingFunctions.hh>`

## Public Functions

|                | Name           |
| -------------- | -------------- |
| | **[PARTICLE_EFF_CONST](/documentation/code/classes/structrivet_1_1particle__eff__const/#function-particle-eff-const)**(double x) |
| double | **[operator()](/documentation/code/classes/structrivet_1_1particle__eff__const/#function-operator())**(const <a href="/documentation/code/classes/classrivet_1_1particle/">Particle</a> & ) const |

## Public Functions Documentation

### function PARTICLE_EFF_CONST

```cpp
inline PARTICLE_EFF_CONST(
    double x
)
```


### function operator()

```cpp
inline double operator()(
    const Particle & 
) const
```


-------------------------------

Updated on 2022-08-07 at 20:17:18 +0100
