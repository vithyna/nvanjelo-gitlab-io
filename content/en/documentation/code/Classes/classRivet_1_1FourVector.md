---

title: 'class Rivet::FourVector'
description: "Specialisation of VectorN to a general (non-momentum) Lorentz 4-vector. "

---

# Rivet::FourVector



Specialisation of VectorN to a general (non-momentum) Lorentz 4-vector.  [More...](#detailed-description)


`#include <Vector4.hh>`

Inherits from [Rivet::Vector< 4 >](/documentation/code/classes/classrivet_1_1vector/)

Inherited by [Rivet::FourMomentum](/documentation/code/classes/classrivet_1_1fourmomentum/)

## Public Types

|                | Name           |
| -------------- | -------------- |
| using Eigen::Matrix< double, N, 1 > | **[EVector](/documentation/code/classes/classrivet_1_1fourvector/#using-evector)** <br>Vector.  |

## Public Functions

|                | Name           |
| -------------- | -------------- |
| | **[FourVector](/documentation/code/modules/group__momutils/#function-fourvector)**() |
| template <typename V4TYPE ,typename std::enable_if< HasXYZT< V4TYPE >::value, int >::type DUMMY =0\> <br>| **[FourVector](/documentation/code/modules/group__momutils/#function-fourvector)**(const V4TYPE & other) |
| | **[FourVector](/documentation/code/modules/group__momutils/#function-fourvector)**(const <a href="/documentation/code/classes/classrivet_1_1vector/">Vector</a>< 4 > & other) |
| | **[FourVector](/documentation/code/modules/group__momutils/#function-fourvector)**(const double t, const double x, const double y, const double z) |
| virtual | **[~FourVector](/documentation/code/modules/group__momutils/#function-~fourvector)**() |
| double | **[t](/documentation/code/modules/group__momutils/#function-t)**() const |
| double | **[t2](/documentation/code/modules/group__momutils/#function-t2)**() const |
| <a href="/documentation/code/classes/classrivet_1_1fourvector/">FourVector</a> & | **[setT](/documentation/code/modules/group__momutils/#function-sett)**(const double t) |
| double | **[x](/documentation/code/modules/group__momutils/#function-x)**() const |
| double | **[x2](/documentation/code/modules/group__momutils/#function-x2)**() const |
| <a href="/documentation/code/classes/classrivet_1_1fourvector/">FourVector</a> & | **[setX](/documentation/code/modules/group__momutils/#function-setx)**(const double x) |
| double | **[y](/documentation/code/modules/group__momutils/#function-y)**() const |
| double | **[y2](/documentation/code/modules/group__momutils/#function-y2)**() const |
| <a href="/documentation/code/classes/classrivet_1_1fourvector/">FourVector</a> & | **[setY](/documentation/code/modules/group__momutils/#function-sety)**(const double y) |
| double | **[z](/documentation/code/modules/group__momutils/#function-z)**() const |
| double | **[z2](/documentation/code/modules/group__momutils/#function-z2)**() const |
| <a href="/documentation/code/classes/classrivet_1_1fourvector/">FourVector</a> & | **[setZ](/documentation/code/modules/group__momutils/#function-setz)**(const double z) |
| double | **[invariant](/documentation/code/modules/group__momutils/#function-invariant)**() const |
| bool | **[isNull](/documentation/code/modules/group__momutils/#function-isnull)**() const |
| double | **[angle](/documentation/code/modules/group__momutils/#function-angle)**(const <a href="/documentation/code/classes/classrivet_1_1fourvector/">FourVector</a> & v) const<br>Angle between this vector and another.  |
| double | **[angle](/documentation/code/modules/group__momutils/#function-angle)**(const <a href="/documentation/code/classes/classrivet_1_1vector3/">Vector3</a> & v3) const<br>Angle between this vector and another (3-vector)  |
| double | **[polarRadius2](/documentation/code/modules/group__momutils/#function-polarradius2)**() const<br>Mod-square of the projection of the 3-vector on to the \( x-y \) plane This is a more efficient function than <code>polarRadius</code>, as it avoids the square root. Use it if you only need the squared value, or e.g. an ordering by magnitude.  |
| double | **[perp2](/documentation/code/modules/group__momutils/#function-perp2)**() const<br>Synonym for polarRadius2.  |
| double | **[rho2](/documentation/code/modules/group__momutils/#function-rho2)**() const<br>Synonym for polarRadius2.  |
| double | **[polarRadius](/documentation/code/modules/group__momutils/#function-polarradius)**() const<br>Magnitude of projection of 3-vector on to the \( x-y \) plane.  |
| double | **[perp](/documentation/code/modules/group__momutils/#function-perp)**() const<br>Synonym for polarRadius.  |
| double | **[rho](/documentation/code/modules/group__momutils/#function-rho)**() const<br>Synonym for polarRadius.  |
| <a href="/documentation/code/classes/classrivet_1_1vector3/">Vector3</a> | **[polarVec](/documentation/code/modules/group__momutils/#function-polarvec)**() const<br>Projection of 3-vector on to the \( x-y \) plane.  |
| <a href="/documentation/code/classes/classrivet_1_1vector3/">Vector3</a> | **[perpVec](/documentation/code/modules/group__momutils/#function-perpvec)**() const<br>Synonym for polarVec.  |
| <a href="/documentation/code/classes/classrivet_1_1vector3/">Vector3</a> | **[rhoVec](/documentation/code/modules/group__momutils/#function-rhovec)**() const<br>Synonym for polarVec.  |
| double | **[azimuthalAngle](/documentation/code/modules/group__momutils/#function-azimuthalangle)**(const <a href="/documentation/code/namespaces/namespacerivet/#enum-phimapping">PhiMapping</a> mapping =ZERO_2PI) const<br>Angle subtended by the 3-vector's projection in x-y and the x-axis.  |
| double | **[phi](/documentation/code/modules/group__momutils/#function-phi)**(const <a href="/documentation/code/namespaces/namespacerivet/#enum-phimapping">PhiMapping</a> mapping =ZERO_2PI) const<br>Synonym for azimuthalAngle.  |
| double | **[polarAngle](/documentation/code/modules/group__momutils/#function-polarangle)**() const<br>Angle subtended by the 3-vector and the z-axis.  |
| double | **[theta](/documentation/code/modules/group__momutils/#function-theta)**() const<br>Synonym for polarAngle.  |
| double | **[pseudorapidity](/documentation/code/modules/group__momutils/#function-pseudorapidity)**() const<br>Pseudorapidity (defined purely by the 3-vector components)  |
| double | **[eta](/documentation/code/modules/group__momutils/#function-eta)**() const<br>Synonym for pseudorapidity.  |
| double | **[abspseudorapidity](/documentation/code/modules/group__momutils/#function-abspseudorapidity)**() const<br>Get the \( |\eta| \) directly.  |
| double | **[abseta](/documentation/code/modules/group__momutils/#function-abseta)**() const<br>Get the \( |\eta| \) directly (alias).  |
| <a href="/documentation/code/classes/classrivet_1_1vector3/">Vector3</a> | **[vector3](/documentation/code/modules/group__momutils/#function-vector3)**() const<br>Get the spatial part of the 4-vector as a 3-vector.  |
| | **[operator Vector3](/documentation/code/modules/group__momutils/#function-operator-vector3)**() const<br>Implicit cast to a 3-vector.  |
| double | **[contract](/documentation/code/modules/group__momutils/#function-contract)**(const <a href="/documentation/code/classes/classrivet_1_1fourvector/">FourVector</a> & v) const<br>Contract two 4-vectors, with metric signature (+ - - -).  |
| double | **[dot](/documentation/code/modules/group__momutils/#function-dot)**(const <a href="/documentation/code/classes/classrivet_1_1fourvector/">FourVector</a> & v) const<br>Contract two 4-vectors, with metric signature (+ - - -).  |
| double | **[operator*](/documentation/code/modules/group__momutils/#function-operator*)**(const <a href="/documentation/code/classes/classrivet_1_1fourvector/">FourVector</a> & v) const<br>Contract two 4-vectors, with metric signature (+ - - -).  |
| <a href="/documentation/code/classes/classrivet_1_1fourvector/">FourVector</a> & | **[operator*=](/documentation/code/modules/group__momutils/#function-operator*=)**(double a)<br>Multiply by a scalar.  |
| <a href="/documentation/code/classes/classrivet_1_1fourvector/">FourVector</a> & | **[operator/=](/documentation/code/modules/group__momutils/#function-operator/=)**(double a)<br>Divide by a scalar.  |
| <a href="/documentation/code/classes/classrivet_1_1fourvector/">FourVector</a> & | **[operator+=](/documentation/code/modules/group__momutils/#function-operator+=)**(const <a href="/documentation/code/classes/classrivet_1_1fourvector/">FourVector</a> & v)<br>Add to this 4-vector.  |
| <a href="/documentation/code/classes/classrivet_1_1fourvector/">FourVector</a> & | **[operator-=](/documentation/code/modules/group__momutils/#function-operator-=)**(const <a href="/documentation/code/classes/classrivet_1_1fourvector/">FourVector</a> & v)<br>Subtract from this 4-vector. NB time as well as space components are subtracted.  |
| <a href="/documentation/code/classes/classrivet_1_1fourvector/">FourVector</a> | **[operator-](/documentation/code/modules/group__momutils/#function-operator-)**() const<br>Multiply all components (space and time) by -1.  |
| <a href="/documentation/code/classes/classrivet_1_1fourvector/">FourVector</a> | **[reverse](/documentation/code/modules/group__momutils/#function-reverse)**() const<br>Multiply space components only by -1.  |
| const double & | **[get](/documentation/code/classes/classrivet_1_1fourvector/#function-get)**(const size_t index) const |
| double & | **[get](/documentation/code/classes/classrivet_1_1fourvector/#function-get)**(const size_t index) |
| const double & | **[operator[]](/documentation/code/classes/classrivet_1_1fourvector/#function-operator[])**(const size_t index) const<br>Direct access to vector elements by index.  |
| double & | **[operator[]](/documentation/code/classes/classrivet_1_1fourvector/#function-operator[])**(const size_t index)<br>Direct access to vector elements by index.  |
| <a href="/documentation/code/classes/classrivet_1_1vector/">Vector</a>< N > & | **[set](/documentation/code/classes/classrivet_1_1fourvector/#function-set)**(const size_t index, const double value)<br>Set indexed value.  |
| constexpr size_t | **[size](/documentation/code/classes/classrivet_1_1fourvector/#function-size)**() const<br>Vector dimensionality.  |
| bool | **[isZero](/documentation/code/classes/classrivet_1_1fourvector/#function-iszero)**(double tolerance =1E-5) const<br>Check for nullness, allowing for numerical precision.  |
| double | **[mod2](/documentation/code/classes/classrivet_1_1fourvector/#function-mod2)**() const<br>Calculate the modulus-squared of a vector. \( \sum_{i=1}^N x_i^2 \).  |
| double | **[mod](/documentation/code/classes/classrivet_1_1fourvector/#function-mod)**() const<br>Calculate the modulus of a vector. \( \sqrt{\sum_{i=1}^N x_i^2} \).  |
| bool | **[operator==](/documentation/code/classes/classrivet_1_1fourvector/#function-operator==)**(const <a href="/documentation/code/classes/classrivet_1_1vector/">Vector</a>< N > & a) const |
| bool | **[operator!=](/documentation/code/classes/classrivet_1_1fourvector/#function-operator!=)**(const <a href="/documentation/code/classes/classrivet_1_1vector/">Vector</a>< N > & a) const |

## Friends

|                | Name           |
| -------------- | -------------- |
| <a href="/documentation/code/classes/classrivet_1_1fourvector/">FourVector</a> | **[multiply](/documentation/code/modules/group__momutils/#friend-multiply)**(const double a, const <a href="/documentation/code/classes/classrivet_1_1fourvector/">FourVector</a> & v)  |
| <a href="/documentation/code/classes/classrivet_1_1fourvector/">FourVector</a> | **[multiply](/documentation/code/modules/group__momutils/#friend-multiply)**(const <a href="/documentation/code/classes/classrivet_1_1fourvector/">FourVector</a> & v, const double a)  |
| <a href="/documentation/code/classes/classrivet_1_1fourvector/">FourVector</a> | **[add](/documentation/code/modules/group__momutils/#friend-add)**(const <a href="/documentation/code/classes/classrivet_1_1fourvector/">FourVector</a> & a, const <a href="/documentation/code/classes/classrivet_1_1fourvector/">FourVector</a> & b)  |
| <a href="/documentation/code/classes/classrivet_1_1fourvector/">FourVector</a> | **[transform](/documentation/code/modules/group__momutils/#friend-transform)**(const <a href="/documentation/code/classes/classrivet_1_1lorentztransform/">LorentzTransform</a> & lt, const <a href="/documentation/code/classes/classrivet_1_1fourvector/">FourVector</a> & v4)  |

## Additional inherited members

**Public Functions inherited from [Rivet::Vector< 4 >](/documentation/code/classes/classrivet_1_1vector/)**

|                | Name           |
| -------------- | -------------- |
| | **[Vector](/documentation/code/classes/classrivet_1_1vector/#function-vector)**() |
| | **[Vector](/documentation/code/classes/classrivet_1_1vector/#function-vector)**(const <a href="/documentation/code/classes/classrivet_1_1vector/">Vector</a>< N > & other) |


## Detailed Description

```cpp
class Rivet::FourVector;
```

Specialisation of VectorN to a general (non-momentum) Lorentz 4-vector. 

**Todo**: Add composite set/mk methods from different coord systems 
## Public Types Documentation

### using EVector

```cpp
using Rivet::Vector< N >::EVector =  Eigen::Matrix<double,N,1>;
```

Vector. 

## Public Functions Documentation

### function FourVector

```cpp
inline FourVector()
```


### function FourVector

```cpp
template <typename V4TYPE ,
typename std::enable_if< HasXYZT< V4TYPE >::value, int >::type DUMMY =0>
inline FourVector(
    const V4TYPE & other
)
```


### function FourVector

```cpp
inline FourVector(
    const Vector< 4 > & other
)
```


### function FourVector

```cpp
inline FourVector(
    const double t,
    const double x,
    const double y,
    const double z
)
```


### function ~FourVector

```cpp
inline virtual ~FourVector()
```


### function t

```cpp
inline double t() const
```


### function t2

```cpp
inline double t2() const
```


### function setT

```cpp
inline FourVector & setT(
    const double t
)
```


### function x

```cpp
inline double x() const
```


### function x2

```cpp
inline double x2() const
```


### function setX

```cpp
inline FourVector & setX(
    const double x
)
```


### function y

```cpp
inline double y() const
```


### function y2

```cpp
inline double y2() const
```


### function setY

```cpp
inline FourVector & setY(
    const double y
)
```


### function z

```cpp
inline double z() const
```


### function z2

```cpp
inline double z2() const
```


### function setZ

```cpp
inline FourVector & setZ(
    const double z
)
```


### function invariant

```cpp
inline double invariant() const
```


### function isNull

```cpp
inline bool isNull() const
```


### function angle

```cpp
inline double angle(
    const FourVector & v
) const
```

Angle between this vector and another. 

### function angle

```cpp
inline double angle(
    const Vector3 & v3
) const
```

Angle between this vector and another (3-vector) 

### function polarRadius2

```cpp
inline double polarRadius2() const
```

Mod-square of the projection of the 3-vector on to the \( x-y \) plane This is a more efficient function than <code>polarRadius</code>, as it avoids the square root. Use it if you only need the squared value, or e.g. an ordering by magnitude. 

### function perp2

```cpp
inline double perp2() const
```

Synonym for polarRadius2. 

### function rho2

```cpp
inline double rho2() const
```

Synonym for polarRadius2. 

### function polarRadius

```cpp
inline double polarRadius() const
```

Magnitude of projection of 3-vector on to the \( x-y \) plane. 

### function perp

```cpp
inline double perp() const
```

Synonym for polarRadius. 

### function rho

```cpp
inline double rho() const
```

Synonym for polarRadius. 

### function polarVec

```cpp
inline Vector3 polarVec() const
```

Projection of 3-vector on to the \( x-y \) plane. 

### function perpVec

```cpp
inline Vector3 perpVec() const
```

Synonym for polarVec. 

### function rhoVec

```cpp
inline Vector3 rhoVec() const
```

Synonym for polarVec. 

### function azimuthalAngle

```cpp
inline double azimuthalAngle(
    const PhiMapping mapping =ZERO_2PI
) const
```

Angle subtended by the 3-vector's projection in x-y and the x-axis. 

### function phi

```cpp
inline double phi(
    const PhiMapping mapping =ZERO_2PI
) const
```

Synonym for azimuthalAngle. 

### function polarAngle

```cpp
inline double polarAngle() const
```

Angle subtended by the 3-vector and the z-axis. 

### function theta

```cpp
inline double theta() const
```

Synonym for polarAngle. 

### function pseudorapidity

```cpp
inline double pseudorapidity() const
```

Pseudorapidity (defined purely by the 3-vector components) 

### function eta

```cpp
inline double eta() const
```

Synonym for pseudorapidity. 

### function abspseudorapidity

```cpp
inline double abspseudorapidity() const
```

Get the \( |\eta| \) directly. 

### function abseta

```cpp
inline double abseta() const
```

Get the \( |\eta| \) directly (alias). 

### function vector3

```cpp
inline Vector3 vector3() const
```

Get the spatial part of the 4-vector as a 3-vector. 

### function operator Vector3

```cpp
inline operator Vector3() const
```

Implicit cast to a 3-vector. 

### function contract

```cpp
inline double contract(
    const FourVector & v
) const
```

Contract two 4-vectors, with metric signature (+ - - -). 

### function dot

```cpp
inline double dot(
    const FourVector & v
) const
```

Contract two 4-vectors, with metric signature (+ - - -). 

### function operator*

```cpp
inline double operator*(
    const FourVector & v
) const
```

Contract two 4-vectors, with metric signature (+ - - -). 

### function operator*=

```cpp
inline FourVector & operator*=(
    double a
)
```

Multiply by a scalar. 

### function operator/=

```cpp
inline FourVector & operator/=(
    double a
)
```

Divide by a scalar. 

### function operator+=

```cpp
inline FourVector & operator+=(
    const FourVector & v
)
```

Add to this 4-vector. 

### function operator-=

```cpp
inline FourVector & operator-=(
    const FourVector & v
)
```

Subtract from this 4-vector. NB time as well as space components are subtracted. 

### function operator-

```cpp
inline FourVector operator-() const
```

Multiply all components (space and time) by -1. 

### function reverse

```cpp
inline FourVector reverse() const
```

Multiply space components only by -1. 

### function get

```cpp
inline const double & get(
    const size_t index
) const
```


### function get

```cpp
inline double & get(
    const size_t index
)
```


### function operator[]

```cpp
inline const double & operator[](
    const size_t index
) const
```

Direct access to vector elements by index. 

### function operator[]

```cpp
inline double & operator[](
    const size_t index
)
```

Direct access to vector elements by index. 

### function set

```cpp
inline Vector< N > & set(
    const size_t index,
    const double value
)
```

Set indexed value. 

### function size

```cpp
inline constexpr size_t size() const
```

Vector dimensionality. 

### function isZero

```cpp
inline bool isZero(
    double tolerance =1E-5
) const
```

Check for nullness, allowing for numerical precision. 

### function mod2

```cpp
inline double mod2() const
```

Calculate the modulus-squared of a vector. \( \sum_{i=1}^N x_i^2 \). 

### function mod

```cpp
inline double mod() const
```

Calculate the modulus of a vector. \( \sqrt{\sum_{i=1}^N x_i^2} \). 

### function operator==

```cpp
inline bool operator==(
    const Vector< N > & a
) const
```


### function operator!=

```cpp
inline bool operator!=(
    const Vector< N > & a
) const
```


## Friends

### friend multiply

```cpp
friend FourVector multiply(
    const double a,

    const FourVector & v
);
```


### friend multiply

```cpp
friend FourVector multiply(
    const FourVector & v,

    const double a
);
```


### friend add

```cpp
friend FourVector add(
    const FourVector & a,

    const FourVector & b
);
```


### friend transform

```cpp
friend FourVector transform(
    const LorentzTransform & lt,

    const FourVector & v4
);
```


-------------------------------

Updated on 2022-08-07 at 20:17:17 +0100
