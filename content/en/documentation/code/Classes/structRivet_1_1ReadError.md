---

title: 'struct Rivet::ReadError'
description: "Error for read failures. "

---

# Rivet::ReadError



<a href="/documentation/code/classes/structrivet_1_1error/">Error</a> for read failures. 


`#include <Exceptions.hh>`

Inherits from [Rivet::IOError](/documentation/code/classes/structrivet_1_1ioerror/), [Rivet::Error](/documentation/code/classes/structrivet_1_1error/), std::runtime_error

## Public Functions

|                | Name           |
| -------------- | -------------- |
| | **[ReadError](/documentation/code/classes/structrivet_1_1readerror/#function-readerror)**(const std::string & what) |

## Additional inherited members

**Public Functions inherited from [Rivet::IOError](/documentation/code/classes/structrivet_1_1ioerror/)**

|                | Name           |
| -------------- | -------------- |
| | **[IOError](/documentation/code/classes/structrivet_1_1ioerror/#function-ioerror)**(const std::string & what) |

**Public Functions inherited from [Rivet::Error](/documentation/code/classes/structrivet_1_1error/)**

|                | Name           |
| -------------- | -------------- |
| | **[Error](/documentation/code/classes/structrivet_1_1error/#function-error)**(const std::string & what) |


## Public Functions Documentation

### function ReadError

```cpp
inline ReadError(
    const std::string & what
)
```


-------------------------------

Updated on 2022-08-07 at 20:17:17 +0100
