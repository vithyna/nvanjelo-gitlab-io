---

title: 'struct Rivet::P3_EFF_CONST'
description: "Take a Vector3 and return a constant number. "

---

# Rivet::P3_EFF_CONST

**Module:** **[Detector smearing & efficiency functions](/documentation/code/modules/group__smearing/)** **/** **[Generic 4-momentum filtering, efficiency and smearing utils](/documentation/code/modules/group__smearing__mom/)**



Take a Vector3 and return a constant number. 


`#include <MomentumSmearingFunctions.hh>`

## Public Functions

|                | Name           |
| -------------- | -------------- |
| | **[P3_EFF_CONST](/documentation/code/classes/structrivet_1_1p3__eff__const/#function-p3-eff-const)**(double x) |
| double | **[operator()](/documentation/code/classes/structrivet_1_1p3__eff__const/#function-operator())**(const <a href="/documentation/code/classes/classrivet_1_1vector3/">Vector3</a> & ) const |

## Public Functions Documentation

### function P3_EFF_CONST

```cpp
inline P3_EFF_CONST(
    double x
)
```


### function operator()

```cpp
inline double operator()(
    const Vector3 & 
) const
```


-------------------------------

Updated on 2022-08-07 at 20:17:18 +0100
