---
title: "Classes"

menu:
  documentation:
    parent: "code"
weight: 20
---


&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>namespace <a href=/documentation/code/namespaces/namespacehepmc3/>HepMC3<a></b><br>
<details><summary><b>namespace <a href=/documentation/code/namespaces/namespacerivet/>Rivet<a></b></summary>
<details><summary><b>namespace <a href=/documentation/code/namespaces/namespacerivet_1_1alice/>ALICE<a></b></summary>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1alice_1_1clmultiplicity/>CLMultiplicity<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1alice_1_1primaryparticles/>PrimaryParticles<a></b><br>Standard <a href="/documentation/code/namespaces/namespacerivet_1_1alice/">ALICE</a> primary particle definition. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1alice_1_1v0andtrigger/>V0AndTrigger<a></b><br>Trigger projection for the <a href="/documentation/code/namespaces/namespacerivet_1_1alice/">ALICE</a> V0-AND (a.k.a. CINT7) requirement. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1alice_1_1v0multiplicity/>V0Multiplicity<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1alice_1_1v0trigger/>V0Trigger<a></b><br></details>
<details><summary><b>namespace <a href=/documentation/code/namespaces/namespacerivet_1_1atlas/>ATLAS<a></b><br>Common projections for ATLAS trigger conditions and centrality. </summary>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1atlas_1_1minbiastrigger/>MinBiasTrigger<a></b><br><a href="/documentation/code/namespaces/namespacerivet_1_1atlas/">ATLAS</a> min bias trigger conditions. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1atlas_1_1sumet__pbpb__centrality/>SumET_PBPB_Centrality<a></b><br>Centrality projection for PbPb collisions (two sided) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1atlas_1_1sumet__pb__centrality/>SumET_PB_Centrality<a></b><br>Centrality projection for pPb collisions (one sided) <br></details>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1absdeltaetawrt/>AbsDeltaEtaWRT<a></b><br>Calculator of \( |\Delta \eta| \) with respect to a given momentum. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1absdeltarapwrt/>AbsDeltaRapWRT<a></b><br>Calculator of \( |\Delta y| \) with respect to a given momentum. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1absetagtr/>AbsEtaGtr<a></b><br>Abs pseudorapidity greater-than functor. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1absetainrange/>AbsEtaInRange<a></b><br>Abs pseudorapidity in-range functor. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1absetaless/>AbsEtaLess<a></b><br>Abs pseudorapidity momentum less-than functor. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1absrapgtr/>AbsRapGtr<a></b><br>Abs rapidity greater-than functor. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1absrapinrange/>AbsRapInRange<a></b><br>Abs rapidity in-range functor. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1absrapless/>AbsRapLess<a></b><br>Abs rapidity momentum less-than functor. <br>
<details><summary><b>class <a href=/documentation/code/classes/classrivet_1_1analysis/>Analysis<a></b><br>This is the base class of all analysis classes in <a href="/documentation/code/namespaces/namespacerivet/">Rivet</a>. </summary>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1analysis_1_1counteradapter/>CounterAdapter<a></b><br>To be used in finalize context only. <br></details>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1analysishandler/>AnalysisHandler<a></b><br>The key class for coordination of <a href="/documentation/code/classes/classrivet_1_1analysis/">Analysis</a> objects and the event loop. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1analysisinfo/>AnalysisInfo<a></b><br>Holder of analysis metadata. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1analysisloader/>AnalysisLoader<a></b><br>Internal class which loads and registers analyses from plugin libs. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1axesdefinition/>AxesDefinition<a></b><br>Base class for projections which define a spatial basis. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1brahmscentrality/>BRAHMSCentrality<a></b><br>BRAHMS Centrality projection. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1beam/>Beam<a></b><br>Project out the incoming beams. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1beamthrust/>BeamThrust<a></b><br>Calculator of the beam-thrust observable. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1binnedhistogram/>BinnedHistogram<a></b><br>A set of booked Histo1DPtr, each in a bin of a second variable. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1booljetand/>BoolJetAND<a></b><br>Functor for and-combination of selector logic. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1booljetfunctor/>BoolJetFunctor<a></b><br>Base type for Jet -> bool functors. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1booljetnot/>BoolJetNOT<a></b><br>Functor for inverting selector logic. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1booljetor/>BoolJetOR<a></b><br>Functor for or-combination of selector logic. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1boolparticleand/>BoolParticleAND<a></b><br>Functor for and-combination of selector logic. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1boolparticlebasefunctor/>BoolParticleBaseFunctor<a></b><br>Base type for Particle -> bool functors. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1boolparticlefunctor/>BoolParticleFunctor<a></b><br>Base type for Particle -> bool functors. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1boolparticlenot/>BoolParticleNOT<a></b><br>Functor for inverting selector logic. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1boolparticleor/>BoolParticleOR<a></b><br>Functor for or-combination of selector logic. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1centralethcm/>CentralEtHCM<a></b><br>Summed \( E_\perp \) of central particles in HCM system. <br>
<details><summary><b>class <a href=/documentation/code/classes/classrivet_1_1centralitybinner/>CentralityBinner<a></b></summary>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1centralitybinner_1_1bin/>Bin<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1centralitybinner_1_1flexibin/>FlexiBin<a></b><br>A flexible bin struct to be used to store temporary AnalysisObjects. <br></details>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1centralityestimator/>CentralityEstimator<a></b><br>Base class for projections profile observable value vs the collision centrality. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1centralityprojection/>CentralityProjection<a></b><br>Used together with the percentile-based analysis objects Percentile and PercentileXaxis. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1chargedfinalstate/>ChargedFinalState<a></b><br>Project only charged final state particles. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1chargedleptons/>ChargedLeptons<a></b><br>Get charged final-state leptons. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1constlossyfinalstate/>ConstLossyFinalState<a></b><br>Randomly lose a constant fraction of particles. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1constrandomfilter/>ConstRandomFilter<a></b><br>Functor used to implement constant random lossiness. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1correlators/>Correlators<a></b><br>Projection for calculating correlators for flow measurements. <br>
<details><summary><b>class <a href=/documentation/code/classes/classrivet_1_1cumulantanalysis/>CumulantAnalysis<a></b><br>Tools for flow analyses. </summary>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1cumulantanalysis_1_1corbin/>CorBin<a></b><br>The basic bin quantity in ECorrelators. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1cumulantanalysis_1_1corbinbase/>CorBinBase<a></b><br>Base class for correlator bins. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1cumulantanalysis_1_1corsinglebin/>CorSingleBin<a></b><br>The basic quantity filled in an ECorrelator. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1cumulantanalysis_1_1ecorrelator/>ECorrelator<a></b><br>A helper class to calculate all event averages of correlators. <br></details>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1cutflow/>Cutflow<a></b><br>A tracker of numbers & fractions of events passing sequential cuts. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1cutflows/>Cutflows<a></b><br>A container for several <a href="/documentation/code/classes/structrivet_1_1cutflow/">Cutflow</a> objects, with some convenient batch access. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>namespace <a href=/documentation/code/namespaces/namespacerivet_1_1cuts/>Cuts<a></b><br>Namespace used for ambiguous identifiers. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1disdiffhadron/>DISDiffHadron<a></b><br>Get the incoming and outgoing hadron in a diffractive ep event. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1disfinalstate/>DISFinalState<a></b><br>Final state particles boosted to the hadronic center of mass system. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1diskinematics/>DISKinematics<a></b><br>Get the DIS kinematic variables and relevant boosts for an event. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1dislepton/>DISLepton<a></b><br>Get the incoming and outgoing leptons in a DIS event. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1disrapiditygap/>DISRapidityGap<a></b><br>Get the incoming and outgoing hadron in a diffractive ep event. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1deltaetagtr/>DeltaEtaGtr<a></b><br>\( |\Delta \eta| \) (with respect to another momentum, _vec_) greater-than functor <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1deltaetainrange/>DeltaEtaInRange<a></b><br>\( \Delta \eta \) (with respect to another 4-momentum, _vec_) in-range functor <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1deltaetaless/>DeltaEtaLess<a></b><br>\( |\Delta \eta| \) (with respect to another momentum, _vec_) less-than functor <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1deltaetawrt/>DeltaEtaWRT<a></b><br>Calculator of \( \Delta \eta \) with respect to a given momentum. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1deltaphigtr/>DeltaPhiGtr<a></b><br>\( |\Delta \phi| \) (with respect to another momentum, _vec_) greater-than functor <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1deltaphiinrange/>DeltaPhiInRange<a></b><br>\( \Delta \phi \) (with respect to another 4-momentum, _vec_) in-range functor <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1deltaphiless/>DeltaPhiLess<a></b><br>\( |\Delta \phi| \) (with respect to another momentum, _vec_) less-than functor <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1deltaphiwrt/>DeltaPhiWRT<a></b><br>Calculator of \( \Delta \phi \) with respect to a given momentum. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1deltargtr/>DeltaRGtr<a></b><br>\( \Delta R \) (with respect to another 4-momentum, _vec_) greater-than functor <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1deltarinrange/>DeltaRInRange<a></b><br>\( \Delta R \) (with respect to another 4-momentum, _vec_) in-range functor <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1deltarless/>DeltaRLess<a></b><br>\( \Delta R \) (with respect to another 4-momentum, _vec_) less-than functor <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1deltarwrt/>DeltaRWRT<a></b><br>Calculator of \( \Delta R \) with respect to a given momentum. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1deltarapgtr/>DeltaRapGtr<a></b><br>\( |\Delta y| \) (with respect to another momentum, _vec_) greater-than functor <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1deltarapinrange/>DeltaRapInRange<a></b><br>\( \Delta y \) (with respect to another 4-momentum, _vec_) in-range functor <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1deltarapless/>DeltaRapLess<a></b><br>\( |\Delta y| \) (with respect to another momentum, _vec_) less-than functor <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1deltarapwrt/>DeltaRapWRT<a></b><br>Calculator of \( \Delta y \) with respect to a given momentum. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1doubleparticlebasefunctor/>DoubleParticleBaseFunctor<a></b><br>Base type for Particle -> double functors. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1dressedlepton/>DressedLepton<a></b><br>A charged lepton meta-particle created by clustering photons close to the bare lepton. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1dressedleptons/>DressedLeptons<a></b><br>Cluster photons from a given FS to all charged particles (typically leptons) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1error/>Error<a></b><br>Generic runtime <a href="/documentation/code/namespaces/namespacerivet/">Rivet</a> error. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1etagtr/>EtaGtr<a></b><br>Pseudorapidity greater-than functor. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1etainrange/>EtaInRange<a></b><br>Pseudorapidity in-range functor. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1etaless/>EtaLess<a></b><br>Pseudorapidity less-than functor. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1event/>Event<a></b><br>Representation of a HepMC event, and enabler of Projection caching. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1eventmixingbase/>EventMixingBase<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1eventmixingcentrality/>EventMixingCentrality<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1eventmixingfinalstate/>EventMixingFinalState<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1fparameter/>FParameter<a></b><br>Calculator of the \( F \)-parameter observable. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1fastjets/>FastJets<a></b><br>Project out jets found using the FastJet package jet algorithms. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1finalpartons/>FinalPartons<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1finalstate/>FinalState<a></b><br>Project out all final-state particles in an event. Probably the most important projection in Rivet! <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1firstparticlewith/>FirstParticleWith<a></b><br>Determine whether a particle is the first in a decay chain to meet the cut/function. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1firstparticlewithout/>FirstParticleWithout<a></b><br>Determine whether a particle is the first in a decay chain not to meet the cut/function. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1fourmomentum/>FourMomentum<a></b><br>Specialized version of the FourVector with momentum/energy functionality. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1fourvector/>FourVector<a></b><br>Specialisation of VectorN to a general (non-momentum) Lorentz 4-vector. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1gammagammafinalstate/>GammaGammaFinalState<a></b><br>Final state particles boosted to the hadronic center of mass system. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1gammagammakinematics/>GammaGammaKinematics<a></b><br>Get the gamma gamma kinematic variables and relevant boosts for an event. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1gammagammaleptons/>GammaGammaLeptons<a></b><br>Get the incoming and outgoing leptons in a gamma gamma collision event in e+e-. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1generatedcentrality/>GeneratedCentrality<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1generatedpercentileprojection/>GeneratedPercentileProjection<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1hadronicfinalstate/>HadronicFinalState<a></b><br>Project only hadronic final state particles. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1hasabspid/>HasAbsPID<a></b><br>|PID| matching functor <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1hasbtag/>HasBTag<a></b><br>B-tagging functor, with a tag selection cut as the stored state. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1hasctag/>HasCTag<a></b><br>C-tagging functor, with a tag selection cut as the stored state. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1hasnotag/>HasNoTag<a></b><br>Anti-B/C-tagging functor, with a tag selection cut as the stored state. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1haspid/>HasPID<a></b><br>PID matching functor. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1hasparticleancestorwith/>HasParticleAncestorWith<a></b><br>Determine whether a particle has an ancestor which meets the cut/function. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1hasparticleancestorwithout/>HasParticleAncestorWithout<a></b><br>Determine whether a particle has an ancestor which doesn't meet the cut/function. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1hasparticlechildwith/>HasParticleChildWith<a></b><br>Determine whether a particle has a child which meets the cut/function. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1hasparticlechildwithout/>HasParticleChildWithout<a></b><br>Determine whether a particle has a child which doesn't meet the cut/function. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1hasparticledescendantwith/>HasParticleDescendantWith<a></b><br>Determine whether a particle has a descendant which meets the cut/function. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1hasparticledescendantwithout/>HasParticleDescendantWithout<a></b><br>Determine whether a particle has a descendant which doesn't meet the cut/function. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1hasparticleparentwith/>HasParticleParentWith<a></b><br>Determine whether a particle has an parent which meets the cut/function. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1hasparticleparentwithout/>HasParticleParentWithout<a></b><br>Determine whether a particle has an parent which doesn't meet the cut/function. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1hastautag/>HasTauTag<a></b><br>Tau-tagging functor, with a tag selection cut as the stored state. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1heavyhadrons/>HeavyHadrons<a></b><br>Project out the last pre-decay b and c hadrons. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1hemispheres/>Hemispheres<a></b><br>Calculate the hemisphere masses and broadenings. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1hepmcheavyion/>HepMCHeavyIon<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>namespace <a href=/documentation/code/namespaces/namespacerivet_1_1hepmcutils/>HepMCUtils<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1ioerror/>IOError<a></b><br><a href="/documentation/code/classes/structrivet_1_1error/">Error</a> for I/O failures. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1identifiedfinalstate/>IdentifiedFinalState<a></b><br>Produce a final state which only contains specified particle IDs. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1impactparameterprojection/>ImpactParameterProjection<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1infoerror/>InfoError<a></b><br><a href="/documentation/code/classes/structrivet_1_1error/">Error</a> specialisation for failures relating to analysis info. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1initialquarks/>InitialQuarks<a></b><br>Project out quarks from the hard process in \( e^+ e^- \to Z^0 \) events. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1invmassfinalstate/>InvMassFinalState<a></b><br>Identify particles which can be paired to fit within a given invariant mass window. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1invisiblefinalstate/>InvisibleFinalState<a></b><br>Final state modifier excluding particles which are experimentally visible. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1jet__btag__effs/>JET_BTAG_EFFS<a></b><br>b-tagging efficiency functor, for more readable b-tag effs and mistag rates <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1jet__eff__const/>JET_EFF_CONST<a></b><br>Take a <a href="/documentation/code/classes/classrivet_1_1jet/">Jet</a> and return a constant efficiency. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1jet/>Jet<a></b><br>Representation of a clustered jet of particles. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1jetefffilter/>JetEffFilter<a></b><br>A functor to return true if <a href="/documentation/code/classes/classrivet_1_1jet/">Jet</a>_j_ survives a random efficiency selection. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1jeteffsmearfn/>JetEffSmearFn<a></b><br>Functor for simultaneous efficiency-filtering and smearing of Jets. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1jetfinder/>JetFinder<a></b><br>Abstract base class for projections which can return a set of <code><a href="/documentation/code/classes/classrivet_1_1jet/">Jet</a></code>s. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1jetshape/>JetShape<a></b><br>Calculate transverse jet profiles. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1jets/>Jets<a></b><br>Specialised vector of <a href="/documentation/code/classes/classrivet_1_1jet/">Jet</a> objects. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>namespace <a href=/documentation/code/namespaces/namespacerivet_1_1kin/>Kin<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1lastparticlewith/>LastParticleWith<a></b><br>Determine whether a particle is the last in a decay chain to meet the cut/function. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1lastparticlewithout/>LastParticleWithout<a></b><br>Determine whether a particle is the last in a decay chain not to meet the cut/function. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1leadingparticlesfinalstate/>LeadingParticlesFinalState<a></b><br>Get the highest-pT occurrences of FS particles with the specified PDG IDs. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1log/>Log<a></b><br>Logging system for controlled & formatted writing to stdout. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1logicerror/>LogicError<a></b><br><a href="/documentation/code/classes/structrivet_1_1error/">Error</a> specialisation for places where alg logic has failed. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1lookuperror/>LookupError<a></b><br><a href="/documentation/code/classes/structrivet_1_1error/">Error</a> relating to looking up analysis objects in the register. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1lorentztransform/>LorentzTransform<a></b><br>Object implementing Lorentz transform calculations and boosts. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1lossyfinalstate/>LossyFinalState<a></b><br>Templated FS projection which can lose some of the supplied particles. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1mc__jetanalysis/>MC_JetAnalysis<a></b><br>Base class providing common functionality for MC jet validation analyses. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1mc__jetsplittings/>MC_JetSplittings<a></b><br>Base class providing common functionality for MC jet validation analyses. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1mc__particleanalysis/>MC_ParticleAnalysis<a></b><br>Base class providing common functionality for MC particle species validation analyses. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1mc__sumetfwdpbcentrality/>MC_SumETFwdPbCentrality<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1mc__ppbminbiastrigger/>MC_pPbMinBiasTrigger<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1metfinder/>METFinder<a></b><br>Interface for projections that find missing transverse energy/momentum. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1matrix/>Matrix<a></b><br>General \( N \)-dimensional mathematical matrix object. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1matrix3/>Matrix3<a></b><br>Specialisation of MatrixN to aid 3 dimensional rotations. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1mendelmin/>MendelMin<a></b><br>A genetic algorithm functional minimizer. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1mergedfinalstate/>MergedFinalState<a></b><br>Get final state particles merged from two <a href="/documentation/code/classes/classrivet_1_1finalstate/">FinalState</a> projections. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1missingmomentum/>MissingMomentum<a></b><br>Calculate missing \( E \), \( E_\perp \) etc. as complements to the total visible momentum. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1neutralfinalstate/>NeutralFinalState<a></b><br>Project only neutral final state particles. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1nonhadronicfinalstate/>NonHadronicFinalState<a></b><br>Project only hadronic final state particles. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1nonpromptfinalstate/>NonPromptFinalState<a></b><br>Find final state particles NOT directly connected to the hard process. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1p3__eff__const/>P3_EFF_CONST<a></b><br>Take a Vector3 and return a constant number. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1p4__eff__const/>P4_EFF_CONST<a></b><br>Take a <a href="/documentation/code/classes/classrivet_1_1fourmomentum/">FourMomentum</a> and return a constant number. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1particle__eff__const/>PARTICLE_EFF_CONST<a></b><br>Take a <a href="/documentation/code/classes/classrivet_1_1particle/">Particle</a> and return a constant number. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>namespace <a href=/documentation/code/namespaces/namespacerivet_1_1pid/>PID<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1parisitensor/>ParisiTensor<a></b><br>Calculate the Parisi event shape tensor (or linear momentum tensor). <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1particle/>Particle<a></b><br>Particle representation, either from a HepMC::GenEvent or reconstructed. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1particlebase/>ParticleBase<a></b><br>Base class for particle-like things like <a href="/documentation/code/classes/classrivet_1_1particle/">Particle</a> and <a href="/documentation/code/classes/classrivet_1_1jet/">Jet</a>. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1particleefffilter/>ParticleEffFilter<a></b><br>A functor to return true if <a href="/documentation/code/classes/classrivet_1_1particle/">Particle</a>_p_ survives a random efficiency selection. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1particleeffsmearfn/>ParticleEffSmearFn<a></b><br>Functor for simultaneous efficiency-filtering and smearing of Particles. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1particlefinder/>ParticleFinder<a></b><br>Base class for projections which return subsets of an event's particles. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1particles/>Particles<a></b><br>Specialised vector of <a href="/documentation/code/classes/classrivet_1_1particle/">Particle</a> objects. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1partonictops/>PartonicTops<a></b><br>Convenience finder of partonic top quarks. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1percentile/>Percentile<a></b><br>The Percentile class for centrality binning. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1percentilebase/>PercentileBase<a></b><br>PercentileBase is the base class of all <a href="/documentation/code/classes/classrivet_1_1percentile/">Percentile</a> classes. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1percentileprojection/>PercentileProjection<a></b><br>class for projections that reports the percentile for a given SingleValueProjection when initialized with a Histo1D of the distribution in the SingleValueProjection. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1percentiletbase/>PercentileTBase<a></b><br>PercentileTBase is the base class of all <a href="/documentation/code/classes/classrivet_1_1percentile/">Percentile</a> classes. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1percentilexaxis/>PercentileXaxis<a></b><br>The PercentileXaxis class for centrality binning. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1piderror/>PidError<a></b><br><a href="/documentation/code/classes/structrivet_1_1error/">Error</a> specialisation for failures relating to particle ID codes. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1primaryhadrons/>PrimaryHadrons<a></b><br>Project out the first hadrons from hadronisation. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1primaryparticles/>PrimaryParticles<a></b><br>Project out primary particles according to definition. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1projection/>Projection<a></b><br>Base class for all <a href="/documentation/code/namespaces/namespacerivet/">Rivet</a> projections. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1projectionapplier/>ProjectionApplier<a></b><br>Common base class for <a href="/documentation/code/classes/classrivet_1_1projection/">Projection</a> and <a href="/documentation/code/classes/classrivet_1_1analysis/">Analysis</a>, used for internal polymorphism. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1projectionhandler/>ProjectionHandler<a></b><br>The projection handler is a central repository for projections to be used in a <a href="/documentation/code/namespaces/namespacerivet/">Rivet</a> analysis run. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1promptfinalstate/>PromptFinalState<a></b><br>Find final state particles directly connected to the hard process. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1ptgtr/>PtGtr<a></b><br>Transverse momentum greater-than functor. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1ptinrange/>PtInRange<a></b><br>Transverse momentum in-range functor. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1ptless/>PtLess<a></b><br>Transverse momentum less-than functor. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1rangeerror/>RangeError<a></b><br><a href="/documentation/code/classes/structrivet_1_1error/">Error</a> for e.g. use of invalid bin ranges. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1rapgtr/>RapGtr<a></b><br>Rapidity greater-than functor. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1rapinrange/>RapInRange<a></b><br>Rapidity in-range functor. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1rapless/>RapLess<a></b><br>Rapidity momentum less-than functor. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1readerror/>ReadError<a></b><br><a href="/documentation/code/classes/structrivet_1_1error/">Error</a> for read failures. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1run/>Run<a></b><br>Interface to handle a run of events read from a HepMC stream or file. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1star__bes__centrality/>STAR_BES_Centrality<a></b><br>Common projections for RHIC experiments' trigger conditions and centrality. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1singlevalueprojection/>SingleValueProjection<a></b><br>Base class for projections returning a single floating point value. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1smearedjets/>SmearedJets<a></b><br>Wrapper projection for smearing <code><a href="/documentation/code/classes/classrivet_1_1jet/">Jet</a></code>s with detector resolutions and efficiencies. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1smearedmet/>SmearedMET<a></b><br>Wrapper projection for smearing missing (transverse) energy/momentum with detector resolutions. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1smearedparticles/>SmearedParticles<a></b><br>Wrapper projection for smearing <code><a href="/documentation/code/classes/classrivet_1_1jet/">Jet</a></code>s with detector resolutions and efficiencies. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1sphericity/>Sphericity<a></b><br>Calculate the sphericity event shape. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1spherocity/>Spherocity<a></b><br>Get the transverse spherocity scalars for hadron-colliders. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1taufinder/>TauFinder<a></b><br>Convenience finder of unstable taus. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1threemomentum/>ThreeMomentum<a></b><br>Specialized version of the ThreeVector with momentum functionality. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1thrust/>Thrust<a></b><br>Get the e+ e- thrust basis and the thrust, thrust major and thrust minor scalars. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1triggercdfrun0run1/>TriggerCDFRun0Run1<a></b><br>Access to the min bias triggers used by CDF in <a href="/documentation/code/classes/classrivet_1_1run/">Run</a> 0 and <a href="/documentation/code/classes/classrivet_1_1run/">Run</a> 1. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1triggercdfrun2/>TriggerCDFRun2<a></b><br>Access to the min bias triggers used by CDF in <a href="/documentation/code/classes/classrivet_1_1run/">Run</a> 0 and <a href="/documentation/code/classes/classrivet_1_1run/">Run</a> 1. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1triggerprojection/>TriggerProjection<a></b><br>Base class for projections returning a bool corresponding to a trigger. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1triggerua5/>TriggerUA5<a></b><br>Access to the min bias triggers used by UA5. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1undressbeamleptons/>UndressBeamLeptons<a></b><br>Project out the incoming beams, but subtract any colinear photons from lepton beams within a given cone. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1unstableparticles/>UnstableParticles<a></b><br>Project out all physical-but-decayed particles in an event. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1usercentestimate/>UserCentEstimate<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1usererror/>UserError<a></b><br><a href="/documentation/code/classes/structrivet_1_1error/">Error</a> specialisation for where the problem is between the chair and the computer. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1vector/>Vector<a></b><br>A minimal base class for \( N \)-dimensional vectors. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1vector2/>Vector2<a></b><br>Two-dimensional specialisation of <a href="/documentation/code/classes/classrivet_1_1vector/">Vector</a>. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1vector3/>Vector3<a></b><br>Three-dimensional specialisation of <a href="/documentation/code/classes/classrivet_1_1vector/">Vector</a>. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1vetoedfinalstate/>VetoedFinalState<a></b><br>FS modifier to exclude classes of particles from the final state. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1visiblefinalstate/>VisibleFinalState<a></b><br>Final state modifier excluding particles which are not experimentally visible. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1wfinder/>WFinder<a></b><br>Convenience finder of leptonically decaying W. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1weighterror/>WeightError<a></b><br>Errors relating to event/bin weights. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1writeerror/>WriteError<a></b><br><a href="/documentation/code/classes/structrivet_1_1error/">Error</a> for write failures. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>class <a href=/documentation/code/classes/classrivet_1_1zfinder/>ZFinder<a></b><br>Convenience finder of leptonically decaying Zs. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>struct <a href=/documentation/code/classes/structrivet_1_1bad__lexical__cast/>bad_lexical_cast<a></b><br>Exception class for throwing from lexical_cast when a parse goes wrong. <br></details>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>namespace <a href=/documentation/code/namespaces/namespacerivet_1_1_0d93/>@93<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>namespace <a href=/documentation/code/namespaces/namespaceyoda/>YODA<a></b><br>
<details><summary><b>namespace <a href=/documentation/code/namespaces/namespacefastjet/>fastjet<a></b></summary>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>namespace <a href=/documentation/code/namespaces/namespacefastjet_1_1jetdefinition/>JetDefinition<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>namespace <a href=/documentation/code/namespaces/namespacefastjet_1_1contrib/>contrib<a></b><br></details>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>namespace <a href=/documentation/code/namespaces/namespacestd/>std<a></b><br>STL namespace. <br>




-------------------------------

Updated on 2022-08-07 at 20:17:18 +0100
