---

title: 'class Rivet::Analysis::CounterAdapter'
description: "To be used in finalize context only. "

---

# Rivet::Analysis::CounterAdapter



To be used in finalize context only. 

## Public Functions

|                | Name           |
| -------------- | -------------- |
| | **[CounterAdapter](/documentation/code/classes/classrivet_1_1analysis_1_1counteradapter/#function-counteradapter)**(double x) |
| | **[CounterAdapter](/documentation/code/classes/classrivet_1_1analysis_1_1counteradapter/#function-counteradapter)**(const YODA::Counter & c) |
| | **[CounterAdapter](/documentation/code/classes/classrivet_1_1analysis_1_1counteradapter/#function-counteradapter)**(const YODA::Scatter1D & s) |
| | **[operator double](/documentation/code/classes/classrivet_1_1analysis_1_1counteradapter/#function-operator-double)**() const |

## Public Functions Documentation

### function CounterAdapter

```cpp
inline CounterAdapter(
    double x
)
```


### function CounterAdapter

```cpp
inline CounterAdapter(
    const YODA::Counter & c
)
```


### function CounterAdapter

```cpp
inline CounterAdapter(
    const YODA::Scatter1D & s
)
```


### function operator double

```cpp
inline operator double() const
```


-------------------------------

Updated on 2022-08-07 at 20:17:16 +0100
