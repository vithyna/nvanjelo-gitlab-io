---

title: 'struct Rivet::FirstParticleWithout'
description: "Determine whether a particle is the first in a decay chain not to meet the cut/function. "

---

# Rivet::FirstParticleWithout

**Module:** **[Particle classifier -> bool functors](/documentation/code/modules/group__particleutils__p2bool/)**



Determine whether a particle is the first in a decay chain not to meet the cut/function. 


`#include <ParticleUtils.hh>`

Inherits from [Rivet::BoolParticleFunctor](/documentation/code/classes/structrivet_1_1boolparticlefunctor/)

## Public Functions

|                | Name           |
| -------------- | -------------- |
| | **[FirstParticleWithout](/documentation/code/classes/structrivet_1_1firstparticlewithout/#function-firstparticlewithout)**(const ParticleSelector & f) |
| | **[FirstParticleWithout](/documentation/code/classes/structrivet_1_1firstparticlewithout/#function-firstparticlewithout)**(const Cut & c) |
| virtual bool | **[operator()](/documentation/code/classes/structrivet_1_1firstparticlewithout/#function-operator())**(const <a href="/documentation/code/classes/classrivet_1_1particle/">Particle</a> & p) const |

## Public Attributes

|                | Name           |
| -------------- | -------------- |
| ParticleSelector | **[fn](/documentation/code/classes/structrivet_1_1firstparticlewithout/#variable-fn)**  |

## Additional inherited members

**Public Functions inherited from [Rivet::BoolParticleFunctor](/documentation/code/classes/structrivet_1_1boolparticlefunctor/)**

|                | Name           |
| -------------- | -------------- |
| virtual | **[~BoolParticleFunctor](/documentation/code/classes/structrivet_1_1boolparticlefunctor/#function-~boolparticlefunctor)**() |


## Public Functions Documentation

### function FirstParticleWithout

```cpp
inline FirstParticleWithout(
    const ParticleSelector & f
)
```


### function FirstParticleWithout

```cpp
FirstParticleWithout(
    const Cut & c
)
```


### function operator()

```cpp
inline virtual bool operator()(
    const Particle & p
) const
```


**Reimplements**: [Rivet::BoolParticleFunctor::operator()](/documentation/code/classes/structrivet_1_1boolparticlefunctor/#function-operator())


## Public Attributes Documentation

### variable fn

```cpp
ParticleSelector fn;
```


-------------------------------

Updated on 2022-08-07 at 20:17:17 +0100
