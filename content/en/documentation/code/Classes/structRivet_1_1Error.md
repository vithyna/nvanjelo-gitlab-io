---

title: 'struct Rivet::Error'
description: "Generic runtime Rivet error. "

---

# Rivet::Error



Generic runtime <a href="/documentation/code/namespaces/namespacerivet/">Rivet</a> error. 


`#include <Exceptions.hh>`

Inherits from std::runtime_error

Inherited by [Rivet::IOError](/documentation/code/classes/structrivet_1_1ioerror/), [Rivet::InfoError](/documentation/code/classes/structrivet_1_1infoerror/), [Rivet::LogicError](/documentation/code/classes/structrivet_1_1logicerror/), [Rivet::LookupError](/documentation/code/classes/structrivet_1_1lookuperror/), [Rivet::PidError](/documentation/code/classes/structrivet_1_1piderror/), [Rivet::RangeError](/documentation/code/classes/structrivet_1_1rangeerror/), [Rivet::UserError](/documentation/code/classes/structrivet_1_1usererror/), [Rivet::WeightError](/documentation/code/classes/structrivet_1_1weighterror/)

## Public Functions

|                | Name           |
| -------------- | -------------- |
| | **[Error](/documentation/code/classes/structrivet_1_1error/#function-error)**(const std::string & what) |

## Public Functions Documentation

### function Error

```cpp
inline Error(
    const std::string & what
)
```


-------------------------------

Updated on 2022-08-07 at 20:17:16 +0100
