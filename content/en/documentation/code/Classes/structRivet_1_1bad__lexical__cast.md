---

title: 'struct Rivet::bad_lexical_cast'
description: "Exception class for throwing from lexical_cast when a parse goes wrong. "

---

# Rivet::bad_lexical_cast

**Module:** **[String utils](/documentation/code/modules/group__strutils/)**



Exception class for throwing from lexical_cast when a parse goes wrong. 


`#include <Utils.hh>`

Inherits from std::runtime_error

## Public Functions

|                | Name           |
| -------------- | -------------- |
| | **[bad_lexical_cast](/documentation/code/classes/structrivet_1_1bad__lexical__cast/#function-bad-lexical-cast)**(const std::string & what) |

## Public Functions Documentation

### function bad_lexical_cast

```cpp
inline bad_lexical_cast(
    const std::string & what
)
```


-------------------------------

Updated on 2022-08-07 at 20:17:17 +0100
