---

title: 'struct Rivet::BoolParticleAND'
description: "Functor for and-combination of selector logic. "

---

# Rivet::BoolParticleAND

**Module:** **[Particle classifier -> bool functors](/documentation/code/modules/group__particleutils__p2bool/)**



Functor for and-combination of selector logic. 


`#include <ParticleUtils.hh>`

Inherits from [Rivet::BoolParticleFunctor](/documentation/code/classes/structrivet_1_1boolparticlefunctor/)

## Public Functions

|                | Name           |
| -------------- | -------------- |
| | **[BoolParticleAND](/documentation/code/classes/structrivet_1_1boolparticleand/#function-boolparticleand)**(const std::vector< ParticleSelector > & sels) |
| | **[BoolParticleAND](/documentation/code/classes/structrivet_1_1boolparticleand/#function-boolparticleand)**(const ParticleSelector & a, const ParticleSelector & b) |
| | **[BoolParticleAND](/documentation/code/classes/structrivet_1_1boolparticleand/#function-boolparticleand)**(const ParticleSelector & a, const ParticleSelector & b, const ParticleSelector & c) |
| virtual bool | **[operator()](/documentation/code/classes/structrivet_1_1boolparticleand/#function-operator())**(const <a href="/documentation/code/classes/classrivet_1_1particle/">Particle</a> & p) const |

## Public Attributes

|                | Name           |
| -------------- | -------------- |
| std::vector< ParticleSelector > | **[selectors](/documentation/code/classes/structrivet_1_1boolparticleand/#variable-selectors)**  |

## Additional inherited members

**Public Functions inherited from [Rivet::BoolParticleFunctor](/documentation/code/classes/structrivet_1_1boolparticlefunctor/)**

|                | Name           |
| -------------- | -------------- |
| virtual | **[~BoolParticleFunctor](/documentation/code/classes/structrivet_1_1boolparticlefunctor/#function-~boolparticlefunctor)**() |


## Public Functions Documentation

### function BoolParticleAND

```cpp
inline BoolParticleAND(
    const std::vector< ParticleSelector > & sels
)
```


### function BoolParticleAND

```cpp
inline BoolParticleAND(
    const ParticleSelector & a,
    const ParticleSelector & b
)
```


### function BoolParticleAND

```cpp
inline BoolParticleAND(
    const ParticleSelector & a,
    const ParticleSelector & b,
    const ParticleSelector & c
)
```


### function operator()

```cpp
inline virtual bool operator()(
    const Particle & p
) const
```


**Reimplements**: [Rivet::BoolParticleFunctor::operator()](/documentation/code/classes/structrivet_1_1boolparticlefunctor/#function-operator())


## Public Attributes Documentation

### variable selectors

```cpp
std::vector< ParticleSelector > selectors;
```


-------------------------------

Updated on 2022-08-07 at 20:17:17 +0100
