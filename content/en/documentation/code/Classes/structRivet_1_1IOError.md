---

title: 'struct Rivet::IOError'
description: "Error for I/O failures. "

---

# Rivet::IOError



<a href="/documentation/code/classes/structrivet_1_1error/">Error</a> for I/O failures. 


`#include <Exceptions.hh>`

Inherits from [Rivet::Error](/documentation/code/classes/structrivet_1_1error/), std::runtime_error

Inherited by [Rivet::ReadError](/documentation/code/classes/structrivet_1_1readerror/), [Rivet::WriteError](/documentation/code/classes/structrivet_1_1writeerror/)

## Public Functions

|                | Name           |
| -------------- | -------------- |
| | **[IOError](/documentation/code/classes/structrivet_1_1ioerror/#function-ioerror)**(const std::string & what) |

## Additional inherited members

**Public Functions inherited from [Rivet::Error](/documentation/code/classes/structrivet_1_1error/)**

|                | Name           |
| -------------- | -------------- |
| | **[Error](/documentation/code/classes/structrivet_1_1error/#function-error)**(const std::string & what) |


## Public Functions Documentation

### function IOError

```cpp
inline IOError(
    const std::string & what
)
```


-------------------------------

Updated on 2022-08-07 at 20:17:17 +0100
