---

title: 'struct Rivet::InfoError'
description: "Error specialisation for failures relating to analysis info. "

---

# Rivet::InfoError



<a href="/documentation/code/classes/structrivet_1_1error/">Error</a> specialisation for failures relating to analysis info. 


`#include <Exceptions.hh>`

Inherits from [Rivet::Error](/documentation/code/classes/structrivet_1_1error/), std::runtime_error

## Public Functions

|                | Name           |
| -------------- | -------------- |
| | **[InfoError](/documentation/code/classes/structrivet_1_1infoerror/#function-infoerror)**(const std::string & what) |

## Additional inherited members

**Public Functions inherited from [Rivet::Error](/documentation/code/classes/structrivet_1_1error/)**

|                | Name           |
| -------------- | -------------- |
| | **[Error](/documentation/code/classes/structrivet_1_1error/#function-error)**(const std::string & what) |


## Public Functions Documentation

### function InfoError

```cpp
inline InfoError(
    const std::string & what
)
```


-------------------------------

Updated on 2022-08-07 at 20:17:17 +0100
