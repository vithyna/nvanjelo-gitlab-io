---

title: 'struct Rivet::P4_EFF_CONST'
description: "Take a FourMomentum and return a constant number. "

---

# Rivet::P4_EFF_CONST

**Module:** **[Detector smearing & efficiency functions](/documentation/code/modules/group__smearing/)** **/** **[Generic 4-momentum filtering, efficiency and smearing utils](/documentation/code/modules/group__smearing__mom/)**



Take a <a href="/documentation/code/classes/classrivet_1_1fourmomentum/">FourMomentum</a> and return a constant number. 


`#include <MomentumSmearingFunctions.hh>`

## Public Functions

|                | Name           |
| -------------- | -------------- |
| | **[P4_EFF_CONST](/documentation/code/classes/structrivet_1_1p4__eff__const/#function-p4-eff-const)**(double x) |
| double | **[operator()](/documentation/code/classes/structrivet_1_1p4__eff__const/#function-operator())**(const <a href="/documentation/code/classes/classrivet_1_1fourmomentum/">FourMomentum</a> & ) const |

## Public Functions Documentation

### function P4_EFF_CONST

```cpp
inline P4_EFF_CONST(
    double x
)
```


### function operator()

```cpp
inline double operator()(
    const FourMomentum & 
) const
```


-------------------------------

Updated on 2022-08-07 at 20:17:18 +0100
