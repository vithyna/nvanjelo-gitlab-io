---

title: 'namespace Rivet::Kin'

description: "[Documentation update required.]"

---

# Rivet::Kin

**Module:** **[Functions for Jets](/documentation/code/modules/group__jetutils/)** **/** **[Operations on collections of Jet](/documentation/code/modules/group__jetutils__coll/)**



## Functions

|                | Name           |
| -------------- | -------------- |
| double | **[sumPt](/documentation/code/namespaces/namespacerivet_1_1kin/#function-sumpt)**(const <a href="/documentation/code/classes/classrivet_1_1jets/">Jets</a> & js) |
| <a href="/documentation/code/classes/classrivet_1_1fourmomentum/">FourMomentum</a> | **[sumP4](/documentation/code/namespaces/namespacerivet_1_1kin/#function-sump4)**(const <a href="/documentation/code/classes/classrivet_1_1jets/">Jets</a> & js) |
| <a href="/documentation/code/classes/classrivet_1_1vector3/">Vector3</a> | **[sumP3](/documentation/code/namespaces/namespacerivet_1_1kin/#function-sump3)**(const <a href="/documentation/code/classes/classrivet_1_1jets/">Jets</a> & js) |
| <a href="/documentation/code/classes/classrivet_1_1fourmomentum/">FourMomentum</a> | **[mom](/documentation/code/modules/group__particlebaseutils/#function-mom)**(const <a href="/documentation/code/classes/classrivet_1_1particlebase/">ParticleBase</a> & p)<br>Unbound function access to momentum.  |
| <a href="/documentation/code/classes/classrivet_1_1fourmomentum/">FourMomentum</a> | **[p4](/documentation/code/modules/group__particlebaseutils/#function-p4)**(const <a href="/documentation/code/classes/classrivet_1_1particlebase/">ParticleBase</a> & p)<br>Unbound function access to momentum.  |
| <a href="/documentation/code/classes/classrivet_1_1vector3/">Vector3</a> | **[p3](/documentation/code/modules/group__particlebaseutils/#function-p3)**(const <a href="/documentation/code/classes/classrivet_1_1particlebase/">ParticleBase</a> & p)<br>Unbound function access to p3.  |
| <a href="/documentation/code/classes/classrivet_1_1vector3/">Vector3</a> | **[pTvec](/documentation/code/modules/group__particlebaseutils/#function-ptvec)**(const <a href="/documentation/code/classes/classrivet_1_1particlebase/">ParticleBase</a> & p)<br>Unbound function access to pTvec.  |
| double | **[p](/documentation/code/modules/group__particlebaseutils/#function-p)**(const <a href="/documentation/code/classes/classrivet_1_1particlebase/">ParticleBase</a> & p)<br>Unbound function access to p.  |
| double | **[pT](/documentation/code/modules/group__particlebaseutils/#function-pt)**(const <a href="/documentation/code/classes/classrivet_1_1particlebase/">ParticleBase</a> & p)<br>Unbound function access to pT.  |
| double | **[Et](/documentation/code/modules/group__particlebaseutils/#function-et)**(const <a href="/documentation/code/classes/classrivet_1_1particlebase/">ParticleBase</a> & p)<br>Unbound function access to ET.  |
| double | **[eta](/documentation/code/modules/group__particlebaseutils/#function-eta)**(const <a href="/documentation/code/classes/classrivet_1_1particlebase/">ParticleBase</a> & p)<br>Unbound function access to eta.  |
| double | **[abseta](/documentation/code/modules/group__particlebaseutils/#function-abseta)**(const <a href="/documentation/code/classes/classrivet_1_1particlebase/">ParticleBase</a> & p)<br>Unbound function access to abseta.  |
| double | **[rap](/documentation/code/modules/group__particlebaseutils/#function-rap)**(const <a href="/documentation/code/classes/classrivet_1_1particlebase/">ParticleBase</a> & p)<br>Unbound function access to rapidity.  |
| double | **[absrap](/documentation/code/modules/group__particlebaseutils/#function-absrap)**(const <a href="/documentation/code/classes/classrivet_1_1particlebase/">ParticleBase</a> & p)<br>Unbound function access to abs rapidity.  |
| double | **[mass](/documentation/code/modules/group__particlebaseutils/#function-mass)**(const <a href="/documentation/code/classes/classrivet_1_1particlebase/">ParticleBase</a> & p)<br>Unbound function access to mass.  |
| double | **[pairPt](/documentation/code/modules/group__particlebaseutils/#function-pairpt)**(const <a href="/documentation/code/classes/classrivet_1_1particlebase/">ParticleBase</a> & p1, const <a href="/documentation/code/classes/classrivet_1_1particlebase/">ParticleBase</a> & p2)<br>Unbound function access to pair pT.  |
| double | **[pairMass](/documentation/code/modules/group__particlebaseutils/#function-pairmass)**(const <a href="/documentation/code/classes/classrivet_1_1particlebase/">ParticleBase</a> & p1, const <a href="/documentation/code/classes/classrivet_1_1particlebase/">ParticleBase</a> & p2)<br>Unbound function access to pair mass.  |
| double | **[sumPt](/documentation/code/namespaces/namespacerivet_1_1kin/#function-sumpt)**(const <a href="/documentation/code/classes/classrivet_1_1particles/">Particles</a> & ps) |
| <a href="/documentation/code/classes/classrivet_1_1fourmomentum/">FourMomentum</a> | **[sumP4](/documentation/code/namespaces/namespacerivet_1_1kin/#function-sump4)**(const <a href="/documentation/code/classes/classrivet_1_1particles/">Particles</a> & ps) |
| <a href="/documentation/code/classes/classrivet_1_1vector3/">Vector3</a> | **[sumP3](/documentation/code/namespaces/namespacerivet_1_1kin/#function-sump3)**(const <a href="/documentation/code/classes/classrivet_1_1particles/">Particles</a> & ps) |


## Functions Documentation

### function sumPt

```cpp
inline double sumPt(
    const Jets & js
)
```


### function sumP4

```cpp
inline FourMomentum sumP4(
    const Jets & js
)
```


### function sumP3

```cpp
inline Vector3 sumP3(
    const Jets & js
)
```


### function mom

```cpp
inline FourMomentum mom(
    const ParticleBase & p
)
```

Unbound function access to momentum. 

### function p4

```cpp
inline FourMomentum p4(
    const ParticleBase & p
)
```

Unbound function access to momentum. 

### function p3

```cpp
inline Vector3 p3(
    const ParticleBase & p
)
```

Unbound function access to p3. 

### function pTvec

```cpp
inline Vector3 pTvec(
    const ParticleBase & p
)
```

Unbound function access to pTvec. 

### function p

```cpp
inline double p(
    const ParticleBase & p
)
```

Unbound function access to p. 

### function pT

```cpp
inline double pT(
    const ParticleBase & p
)
```

Unbound function access to pT. 

### function Et

```cpp
inline double Et(
    const ParticleBase & p
)
```

Unbound function access to ET. 

### function eta

```cpp
inline double eta(
    const ParticleBase & p
)
```

Unbound function access to eta. 

### function abseta

```cpp
inline double abseta(
    const ParticleBase & p
)
```

Unbound function access to abseta. 

### function rap

```cpp
inline double rap(
    const ParticleBase & p
)
```

Unbound function access to rapidity. 

### function absrap

```cpp
inline double absrap(
    const ParticleBase & p
)
```

Unbound function access to abs rapidity. 

### function mass

```cpp
inline double mass(
    const ParticleBase & p
)
```

Unbound function access to mass. 

### function pairPt

```cpp
inline double pairPt(
    const ParticleBase & p1,
    const ParticleBase & p2
)
```

Unbound function access to pair pT. 

### function pairMass

```cpp
inline double pairMass(
    const ParticleBase & p1,
    const ParticleBase & p2
)
```

Unbound function access to pair mass. 

### function sumPt

```cpp
inline double sumPt(
    const Particles & ps
)
```


### function sumP4

```cpp
inline FourMomentum sumP4(
    const Particles & ps
)
```


### function sumP3

```cpp
inline Vector3 sumP3(
    const Particles & ps
)
```






-------------------------------

Updated on 2022-08-07 at 20:17:18 +0100
