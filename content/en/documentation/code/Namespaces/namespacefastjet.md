---

title: 'namespace fastjet'

description: "[Documentation update required.]"

---

# fastjet



## Namespaces

| Name           |
| -------------- |
| **[fastjet::contrib](/documentation/code/namespaces/namespacefastjet_1_1contrib/)**  |
| **[fastjet::JetDefinition](/documentation/code/namespaces/namespacefastjet_1_1jetdefinition/)**  |






-------------------------------

Updated on 2022-08-07 at 20:17:17 +0100
