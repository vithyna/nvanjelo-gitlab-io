---

title: 'namespace YODA'

description: "[Documentation update required.]"

---

# YODA



## Types

|                | Name           |
| -------------- | -------------- |
| typedef std::shared_ptr< YODA::AnalysisObject > | **[AnalysisObjectPtr](/documentation/code/namespaces/namespaceyoda/#typedef-analysisobjectptr)**  |
| typedef std::shared_ptr< YODA::Counter > | **[CounterPtr](/documentation/code/namespaces/namespaceyoda/#typedef-counterptr)**  |
| typedef std::shared_ptr< YODA::Histo1D > | **[Histo1DPtr](/documentation/code/namespaces/namespaceyoda/#typedef-histo1dptr)**  |
| typedef std::shared_ptr< YODA::Histo2D > | **[Histo2DPtr](/documentation/code/namespaces/namespaceyoda/#typedef-histo2dptr)**  |
| typedef std::shared_ptr< YODA::Profile1D > | **[Profile1DPtr](/documentation/code/namespaces/namespaceyoda/#typedef-profile1dptr)**  |
| typedef std::shared_ptr< YODA::Profile2D > | **[Profile2DPtr](/documentation/code/namespaces/namespaceyoda/#typedef-profile2dptr)**  |
| typedef std::shared_ptr< YODA::Scatter1D > | **[Scatter1DPtr](/documentation/code/namespaces/namespaceyoda/#typedef-scatter1dptr)**  |
| typedef std::shared_ptr< YODA::Scatter2D > | **[Scatter2DPtr](/documentation/code/namespaces/namespaceyoda/#typedef-scatter2dptr)**  |
| typedef std::shared_ptr< YODA::Scatter3D > | **[Scatter3DPtr](/documentation/code/namespaces/namespaceyoda/#typedef-scatter3dptr)**  |

## Types Documentation

### typedef AnalysisObjectPtr

```cpp
typedef std::shared_ptr<YODA::AnalysisObject> YODA::AnalysisObjectPtr;
```


### typedef CounterPtr

```cpp
typedef std::shared_ptr<YODA::Counter> YODA::CounterPtr;
```


### typedef Histo1DPtr

```cpp
typedef std::shared_ptr<YODA::Histo1D> YODA::Histo1DPtr;
```


### typedef Histo2DPtr

```cpp
typedef std::shared_ptr<YODA::Histo2D> YODA::Histo2DPtr;
```


### typedef Profile1DPtr

```cpp
typedef std::shared_ptr<YODA::Profile1D> YODA::Profile1DPtr;
```


### typedef Profile2DPtr

```cpp
typedef std::shared_ptr<YODA::Profile2D> YODA::Profile2DPtr;
```


### typedef Scatter1DPtr

```cpp
typedef std::shared_ptr<YODA::Scatter1D> YODA::Scatter1DPtr;
```


### typedef Scatter2DPtr

```cpp
typedef std::shared_ptr<YODA::Scatter2D> YODA::Scatter2DPtr;
```


### typedef Scatter3DPtr

```cpp
typedef std::shared_ptr<YODA::Scatter3D> YODA::Scatter3DPtr;
```







-------------------------------

Updated on 2022-08-07 at 20:17:16 +0100
