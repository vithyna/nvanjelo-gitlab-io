---
title: "Namespaces"

menu:
  documentation:
    parent: "code"
weight: 50
---


&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>namespace <a href=/documentation/code/namespaces/namespacehepmc3/>HepMC3<a></b><br>
<details><summary><b>namespace <a href=/documentation/code/namespaces/namespacerivet/>Rivet<a></b></summary>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>namespace <a href=/documentation/code/namespaces/namespacerivet_1_1alice/>ALICE<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>namespace <a href=/documentation/code/namespaces/namespacerivet_1_1atlas/>ATLAS<a></b><br>Common projections for ATLAS trigger conditions and centrality. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>namespace <a href=/documentation/code/namespaces/namespacerivet_1_1cuts/>Cuts<a></b><br>Namespace used for ambiguous identifiers. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>namespace <a href=/documentation/code/namespaces/namespacerivet_1_1hepmcutils/>HepMCUtils<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>namespace <a href=/documentation/code/namespaces/namespacerivet_1_1kin/>Kin<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>namespace <a href=/documentation/code/namespaces/namespacerivet_1_1pid/>PID<a></b><br></details>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>namespace <a href=/documentation/code/namespaces/namespacerivet_1_1_0d93/>@93<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>namespace <a href=/documentation/code/namespaces/namespaceyoda/>YODA<a></b><br>
<details><summary><b>namespace <a href=/documentation/code/namespaces/namespacefastjet/>fastjet<a></b></summary>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>namespace <a href=/documentation/code/namespaces/namespacefastjet_1_1jetdefinition/>JetDefinition<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>namespace <a href=/documentation/code/namespaces/namespacefastjet_1_1contrib/>contrib<a></b><br></details>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>namespace <a href=/documentation/code/namespaces/namespacestd/>std<a></b><br>STL namespace. <br>




-------------------------------

Updated on 2022-08-07 at 20:17:18 +0100
