---

title: 'namespace Rivet::ATLAS'
description: "Common projections for ATLAS trigger conditions and centrality. "

---

# Rivet::ATLAS

Common projections for ATLAS trigger conditions and centrality. 

## Classes

|                | Name           |
| -------------- | -------------- |
| class | **[Rivet::ATLAS::MinBiasTrigger](/documentation/code/classes/classrivet_1_1atlas_1_1minbiastrigger/)** <br><a href="/documentation/code/namespaces/namespacerivet_1_1atlas/">ATLAS</a> min bias trigger conditions.  |
| class | **[Rivet::ATLAS::SumET_PB_Centrality](/documentation/code/classes/classrivet_1_1atlas_1_1sumet__pb__centrality/)** <br>Centrality projection for pPb collisions (one sided)  |
| class | **[Rivet::ATLAS::SumET_PBPB_Centrality](/documentation/code/classes/classrivet_1_1atlas_1_1sumet__pbpb__centrality/)** <br>Centrality projection for PbPb collisions (two sided)  |






-------------------------------

Updated on 2022-08-07 at 20:17:17 +0100
