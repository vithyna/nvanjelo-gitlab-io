---

title: 'group Detector smearing & efficiency functions'

description: "[Documentation update required.]"

---

# Detector smearing & efficiency functions



## Modules

| Name           |
| -------------- |
| **[Experiment-specific electron efficiency and smearing functions](/documentation/code/modules/group__smearing__elec/)**  |
| **[Experiment-specific photon efficiency and smearing functions](/documentation/code/modules/group__smearing__photon/)**  |
| **[Experiment-specific muon efficiency and smearing functions](/documentation/code/modules/group__smearing__muon/)**  |
| **[Experiment-specific tau efficiency and smearing functions](/documentation/code/modules/group__smearing__tau/)**  |
| **[Experiment-specific jet efficiency and smearing functions](/documentation/code/modules/group__smearing__jet/)**  |
| **[Experiment-specific missing-ET smearing functions](/documentation/code/modules/group__smearing__met/)**  |
| **[Experiment-specific tracking efficiency and smearing functions](/documentation/code/modules/group__smearing__trk/)**  |
| **[Generic jet filtering, efficiency and smearing utils](/documentation/code/modules/group__smearing__particle/)**  |
| **[Generic 4-momentum filtering, efficiency and smearing utils](/documentation/code/modules/group__smearing__mom/)**  |






-------------------------------

Updated on 2022-08-07 at 20:17:18 +0100
