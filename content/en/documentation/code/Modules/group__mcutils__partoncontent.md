---

title: 'group Parton content functions'

description: "[Documentation update required.]"

---

# Parton content functions



## Functions

|                | Name           |
| -------------- | -------------- |
| bool | **[hasDown](/documentation/code/modules/group__mcutils__partoncontent/#function-hasdown)**(int pid)<br>Does this particle contain a down quark?  |
| bool | **[hasUp](/documentation/code/modules/group__mcutils__partoncontent/#function-hasup)**(int pid)<br>Does this particle contain an up quark?  |
| bool | **[hasStrange](/documentation/code/modules/group__mcutils__partoncontent/#function-hasstrange)**(int pid)<br>Does this particle contain a strange quark?  |
| bool | **[hasCharm](/documentation/code/modules/group__mcutils__partoncontent/#function-hascharm)**(int pid)<br>Does this particle contain a charm quark?  |
| bool | **[hasBottom](/documentation/code/modules/group__mcutils__partoncontent/#function-hasbottom)**(int pid)<br>Does this particle contain a bottom quark?  |
| bool | **[hasTop](/documentation/code/modules/group__mcutils__partoncontent/#function-hastop)**(int pid)<br>Does this particle contain a top quark?  |


## Functions Documentation

### function hasDown

```
inline bool hasDown(
    int pid
)
```

Does this particle contain a down quark? 

### function hasUp

```
inline bool hasUp(
    int pid
)
```

Does this particle contain an up quark? 

### function hasStrange

```
inline bool hasStrange(
    int pid
)
```

Does this particle contain a strange quark? 

### function hasCharm

```
inline bool hasCharm(
    int pid
)
```

Does this particle contain a charm quark? 

### function hasBottom

```
inline bool hasBottom(
    int pid
)
```

Does this particle contain a bottom quark? 

### function hasTop

```
inline bool hasTop(
    int pid
)
```

Does this particle contain a top quark? 





-------------------------------

Updated on 2022-08-07 at 20:17:17 +0100
