---

title: 'group Run conditions'

description: "[Documentation update required.]"

---

# Run conditions



## Functions

|                | Name           |
| -------------- | -------------- |
| const ParticlePair & | **[beams](/documentation/code/modules/group__analysis__run/#function-beams)**() const<br>Incoming beams for this run.  |
| const PdgIdPair | **[beamIds](/documentation/code/modules/group__analysis__run/#function-beamids)**() const<br>Incoming beam IDs for this run.  |
| double | **[sqrtS](/documentation/code/modules/group__analysis__run/#function-sqrts)**() const<br>Centre of mass energy for this run.  |
| bool | **[merging](/documentation/code/modules/group__analysis__run/#function-merging)**() const<br>Check if we are running rivet-merge.  |


## Functions Documentation

### function beams

```
const ParticlePair & beams() const
```

Incoming beams for this run. 

### function beamIds

```
const PdgIdPair beamIds() const
```

Incoming beam IDs for this run. 

### function sqrtS

```
double sqrtS() const
```

Centre of mass energy for this run. 

### function merging

```
inline bool merging() const
```

Check if we are running rivet-merge. 





-------------------------------

Updated on 2022-08-07 at 20:17:18 +0100
