---

title: 'group avoidf$avoidDelta phiavoidf$ calculations from 3-vectors'

description: "[Documentation update required.]"

---

# \f$\Delta phi\f$ calculations from 3-vectors



## Functions

|                | Name           |
| -------------- | -------------- |
| double | **[deltaPhi](/documentation/code/modules/group__momutils__vec3__dphi/#function-deltaphi)**(const Vector3 & a, const Vector3 & b, bool sign =false)<br>Calculate the difference in azimuthal angle between two spatial vectors.  |
| double | **[deltaPhi](/documentation/code/modules/group__momutils__vec3__dphi/#function-deltaphi)**(const Vector3 & v, double phi2, bool sign =false)<br>Calculate the difference in azimuthal angle between two spatial vectors.  |
| double | **[deltaPhi](/documentation/code/modules/group__momutils__vec3__dphi/#function-deltaphi)**(double phi1, const Vector3 & v, bool sign =false)<br>Calculate the difference in azimuthal angle between two spatial vectors.  |


## Functions Documentation

### function deltaPhi

```
inline double deltaPhi(
    const Vector3 & a,
    const Vector3 & b,
    bool sign =false
)
```

Calculate the difference in azimuthal angle between two spatial vectors. 

### function deltaPhi

```
inline double deltaPhi(
    const Vector3 & v,
    double phi2,
    bool sign =false
)
```

Calculate the difference in azimuthal angle between two spatial vectors. 

### function deltaPhi

```
inline double deltaPhi(
    double phi1,
    const Vector3 & v,
    bool sign =false
)
```

Calculate the difference in azimuthal angle between two spatial vectors. 





-------------------------------

Updated on 2022-08-07 at 20:17:18 +0100
