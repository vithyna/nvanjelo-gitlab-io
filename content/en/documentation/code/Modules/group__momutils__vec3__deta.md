---

title: 'group avoidf$ |avoidDelta eta|avoidf$ calculations from 3-vectors'

description: "[Documentation update required.]"

---

# \f$ |\Delta eta|\f$ calculations from 3-vectors



## Functions

|                | Name           |
| -------------- | -------------- |
| double | **[deltaEta](/documentation/code/modules/group__momutils__vec3__deta/#function-deltaeta)**(const Vector3 & a, const Vector3 & b, bool sign =false)<br>Calculate the difference in pseudorapidity between two spatial vectors.  |
| double | **[deltaEta](/documentation/code/modules/group__momutils__vec3__deta/#function-deltaeta)**(const Vector3 & v, double eta2, bool sign =false)<br>Calculate the difference in pseudorapidity between two spatial vectors.  |
| double | **[deltaEta](/documentation/code/modules/group__momutils__vec3__deta/#function-deltaeta)**(double eta1, const Vector3 & v, bool sign =false)<br>Calculate the difference in pseudorapidity between two spatial vectors.  |


## Functions Documentation

### function deltaEta

```
inline double deltaEta(
    const Vector3 & a,
    const Vector3 & b,
    bool sign =false
)
```

Calculate the difference in pseudorapidity between two spatial vectors. 

### function deltaEta

```
inline double deltaEta(
    const Vector3 & v,
    double eta2,
    bool sign =false
)
```

Calculate the difference in pseudorapidity between two spatial vectors. 

### function deltaEta

```
inline double deltaEta(
    double eta1,
    const Vector3 & v,
    bool sign =false
)
```

Calculate the difference in pseudorapidity between two spatial vectors. 





-------------------------------

Updated on 2022-08-07 at 20:17:18 +0100
