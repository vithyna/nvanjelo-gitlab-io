---
title: "Modules"

menu:
  documentation:
    parent: "code"
weight: 10
---


&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__alice/>ALICE specifics<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__anainfo__metadata/>Metadata<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__anainfo__options/>Analysis-options support<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__anainfo__status/>Status info and categories<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__analysis__aoaccess/>Data object registration, retrieval, and removal<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__analysis__beamcompat/>Analysis / beam compatibility testing<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__analysis__bookhi/>Booking heavy ion features<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__analysis__cbook/>Counter booking<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__analysis__h1book/>1D histogram booking<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__analysis__h2book/>2D histogram booking<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__analysis__histopaths/>Histogram paths<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__analysis__main/>Main analysis methods<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__analysis__manip/>Analysis object manipulation<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__analysis__meta/>Metadata<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__analysis__p1book/>1D profile histogram booking<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__analysis__p2book/>2D profile histogram booking<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__analysis__refdata/>Histogram reference data<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__analysis__run/>Run conditions<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__analysis__s2book/>2D scatter booking<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__analysis__s3book/>3D scatter booking<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__analysis__xsecvars/>Cross-section variables<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__anamacros/>Analysis macros<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__aomanip/>Analysis object manipulation functions<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__aotuples/>Minimal objects representing AO fills, to be buffered before pushToPersistent().<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__contutils/>Container utils<a></b><br>
<details><summary><b>group <a href=/documentation/code/modules/group__jetutils/>Functions for Jets<a></b></summary>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__jetutils__coll/>Operations on collections of Jet<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__jetutils__conv/>Converting between Jets, Particles and PseudoJets<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__jetutils__filt/>Unbound functions for filtering jets<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__jetutils__j2bool/>Jet classifier -> bool functors<a></b><br></details>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__logmacros/>Logging macros<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__mathutils/>Maths utilities<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__mcutils__angmom/>Angular momentum functions<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__mcutils__charge/>Charge functions<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__mcutils__charge__classes/>General PID-based classifier functions<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__mcutils__fundamental/>Fundamental particles<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__mcutils__idclasses/>More general particle class identification functions<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__mcutils__interactions/>Interaction classifiers<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__mcutils__nucleus__ion/>Nucleus/ion functions<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__mcutils__other/>Other classifiers<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__mcutils__parton__classes/>Parton content classification<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__mcutils__partoncontent/>Parton content functions<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__mcutils__qcomp/>Quark composite functions<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__mcutils__utils/>Utility functions<a></b><br>
<details><summary><b>group <a href=/documentation/code/modules/group__momutils/>Functions for 4-momenta<a></b></summary>
<details><summary><b>group <a href=/documentation/code/modules/group__momutils__cmp/>4-vector comparison functions (for sorting)<a></b></summary>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__momutils__mt/>MT calculation<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__momutils__str/>4-vector string representations<a></b><br></details></details>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__momutils__vec3__deta/>\f$ |\Delta eta|\f$ calculations from 3-vectors<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__momutils__vec3__dphi/>\f$\Delta phi\f$ calculations from 3-vectors<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__momutils__vec3__dr/>\f$\Delta R\f$ calculations from 3-vectors<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__momutils__vec3__mt/>MT calculation<a></b><br>
<details><summary><b>group <a href=/documentation/code/modules/group__particlebaseutils/>Functions for Particles and Jets<a></b></summary>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__particlebasetutils__pb2bool/>ParticleBase classifier -> bool functors<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__particlebaseutils__pb2dbl/>ParticleBase comparison -> double functors<a></b><br>
<details><summary><b>group <a href=/documentation/code/modules/group__particlebaseutils__uberfilt/>Next-level filtering<a></b></summary>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__particlebaseutils__iso/>Isolation helpers<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__particlebaseutils__kin/>Unbound functions for kinematic properties<a></b><br></details></details>
<details><summary><b>group <a href=/documentation/code/modules/group__particleutils/>Functions for Particles<a></b></summary>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__particleutils__class/>Particle classifier functions<a></b><br></details>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__particleutils__charge/>Particle charge/sign comparison functions<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__particleutils__filt/>Unbound functions for filtering particles<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__particleutils__kin/>Operations on collections of Particle<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__particleutils__nonpid/>Non-PID particle properties, via unbound functions<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__particleutils__p2bool/>Particle classifier -> bool functors<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__particleutils__pair/>Particle pair functions<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__particleutils__pairclass/>Particle pair classifiers<a></b><br>Is this particle species charged? <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__pathutils/>Path utils<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__ppair__class/>Particle pair classifiers<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__rivetpaths/>Rivet file searching utils<a></b><br>
<details><summary><b>group <a href=/documentation/code/modules/group__smearing/>Detector smearing & efficiency functions<a></b></summary>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__smearing__elec/>Experiment-specific electron efficiency and smearing functions<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__smearing__jet/>Experiment-specific jet efficiency and smearing functions<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__smearing__met/>Experiment-specific missing-ET smearing functions<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__smearing__mom/>Generic 4-momentum filtering, efficiency and smearing utils<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__smearing__muon/>Experiment-specific muon efficiency and smearing functions<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__smearing__particle/>Generic jet filtering, efficiency and smearing utils<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__smearing__photon/>Experiment-specific photon efficiency and smearing functions<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__smearing__tau/>Experiment-specific tau efficiency and smearing functions<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__smearing__trk/>Experiment-specific tracking efficiency and smearing functions<a></b><br></details>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__strutils/>String utils<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__useraos/>User-facing analysis object wrappers<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>group <a href=/documentation/code/modules/group__utils/>Other utilities<a></b><br>




-------------------------------

Updated on 2022-08-07 at 20:17:18 +0100
