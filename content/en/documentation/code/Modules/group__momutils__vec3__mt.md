---

title: 'group MT calculation'

description: "[Documentation update required.]"

---

# MT calculation



## Functions

|                | Name           |
| -------------- | -------------- |
| double | **[mT](/documentation/code/modules/group__momutils__vec3__mt/#function-mt)**(const Vector3 & vis, const Vector3 & invis)<br>Calculate transverse mass of a visible and an invisible 3-vector.  |


## Functions Documentation

### function mT

```
inline double mT(
    const Vector3 & vis,
    const Vector3 & invis
)
```

Calculate transverse mass of a visible and an invisible 3-vector. 





-------------------------------

Updated on 2022-08-07 at 20:17:18 +0100
