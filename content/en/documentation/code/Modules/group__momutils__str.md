---

title: 'group 4-vector string representations'

description: "[Documentation update required.]"

---

# 4-vector string representations

**Module:** **[Functions for 4-momenta](/documentation/code/modules/group__momutils/)** **/** **[4-vector comparison functions (for sorting)](/documentation/code/modules/group__momutils__cmp/)**



## Functions

|                | Name           |
| -------------- | -------------- |
| std::string | **[toString](/documentation/code/modules/group__momutils__str/#function-tostring)**(const FourVector & lv)<br>Render a 4-vector as a string.  |
| std::ostream & | **[operator<<](/documentation/code/modules/group__momutils__str/#function-operator<<)**(std::ostream & out, const FourVector & lv)<br>Write a 4-vector to an ostream.  |


## Functions Documentation

### function toString

```
inline std::string toString(
    const FourVector & lv
)
```

Render a 4-vector as a string. 

### function operator<<

```
inline std::ostream & operator<<(
    std::ostream & out,
    const FourVector & lv
)
```

Write a 4-vector to an ostream. 





-------------------------------

Updated on 2022-08-07 at 20:17:18 +0100
