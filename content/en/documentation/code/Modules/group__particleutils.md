---

title: 'group Functions for Particles'

description: "[Documentation update required.]"

---

# Functions for Particles



## Modules

| Name           |
| -------------- |
| **[Particle classifier functions](/documentation/code/modules/group__particleutils__class/)**  |

## Functions

|                | Name           |
| -------------- | -------------- |
| int | **[pid](/documentation/code/modules/group__particleutils/#function-pid)**(const Particle & p)<br>Unbound function access to PID code.  |
| int | **[abspid](/documentation/code/modules/group__particleutils/#function-abspid)**(const Particle & p)<br>Unbound function access to abs PID code.  |


## Functions Documentation

### function pid

```
inline int pid(
    const Particle & p
)
```

Unbound function access to PID code. 

### function abspid

```
inline int abspid(
    const Particle & p
)
```

Unbound function access to abs PID code. 





-------------------------------

Updated on 2022-08-07 at 20:17:17 +0100
