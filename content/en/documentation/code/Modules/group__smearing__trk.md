---

title: 'group Experiment-specific tracking efficiency and smearing functions'

description: "[Documentation update required.]"

---

# Experiment-specific tracking efficiency and smearing functions

**Module:** **[Detector smearing & efficiency functions](/documentation/code/modules/group__smearing/)**



## Functions

|                | Name           |
| -------------- | -------------- |
| double | **[TRK_EFF_ATLAS_RUN1](/documentation/code/modules/group__smearing__trk/#function-trk-eff-atlas-run1)**(const Particle & p)<br><a href="/documentation/code/namespaces/namespacerivet_1_1atlas/">ATLAS</a><a href="/documentation/code/classes/classrivet_1_1run/">Run</a> 1 tracking efficiency.  |
| double | **[TRK_EFF_ATLAS_RUN2](/documentation/code/modules/group__smearing__trk/#function-trk-eff-atlas-run2)**(const Particle & p) |
| double | **[TRK_EFF_CMS_RUN1](/documentation/code/modules/group__smearing__trk/#function-trk-eff-cms-run1)**(const Particle & p)<br>CMS <a href="/documentation/code/classes/classrivet_1_1run/">Run</a> 1 tracking efficiency.  |
| double | **[TRK_EFF_CMS_RUN2](/documentation/code/modules/group__smearing__trk/#function-trk-eff-cms-run2)**(const Particle & p) |


## Functions Documentation

### function TRK_EFF_ATLAS_RUN1

```
inline double TRK_EFF_ATLAS_RUN1(
    const Particle & p
)
```

<a href="/documentation/code/namespaces/namespacerivet_1_1atlas/">ATLAS</a><a href="/documentation/code/classes/classrivet_1_1run/">Run</a> 1 tracking efficiency. 

### function TRK_EFF_ATLAS_RUN2

```
inline double TRK_EFF_ATLAS_RUN2(
    const Particle & p
)
```


**Todo**: Currently just a copy of <a href="/documentation/code/classes/classrivet_1_1run/">Run</a> 1: fix! 

<a href="/documentation/code/namespaces/namespacerivet_1_1atlas/">ATLAS</a><a href="/documentation/code/classes/classrivet_1_1run/">Run</a> 2 tracking efficiency 


### function TRK_EFF_CMS_RUN1

```
inline double TRK_EFF_CMS_RUN1(
    const Particle & p
)
```

CMS <a href="/documentation/code/classes/classrivet_1_1run/">Run</a> 1 tracking efficiency. 

### function TRK_EFF_CMS_RUN2

```
inline double TRK_EFF_CMS_RUN2(
    const Particle & p
)
```


**Todo**: Currently just a copy of <a href="/documentation/code/classes/classrivet_1_1run/">Run</a> 1: fix! 

CMS <a href="/documentation/code/classes/classrivet_1_1run/">Run</a> 2 tracking efficiency 






-------------------------------

Updated on 2022-08-07 at 20:17:18 +0100
