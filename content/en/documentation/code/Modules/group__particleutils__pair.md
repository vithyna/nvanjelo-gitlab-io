---

title: 'group Particle pair functions'

description: "[Documentation update required.]"

---

# Particle pair functions



## Functions

|                | Name           |
| -------------- | -------------- |
| PdgIdPair | **[pids](/documentation/code/modules/group__particleutils__pair/#function-pids)**(const ParticlePair & pp) |


## Functions Documentation

### function pids

```
inline PdgIdPair pids(
    const ParticlePair & pp
)
```


**Todo**: Make ParticlePair a custom class instead? 

Get the PDG ID codes of a ParticlePair 






-------------------------------

Updated on 2022-08-07 at 20:17:17 +0100
