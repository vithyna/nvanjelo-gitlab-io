---

title: 'group Fundamental particles'

description: "[Documentation update required.]"

---

# Fundamental particles



## Functions

|                | Name           |
| -------------- | -------------- |
| bool | **[isQuark](/documentation/code/modules/group__mcutils__fundamental/#function-isquark)**(int pid)<br>Determine if the PID is that of a quark.  |
| bool | **[isGluon](/documentation/code/modules/group__mcutils__fundamental/#function-isgluon)**(int pid)<br>Determine if the PID is that of a gluon.  |
| bool | **[isParton](/documentation/code/modules/group__mcutils__fundamental/#function-isparton)**(int pid)<br>Determine if the PID is that of a parton (quark or gluon)  |
| bool | **[isPhoton](/documentation/code/modules/group__mcutils__fundamental/#function-isphoton)**(int pid)<br>Determine if the PID is that of a photon.  |
| bool | **[isElectron](/documentation/code/modules/group__mcutils__fundamental/#function-iselectron)**(int pid)<br>Determine if the PID is that of an electron or positron.  |
| bool | **[isMuon](/documentation/code/modules/group__mcutils__fundamental/#function-ismuon)**(int pid)<br>Determine if the PID is that of an muon or antimuon.  |
| bool | **[isTau](/documentation/code/modules/group__mcutils__fundamental/#function-istau)**(int pid)<br>Determine if the PID is that of an tau or antitau.  |
| bool | **[isChargedLepton](/documentation/code/modules/group__mcutils__fundamental/#function-ischargedlepton)**(int pid)<br>Determine if the PID is that of a charged lepton.  |
| bool | **[isChLepton](/documentation/code/modules/group__mcutils__fundamental/#function-ischlepton)**(int pid) |
| bool | **[isNeutrino](/documentation/code/modules/group__mcutils__fundamental/#function-isneutrino)**(int pid)<br>Determine if the PID is that of a neutrino.  |
| bool | **[isWplus](/documentation/code/modules/group__mcutils__fundamental/#function-iswplus)**(int pid)<br>Determine if the PID is that of a W+.  |
| bool | **[isWminus](/documentation/code/modules/group__mcutils__fundamental/#function-iswminus)**(int pid)<br>Determine if the PID is that of a W-.  |
| bool | **[isW](/documentation/code/modules/group__mcutils__fundamental/#function-isw)**(int pid)<br>Determine if the PID is that of a W+-.  |
| bool | **[isZ](/documentation/code/modules/group__mcutils__fundamental/#function-isz)**(int pid)<br>Determine if the PID is that of a Z0.  |
| bool | **[isHiggs](/documentation/code/modules/group__mcutils__fundamental/#function-ishiggs)**(int pid)<br>Determine if the PID is that of an SM/lightest SUSY Higgs.  |
| bool | **[isGraviton](/documentation/code/modules/group__mcutils__fundamental/#function-isgraviton)**(int pid)<br>Is this a graviton?  |
| bool | **[isStrange](/documentation/code/modules/group__mcutils__fundamental/#function-isstrange)**(int pid)<br>Determine if the PID is that of an s/sbar.  |
| bool | **[isCharm](/documentation/code/modules/group__mcutils__fundamental/#function-ischarm)**(int pid)<br>Determine if the PID is that of a c/cbar.  |
| bool | **[isBottom](/documentation/code/modules/group__mcutils__fundamental/#function-isbottom)**(int pid)<br>Determine if the PID is that of a b/bbar.  |
| bool | **[isTop](/documentation/code/modules/group__mcutils__fundamental/#function-istop)**(int pid)<br>Determine if the PID is that of a t/tbar.  |


## Functions Documentation

### function isQuark

```
inline bool isQuark(
    int pid
)
```

Determine if the PID is that of a quark. 

### function isGluon

```
inline bool isGluon(
    int pid
)
```

Determine if the PID is that of a gluon. 

### function isParton

```
inline bool isParton(
    int pid
)
```

Determine if the PID is that of a parton (quark or gluon) 

### function isPhoton

```
inline bool isPhoton(
    int pid
)
```

Determine if the PID is that of a photon. 

### function isElectron

```
inline bool isElectron(
    int pid
)
```

Determine if the PID is that of an electron or positron. 

### function isMuon

```
inline bool isMuon(
    int pid
)
```

Determine if the PID is that of an muon or antimuon. 

### function isTau

```
inline bool isTau(
    int pid
)
```

Determine if the PID is that of an tau or antitau. 

### function isChargedLepton

```
inline bool isChargedLepton(
    int pid
)
```

Determine if the PID is that of a charged lepton. 

### function isChLepton

```
inline bool isChLepton(
    int pid
)
```


**Deprecated**: 

Prefer isChargedLepton 

Alias for isChargedLepton 


### function isNeutrino

```
inline bool isNeutrino(
    int pid
)
```

Determine if the PID is that of a neutrino. 

### function isWplus

```
inline bool isWplus(
    int pid
)
```

Determine if the PID is that of a W+. 

### function isWminus

```
inline bool isWminus(
    int pid
)
```

Determine if the PID is that of a W-. 

### function isW

```
inline bool isW(
    int pid
)
```

Determine if the PID is that of a W+-. 

### function isZ

```
inline bool isZ(
    int pid
)
```

Determine if the PID is that of a Z0. 

### function isHiggs

```
inline bool isHiggs(
    int pid
)
```

Determine if the PID is that of an SM/lightest SUSY Higgs. 

### function isGraviton

```
inline bool isGraviton(
    int pid
)
```

Is this a graviton? 

**Todo**: isSUSYHiggs? 

### function isStrange

```
inline bool isStrange(
    int pid
)
```

Determine if the PID is that of an s/sbar. 

### function isCharm

```
inline bool isCharm(
    int pid
)
```

Determine if the PID is that of a c/cbar. 

### function isBottom

```
inline bool isBottom(
    int pid
)
```

Determine if the PID is that of a b/bbar. 

### function isTop

```
inline bool isTop(
    int pid
)
```

Determine if the PID is that of a t/tbar. 





-------------------------------

Updated on 2022-08-07 at 20:17:17 +0100
