---

title: 'group Unbound functions for kinematic properties'

description: "[Documentation update required.]"

---

# Unbound functions for kinematic properties

**Module:** **[Functions for Particles and Jets](/documentation/code/modules/group__particlebaseutils/)** **/** **[Next-level filtering](/documentation/code/modules/group__particlebaseutils__uberfilt/)**

 [More...](#detailed-description)

## Namespaces

| Name           |
| -------------- |
| **[Rivet::Kin](/documentation/code/namespaces/namespacerivet_1_1kin/)**  |

## Detailed Description


**Note**: In a sub-namespace (imported by default) for protection 

**Todo**: 

  * Add 'all' variants 
  * Mostly move to functions on <a href="/documentation/code/classes/classrivet_1_1fourmomentum/">FourMomentum</a>






-------------------------------

Updated on 2022-08-07 at 20:17:18 +0100
