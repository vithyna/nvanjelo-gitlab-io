---

title: 'group Operations on collections of Jet'

description: "[Documentation update required.]"

---

# Operations on collections of Jet

**Module:** **[Functions for Jets](/documentation/code/modules/group__jetutils/)**

 [More...](#detailed-description)

## Namespaces

| Name           |
| -------------- |
| **[Rivet::Kin](/documentation/code/namespaces/namespacerivet_1_1kin/)**  |

## Detailed Description


**Note**: This can't be done on generic collections of <a href="/documentation/code/classes/classrivet_1_1particlebase/">ParticleBase</a>&ndash; thanks, C++ :-/ 





-------------------------------

Updated on 2022-08-07 at 20:17:18 +0100
