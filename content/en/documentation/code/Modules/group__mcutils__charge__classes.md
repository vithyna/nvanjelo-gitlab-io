---

title: 'group General PID-based classifier functions'

description: "[Documentation update required.]"

---

# General PID-based classifier functions



## Functions

|                | Name           |
| -------------- | -------------- |
| bool | **[isCharged](/documentation/code/modules/group__mcutils__charge__classes/#function-ischarged)**(int pid)<br>Determine if the particle is electrically charged.  |
| bool | **[isNeutral](/documentation/code/modules/group__mcutils__charge__classes/#function-isneutral)**(int pid)<br>Determine if the particle is electrically neutral.  |


## Functions Documentation

### function isCharged

```
inline bool isCharged(
    int pid
)
```

Determine if the particle is electrically charged. 

### function isNeutral

```
inline bool isNeutral(
    int pid
)
```

Determine if the particle is electrically neutral. 





-------------------------------

Updated on 2022-08-07 at 20:17:17 +0100
