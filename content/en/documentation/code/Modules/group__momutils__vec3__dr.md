---

title: 'group avoidf$avoidDelta Ravoidf$ calculations from 3-vectors'

description: "[Documentation update required.]"

---

# \f$\Delta R\f$ calculations from 3-vectors



## Functions

|                | Name           |
| -------------- | -------------- |
| double | **[deltaR2](/documentation/code/modules/group__momutils__vec3__dr/#function-deltar2)**(const Vector3 & a, const Vector3 & b)<br>Calculate the 2D rapidity-azimuthal ("eta-phi") distance between two spatial vectors.  |
| double | **[deltaR](/documentation/code/modules/group__momutils__vec3__dr/#function-deltar)**(const Vector3 & a, const Vector3 & b)<br>Calculate the 2D rapidity-azimuthal ("eta-phi") distance between two spatial vectors.  |
| double | **[deltaR2](/documentation/code/modules/group__momutils__vec3__dr/#function-deltar2)**(const Vector3 & v, double eta2, double phi2)<br>Calculate the 2D rapidity-azimuthal ("eta-phi") distance between two spatial vectors.  |
| double | **[deltaR](/documentation/code/modules/group__momutils__vec3__dr/#function-deltar)**(const Vector3 & v, double eta2, double phi2)<br>Calculate the 2D rapidity-azimuthal ("eta-phi") distance between two spatial vectors.  |
| double | **[deltaR2](/documentation/code/modules/group__momutils__vec3__dr/#function-deltar2)**(double eta1, double phi1, const Vector3 & v)<br>Calculate the 2D rapidity-azimuthal ("eta-phi") distance between two spatial vectors.  |
| double | **[deltaR](/documentation/code/modules/group__momutils__vec3__dr/#function-deltar)**(double eta1, double phi1, const Vector3 & v)<br>Calculate the 2D rapidity-azimuthal ("eta-phi") distance between two spatial vectors.  |


## Functions Documentation

### function deltaR2

```
inline double deltaR2(
    const Vector3 & a,
    const Vector3 & b
)
```

Calculate the 2D rapidity-azimuthal ("eta-phi") distance between two spatial vectors. 

### function deltaR

```
inline double deltaR(
    const Vector3 & a,
    const Vector3 & b
)
```

Calculate the 2D rapidity-azimuthal ("eta-phi") distance between two spatial vectors. 

### function deltaR2

```
inline double deltaR2(
    const Vector3 & v,
    double eta2,
    double phi2
)
```

Calculate the 2D rapidity-azimuthal ("eta-phi") distance between two spatial vectors. 

### function deltaR

```
inline double deltaR(
    const Vector3 & v,
    double eta2,
    double phi2
)
```

Calculate the 2D rapidity-azimuthal ("eta-phi") distance between two spatial vectors. 

### function deltaR2

```
inline double deltaR2(
    double eta1,
    double phi1,
    const Vector3 & v
)
```

Calculate the 2D rapidity-azimuthal ("eta-phi") distance between two spatial vectors. 

### function deltaR

```
inline double deltaR(
    double eta1,
    double phi1,
    const Vector3 & v
)
```

Calculate the 2D rapidity-azimuthal ("eta-phi") distance between two spatial vectors. 





-------------------------------

Updated on 2022-08-07 at 20:17:18 +0100
