---

title: 'group Angular momentum functions'

description: "[Documentation update required.]"

---

# Angular momentum functions



## Functions

|                | Name           |
| -------------- | -------------- |
| int | **[jSpin](/documentation/code/modules/group__mcutils__angmom/#function-jspin)**(int pid)<br>jSpin returns 2J+1, where J is the total spin  |
| int | **[sSpin](/documentation/code/modules/group__mcutils__angmom/#function-sspin)**(int pid)<br>sSpin returns 2S+1, where S is the spin  |
| int | **[lSpin](/documentation/code/modules/group__mcutils__angmom/#function-lspin)**(int pid)<br>lSpin returns 2L+1, where L is the orbital angular momentum  |


## Functions Documentation

### function jSpin

```
inline int jSpin(
    int pid
)
```

jSpin returns 2J+1, where J is the total spin 

### function sSpin

```
inline int sSpin(
    int pid
)
```

sSpin returns 2S+1, where S is the spin 

### function lSpin

```
inline int lSpin(
    int pid
)
```

lSpin returns 2L+1, where L is the orbital angular momentum 





-------------------------------

Updated on 2022-08-07 at 20:17:17 +0100
