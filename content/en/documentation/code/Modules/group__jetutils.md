---

title: 'group Functions for Jets'

description: "[Documentation update required.]"

---

# Functions for Jets



## Modules

| Name           |
| -------------- |
| **[Converting between Jets, Particles and PseudoJets](/documentation/code/modules/group__jetutils__conv/)**  |
| **[Jet classifier -> bool functors](/documentation/code/modules/group__jetutils__j2bool/)**  |
| **[Unbound functions for filtering jets](/documentation/code/modules/group__jetutils__filt/)**  |
| **[Operations on collections of Jet](/documentation/code/modules/group__jetutils__coll/)**  |






-------------------------------

Updated on 2022-08-07 at 20:17:18 +0100
