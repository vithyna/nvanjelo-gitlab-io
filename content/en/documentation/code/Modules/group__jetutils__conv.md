---

title: 'group Converting between Jets, Particles and PseudoJets'

description: "[Documentation update required.]"

---

# Converting between Jets, Particles and PseudoJets

**Module:** **[Functions for Jets](/documentation/code/modules/group__jetutils/)**



## Functions

|                | Name           |
| -------------- | -------------- |
| PseudoJets | **[mkPseudoJets](/documentation/code/modules/group__jetutils__conv/#function-mkpseudojets)**(const Particles & ps) |
| PseudoJets | **[mkPseudoJets](/documentation/code/modules/group__jetutils__conv/#function-mkpseudojets)**(const Jets & js) |
| Jets | **[mkJets](/documentation/code/modules/group__jetutils__conv/#function-mkjets)**(const PseudoJets & pjs) |


## Functions Documentation

### function mkPseudoJets

```
inline PseudoJets mkPseudoJets(
    const Particles & ps
)
```


### function mkPseudoJets

```
inline PseudoJets mkPseudoJets(
    const Jets & js
)
```


### function mkJets

```
inline Jets mkJets(
    const PseudoJets & pjs
)
```






-------------------------------

Updated on 2022-08-07 at 20:17:18 +0100
