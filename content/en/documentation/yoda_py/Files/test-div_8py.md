---

title: 'file tests/test-div.py'

description: "[Documentation update required.]"

---

# tests/test-div.py



## Namespaces

| Name           |
| -------------- |
| **[test-div](/documentation/yoda_py/namespaces/namespacetest-div/)**  |




## Source code

```python
#! /usr/bin/env python

import yoda, random

h1, h2 = [yoda.Histo1D(4, 0, 10) for _ in range(2)]
for i in range(1000):
    h1.fill(random.uniform(0,10))
    h2.fill(random.uniform(0,10))

s = h1 / h2
print(s)
for p in s.points():
    print(" ", p)

print()

s = h1.divideBy(h2)
print(s)
for p in s.points():
    print(" ", p)
```


-------------------------------

Updated on 2022-08-08 at 20:05:55 +0100
