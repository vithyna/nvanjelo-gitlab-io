---

title: 'file tests/test-counter.py'

description: "[Documentation update required.]"

---

# tests/test-counter.py



## Namespaces

| Name           |
| -------------- |
| **[test-counter](/documentation/yoda_py/namespaces/namespacetest-counter/)**  |




## Source code

```python
#! /usr/bin/env python

import yoda, random

c = yoda.Counter(path="/foo", title="MyTitle")

NUM_SAMPLES = 1000
for i in range(NUM_SAMPLES):
    c.fill(random.gauss(10,3))
print(c.val(), "+-", c.err())

yoda.write([c], "counter.yoda")
aos = yoda.read("counter.yoda")
for _, ao in aos.items():
    print(ao)
```


-------------------------------

Updated on 2022-08-08 at 20:05:55 +0100
