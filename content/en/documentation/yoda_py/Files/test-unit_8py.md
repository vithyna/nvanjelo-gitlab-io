---

title: 'file tests/test-unit.py'

description: "[Documentation update required.]"

---

# tests/test-unit.py



## Namespaces

| Name           |
| -------------- |
| **[test-unit](/documentation/yoda_py/namespaces/namespacetest-unit/)**  |




## Source code

```python
#! /usr/bin/env python

import yoda

print("WE NEED SOME REAL WORLD YODA PYTHON EXAMPLES AS TESTS!")

# import unittest

# class TestReader(unittest.TestCase):
#     def setUp(self):
#         print "Unit test setup"

#     def test_reader(self):
#         reader = yoda.ReaderAIDA()

# if __name__=='__main__':
#     unittest.main()
```


-------------------------------

Updated on 2022-08-08 at 20:05:55 +0100
