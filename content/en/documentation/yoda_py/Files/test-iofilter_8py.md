---

title: 'file tests/test-iofilter.py'

description: "[Documentation update required.]"

---

# tests/test-iofilter.py



## Namespaces

| Name           |
| -------------- |
| **[test-iofilter](/documentation/yoda_py/namespaces/namespacetest-iofilter/)**  |




## Source code

```python
#! /usr/bin/env python

import yoda
import os

srcpath = os.getenv('YODA_TESTS_SRC')

testfile = os.path.join(srcpath,"iofilter.yoda")

aos = yoda.read(testfile, False, patterns=r".*_(eta|mass)_(2|4)")
print(aos)
assert len(aos) == 4

index = yoda.mkIndexYODA(testfile)
print("Index of ref file:")
print(index)
import re
aos = yoda.read(testfile, False, patterns=[r".*_y_(1|3)", re.compile(r".*_dphi_(2|4)")])
print(aos)
assert len(aos) == 3

aos = yoda.read(testfile, False, patterns=[r".*_y_(1|3)", re.compile(r".*_dphi_(2|4)")], unpatterns=r".*_y_1")
print(aos)
assert len(aos) == 2
```


-------------------------------

Updated on 2022-08-08 at 20:05:55 +0100
