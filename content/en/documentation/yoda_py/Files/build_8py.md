---

title: 'file pyext/build.py'

description: "[Documentation update required.]"

---

# pyext/build.py



## Namespaces

| Name           |
| -------------- |
| **[build](/documentation/yoda_py/namespaces/namespacebuild/)**  |




## Source code

```python
#! /usr/bin/env python

from __future__ import print_function
import platform, sysconfig, os, sys
from glob import glob



extsrcdir = "/home/anarendran/Documents/temp/yoda/pyext/yoda"
extbuilddir = "/home/anarendran/Documents/temp/yoda/pyext/yoda"
srcdir = os.path.abspath("/home/anarendran/Documents/temp/yoda/src")
libdir = os.path.abspath("/home/anarendran/Documents/temp/yoda/src/.libs")
incdirs = [os.path.abspath("/home/anarendran/Documents/temp/yoda/include"),
           os.path.abspath("/home/anarendran/Documents/temp/yoda/include"),
           os.path.abspath(extsrcdir),
           os.path.abspath(extbuilddir)]
lookupdirs = []


incargs = " ".join("-I{}".format(d) for d in incdirs)
incargs += " -I/home/anarendran/Documents/temp/local/include"
incargs += "  -DNDEBUG -I/usr/include -I/usr/include"
incargs += " -I" + sysconfig.get_config_var("INCLUDEPY")


cmpargs = "-O3 -Wno-unused-but-set-variable -Wno-sign-compare"


linkargs = " ".join("-L{}".format(d) for d in lookupdirs)
linkargs += " -L/home/anarendran/Documents/temp/yoda/src/.libs" if "YODA_LOCAL" in os.environ else "-L/home/anarendran/Documents/temp/local/lib"


libraries = ["YODA"]
libargs = " ".join("-l{}".format(l) for l in libraries)


pyargs = "-I" + sysconfig.get_config_var("INCLUDEPY")
libpys = [os.path.join(sysconfig.get_config_var(ld), sysconfig.get_config_var("LDLIBRARY")) for ld in ["LIBPL", "LIBDIR"]]
libpy = None
for lp in libpys:
    if os.path.exists(lp):
        libpy = lp
        break
if libpy is None:
    print("No libpython found in expected location {}, exiting".format(libpy))
    sys.exit(1)
pyargs += " " + libpy
pyargs += " " + sysconfig.get_config_var("LIBS")
pyargs += " " + sysconfig.get_config_var("LIBM")
pyargs += " " + sysconfig.get_config_var("LINKFORSHARED")



import shutil
builddir = "/home/anarendran/Documents/temp/yoda/pyext/build/yoda"
try:
    shutil.rmtree(builddir)
except:
    pass
try:
    os.makedirs(builddir)
except FileExistsError:
    pass
for pyfile in glob(os.path.join(extsrcdir, "*.py")):
    shutil.copy(pyfile, builddir)



srcnames = ["core", "util"]
if "BUILD_ROOTCOMPAT" in os.environ:
    srcnames += "rootcompat"



for srcname in srcnames:
    
    srcpath = os.path.join(extbuilddir, srcname+".cpp")
    if not os.path.isfile(srcpath): # distcheck has it in srcdir
        srcpath = os.path.relpath(os.path.join(extsrcdir, srcname+".cpp"))

    
    if srcname == "core":
        srcpath += " " + os.path.join(extsrcdir, "errors.cpp")

    
    xcmpargs, xlinkargs = cmpargs, linkargs
    if srcname == "rootcompat":
        xcmpargs += " " + ""
        xlinkargs += " " + " "

    
    compile_cmd = "  ".join([os.environ.get("CXX", "g++"), "-shared -fPIC", "-o {}.so".format(srcname),
                             srcpath, incargs, xcmpargs, xlinkargs, libargs, pyargs])
    print("Build command =", compile_cmd)

    
    import subprocess
    subprocess.call(compile_cmd.split(), cwd=builddir)
```


-------------------------------

Updated on 2022-08-08 at 20:05:55 +0100
