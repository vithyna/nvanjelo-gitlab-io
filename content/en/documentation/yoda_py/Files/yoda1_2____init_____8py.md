---

title: 'file pyext/yoda1/__init__.py'

description: "[Documentation update required.]"

---

# pyext/yoda1/__init__.py



## Namespaces

| Name           |
| -------------- |
| **[yoda1](/documentation/yoda_py/namespaces/namespaceyoda1/)**  |




## Source code

```python
# -*- python -*-

from yoda import *
from yoda import __version__
```


-------------------------------

Updated on 2022-08-08 at 20:05:55 +0100
