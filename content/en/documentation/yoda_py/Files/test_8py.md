---

title: 'file sphinxdoc/test.py'

description: "[Documentation update required.]"

---

# sphinxdoc/test.py



## Namespaces

| Name           |
| -------------- |
| **[test](/documentation/yoda_py/namespaces/namespacetest/)**  |

## Classes

|                | Name           |
| -------------- | -------------- |
| class | **[test::Test](/documentation/yoda_py/classes/classtest_1_1test/)**  |




## Source code

```python
"""Test module doc"""

class Test(object):
        """Testing test class"""
        pass
```


-------------------------------

Updated on 2022-08-08 at 20:05:55 +0100
