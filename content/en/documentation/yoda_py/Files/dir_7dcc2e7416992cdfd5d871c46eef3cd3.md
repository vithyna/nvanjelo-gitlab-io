---

title: 'dir sphinxdoc'

description: "[Documentation update required.]"

---

# sphinxdoc



## Files

| Name           |
| -------------- |
| **[sphinxdoc/conf.py](/documentation/yoda_py/files/sphinxdoc_2conf_8py/#file-conf.py)**  |
| **[sphinxdoc/test.py](/documentation/yoda_py/files/test_8py/#file-test.py)**  |






-------------------------------

Updated on 2022-08-08 at 20:05:55 +0100
