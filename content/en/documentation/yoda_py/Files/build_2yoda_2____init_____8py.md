---

title: 'file pyext/build/yoda/__init__.py'

description: "[Documentation update required.]"

---

# pyext/build/yoda/__init__.py



## Namespaces

| Name           |
| -------------- |
| **[yoda](/documentation/yoda_py/namespaces/namespaceyoda/)**  |
| **[yoda::core](/documentation/yoda_py/namespaces/namespaceyoda_1_1core/)** <br>Pull in core YODA C++/Python extension functionality.  |




## Source code

```python
from __future__ import print_function


from yoda.core import *

__version__ = core.version()



from yoda.search import match_aos



try:
    import yoda.plotting
    from yoda.plotting import mplinit, plot
    HAS_PLOTTING = True
    # def plot(*args, **kwargs):
    #     from yoda.plotting import plot as p
    #     return p(*args, **kwargs)
except:
    HAS_PLOTTING = False



try:
    import yoda.root
    HAS_ROOT_SUPPORT = True
    # TODO: remove in v2
    def to_root(ao):
        print("yoda.to_root() is deprecated: use yoda.root.to_root()")
        return yoda.root.to_root(ao)
except:
    HAS_ROOT_SUPPORT = False
```


-------------------------------

Updated on 2022-08-08 at 20:05:55 +0100
