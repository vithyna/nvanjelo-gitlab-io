---

title: 'class yoda::search::Point'

description: "[Documentation update required.]"

---

# yoda::search::Point





Inherits from object, object

## Public Attributes

|                | Name           |
| -------------- | -------------- |
| | **[path](/documentation/yoda_py/classes/classyoda_1_1search_1_1point/#variable-path)**  |
| | **[n](/documentation/yoda_py/classes/classyoda_1_1search_1_1point/#variable-n)**  |
| | **[xmin](/documentation/yoda_py/classes/classyoda_1_1search_1_1point/#variable-xmin)**  |
| | **[xmax](/documentation/yoda_py/classes/classyoda_1_1search_1_1point/#variable-xmax)**  |
| | **[value](/documentation/yoda_py/classes/classyoda_1_1search_1_1point/#variable-value)**  |

## Public Attributes Documentation

### variable path

```python
path;
```


### variable n

```python
n;
```


### variable xmin

```python
xmin;
```


### variable xmax

```python
xmax;
```


### variable value

```python
value;
```


-------------------------------

Updated on 2022-08-08 at 20:05:55 +0100
