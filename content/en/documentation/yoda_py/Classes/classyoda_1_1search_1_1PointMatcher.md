---

title: 'class yoda::search::PointMatcher'

description: "[Documentation update required.]"

---

# yoda::search::PointMatcher



 [More...](#detailed-description)

Inherits from object, object

## Public Functions

|                | Name           |
| -------------- | -------------- |
| def | **[set_patt](/documentation/yoda_py/classes/classyoda_1_1search_1_1pointmatcher/#function-set-patt)**(self self, <a href="/documentation/yoda_py/classes/classyoda_1_1search_1_1pointmatcher/#variable-patt">patt</a> patt) |
| def | **[match_path](/documentation/yoda_py/classes/classyoda_1_1search_1_1pointmatcher/#function-match-path)**(self self, <a href="/documentation/yoda_py/classes/classyoda_1_1search_1_1pointmatcher/#variable-path">path</a> path) |
| def | **[search_path](/documentation/yoda_py/classes/classyoda_1_1search_1_1pointmatcher/#function-search-path)**(self self, <a href="/documentation/yoda_py/classes/classyoda_1_1search_1_1pointmatcher/#variable-path">path</a> path) |
| def | **[match_pos](/documentation/yoda_py/classes/classyoda_1_1search_1_1pointmatcher/#function-match-pos)**(self self, p p) |
| def | **[set_patt](/documentation/yoda_py/classes/classyoda_1_1search_1_1pointmatcher/#function-set-patt)**(self self, <a href="/documentation/yoda_py/classes/classyoda_1_1search_1_1pointmatcher/#variable-patt">patt</a> patt) |
| def | **[match_path](/documentation/yoda_py/classes/classyoda_1_1search_1_1pointmatcher/#function-match-path)**(self self, <a href="/documentation/yoda_py/classes/classyoda_1_1search_1_1pointmatcher/#variable-path">path</a> path) |
| def | **[search_path](/documentation/yoda_py/classes/classyoda_1_1search_1_1pointmatcher/#function-search-path)**(self self, <a href="/documentation/yoda_py/classes/classyoda_1_1search_1_1pointmatcher/#variable-path">path</a> path) |
| def | **[match_pos](/documentation/yoda_py/classes/classyoda_1_1search_1_1pointmatcher/#function-match-pos)**(self self, p p) |

## Public Attributes

|                | Name           |
| -------------- | -------------- |
| | **[re_patt](/documentation/yoda_py/classes/classyoda_1_1search_1_1pointmatcher/#variable-re-patt)**  |
| | **[patt](/documentation/yoda_py/classes/classyoda_1_1search_1_1pointmatcher/#variable-patt)** <br>Strip separated comments.  |
| | **[path](/documentation/yoda_py/classes/classyoda_1_1search_1_1pointmatcher/#variable-path)**  |
| | **[indextype](/documentation/yoda_py/classes/classyoda_1_1search_1_1pointmatcher/#variable-indextype)**  |
| | **[index](/documentation/yoda_py/classes/classyoda_1_1search_1_1pointmatcher/#variable-index)**  |

## Detailed Description

```python
class yoda::search::PointMatcher;
```




```
\
System for selecting subsets of bins based on a search range
syntax extended from Professor weight files:

Path structure: /path/parts/to/histo[syst_variation]@xmin:xmax
            or: /path/parts/to/histo[syst_variation]#nmin:nmax

TODO: Extend to multi-dimensional ranges i.e. @xmin:xmax,#nymin:nymax,...
```

## Public Functions Documentation

### function set_patt

```python
def set_patt(
    self self,
    patt patt
)
```


### function match_path

```python
def match_path(
    self self,
    path path
)
```


### function search_path

```python
def search_path(
    self self,
    path path
)
```


### function match_pos

```python
def match_pos(
    self self,
    p p
)
```




```
Decide if a given point p is in the match range.

p must be an object with attrs xmin, xmax, n

TODO: Use open ranges to include underflow and overflow

TODO: Allow negative indices in Python style, and use index=-1
to mean the N+1 index needed to include the last bin without
picking up the overflow, too.

TODO: Extension to multiple dimensions
```


### function set_patt

```python
def set_patt(
    self self,
    patt patt
)
```


### function match_path

```python
def match_path(
    self self,
    path path
)
```


### function search_path

```python
def search_path(
    self self,
    path path
)
```


### function match_pos

```python
def match_pos(
    self self,
    p p
)
```




```
Decide if a given point p is in the match range.

p must be an object with attrs xmin, xmax, n

TODO: Use open ranges to include underflow and overflow

TODO: Allow negative indices in Python style, and use index=-1
to mean the N+1 index needed to include the last bin without
picking up the overflow, too.

TODO: Extension to multiple dimensions
```


## Public Attributes Documentation

### variable re_patt

```python
re_patt;
```


### variable patt

```python
patt;
```

Strip separated comments. 

### variable path

```python
path;
```


### variable indextype

```python
indextype;
```


### variable index

```python
index;
```


-------------------------------

Updated on 2022-08-08 at 20:05:55 +0100
