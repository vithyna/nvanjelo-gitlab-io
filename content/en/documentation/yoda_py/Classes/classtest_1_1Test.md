---

title: 'class test::Test'

description: "[Documentation update required.]"

---

# test::Test



 [More...](#detailed-description)

Inherits from object

## Detailed Description

```python
class test::Test;
```




```
Testing test class```

-------------------------------

Updated on 2022-08-08 at 20:05:55 +0100
