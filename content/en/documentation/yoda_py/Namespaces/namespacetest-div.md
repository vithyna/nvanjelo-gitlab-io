---

title: 'namespace test-div'

description: "[Documentation update required.]"

---

# test-div



## Attributes

|                | Name           |
| -------------- | -------------- |
| | **[h1](/documentation/yoda_py/namespaces/namespacetest-div/#variable-h1)**  |
| | **[h2](/documentation/yoda_py/namespaces/namespacetest-div/#variable-h2)**  |
| | **[s](/documentation/yoda_py/namespaces/namespacetest-div/#variable-s)**  |



## Attributes Documentation

### variable h1

```python
h1;
```


### variable h2

```python
h2;
```


### variable s

```python
s =  h1 / h2;
```





-------------------------------

Updated on 2022-08-08 at 20:05:55 +0100
