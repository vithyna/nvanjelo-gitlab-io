---

title: 'namespace test-h1d'

description: "[Documentation update required.]"

---

# test-h1d



## Attributes

|                | Name           |
| -------------- | -------------- |
| | **[h1](/documentation/yoda_py/namespaces/namespacetest-h1d/#variable-h1)**  |
| | **[linspace](/documentation/yoda_py/namespaces/namespacetest-h1d/#variable-linspace)**  |
| | **[h2](/documentation/yoda_py/namespaces/namespacetest-h1d/#variable-h2)**  |
| | **[logspace](/documentation/yoda_py/namespaces/namespacetest-h1d/#variable-logspace)**  |
| | **[h3](/documentation/yoda_py/namespaces/namespacetest-h1d/#variable-h3)**  |
| int | **[NUM_SAMPLES](/documentation/yoda_py/namespaces/namespacetest-h1d/#variable-num-samples)**  |
| int | **[exp](/documentation/yoda_py/namespaces/namespacetest-h1d/#variable-exp)**  |
| float | **[val](/documentation/yoda_py/namespaces/namespacetest-h1d/#variable-val)**  |
| | **[aos](/documentation/yoda_py/namespaces/namespacetest-h1d/#variable-aos)**  |
| | **[s](/documentation/yoda_py/namespaces/namespacetest-h1d/#variable-s)**  |
| | **[s2](/documentation/yoda_py/namespaces/namespacetest-h1d/#variable-s2)**  |
| | **[s1](/documentation/yoda_py/namespaces/namespacetest-h1d/#variable-s1)**  |
| | **[area](/documentation/yoda_py/namespaces/namespacetest-h1d/#variable-area)**  |
| | **[su](/documentation/yoda_py/namespaces/namespacetest-h1d/#variable-su)** <br>Check the inclusion of underflow and overflow bins.  |
| | **[so](/documentation/yoda_py/namespaces/namespacetest-h1d/#variable-so)**  |
| | **[suo](/documentation/yoda_py/namespaces/namespacetest-h1d/#variable-suo)**  |



## Attributes Documentation

### variable h1

```python
h1 =  yoda.Histo1D(20, 0.0, 100.0, path="/foo", title="MyTitle");
```


### variable linspace

```python
linspace =  yoda.linspace(20, 0.0, 100.0);
```


### variable h2

```python
h2 =  yoda.Histo1D(linspace, path="/bar", title="Linearly spaced histo");
```


### variable logspace

```python
logspace =  yoda.logspace(20, 1.0, 64);
```


### variable h3

```python
h3 =  yoda.Histo1D(logspace, path="/baz", title="Log-spaced histo");
```


### variable NUM_SAMPLES

```python
int NUM_SAMPLES =  1000;
```


### variable exp

```python
int exp =  - (i-NUM_SAMPLES/2)**2 / float(NUM_SAMPLES/4);
```


### variable val

```python
float val =  2.718 ** exp;
```


### variable aos

```python
aos =  yoda.read("h1d.yoda");
```


### variable s

```python
s =  yoda.mkScatter(h1);
```


### variable s2

```python
s2 =  s.mkScatter();
```


### variable s1

```python
s1 =  yoda.mkScatter(h3);
```


### variable area

```python
area;
```


### variable su

```python
su =  yoda.mkScatter(h3, uflow_binwidth=1.0);
```

Check the inclusion of underflow and overflow bins. 

### variable so

```python
so =  yoda.mkScatter(h3, oflow_binwidth=1.0);
```


### variable suo

```python
suo =  yoda.mkScatter(h3, uflow_binwidth=1.0, oflow_binwidth=1.0);
```





-------------------------------

Updated on 2022-08-08 at 20:05:55 +0100
