---

title: 'namespace test-io'

description: "[Documentation update required.]"

---

# test-io



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[testsrcpath](/documentation/yoda_py/namespaces/namespacetest-io/#function-testsrcpath)**(fname fname) |

## Attributes

|                | Name           |
| -------------- | -------------- |
| | **[TESTSRCDIR](/documentation/yoda_py/namespaces/namespacetest-io/#variable-testsrcdir)**  |
| | **[aos_ref](/documentation/yoda_py/namespaces/namespacetest-io/#variable-aos-ref)**  |
| def | **[ypath](/documentation/yoda_py/namespaces/namespacetest-io/#variable-ypath)**  |
| def | **[yzpath](/documentation/yoda_py/namespaces/namespacetest-io/#variable-yzpath)**  |


## Functions Documentation

### function testsrcpath

```python
def testsrcpath(
    fname fname
)
```



## Attributes Documentation

### variable TESTSRCDIR

```python
TESTSRCDIR =  os.environ.get("YODA_TESTS_SRC", ".");
```


### variable aos_ref

```python
aos_ref =  yoda.read(testsrcpath("test.yoda"));
```


### variable ypath

```python
def ypath =  testsrcpath("test.yoda");
```


### variable yzpath

```python
def yzpath =  ypath + ".gz";
```





-------------------------------

Updated on 2022-08-08 at 20:05:55 +0100
