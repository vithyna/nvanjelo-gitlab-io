---

title: 'namespace test-rebin'

description: "[Documentation update required.]"

---

# test-rebin



## Attributes

|                | Name           |
| -------------- | -------------- |
| | **[h](/documentation/yoda_py/namespaces/namespacetest-rebin/#variable-h)**  |
| | **[ha1](/documentation/yoda_py/namespaces/namespacetest-rebin/#variable-ha1)**  |
| | **[ha2](/documentation/yoda_py/namespaces/namespacetest-rebin/#variable-ha2)**  |
| | **[hb1](/documentation/yoda_py/namespaces/namespacetest-rebin/#variable-hb1)**  |
| | **[hb2](/documentation/yoda_py/namespaces/namespacetest-rebin/#variable-hb2)**  |



## Attributes Documentation

### variable h

```python
h =  yoda.Histo1D(10, 0, 5);
```


### variable ha1

```python
ha1 =  h.clone();
```


### variable ha2

```python
ha2 =  h.clone();
```


### variable hb1

```python
hb1 =  h.clone();
```


### variable hb2

```python
hb2 =  h.clone();
```





-------------------------------

Updated on 2022-08-08 at 20:05:55 +0100
