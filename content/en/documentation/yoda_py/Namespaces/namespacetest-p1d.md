---

title: 'namespace test-p1d'

description: "[Documentation update required.]"

---

# test-p1d



## Attributes

|                | Name           |
| -------------- | -------------- |
| | **[p1](/documentation/yoda_py/namespaces/namespacetest-p1d/#variable-p1)**  |
| | **[linspace](/documentation/yoda_py/namespaces/namespacetest-p1d/#variable-linspace)**  |
| | **[p2](/documentation/yoda_py/namespaces/namespacetest-p1d/#variable-p2)**  |
| | **[logspace](/documentation/yoda_py/namespaces/namespacetest-p1d/#variable-logspace)**  |
| | **[p3](/documentation/yoda_py/namespaces/namespacetest-p1d/#variable-p3)**  |
| int | **[NUM_SAMPLES](/documentation/yoda_py/namespaces/namespacetest-p1d/#variable-num-samples)**  |
| | **[val](/documentation/yoda_py/namespaces/namespacetest-p1d/#variable-val)**  |
| | **[aos](/documentation/yoda_py/namespaces/namespacetest-p1d/#variable-aos)**  |
| | **[s](/documentation/yoda_py/namespaces/namespacetest-p1d/#variable-s)**  |
| | **[s2](/documentation/yoda_py/namespaces/namespacetest-p1d/#variable-s2)**  |
| | **[s1](/documentation/yoda_py/namespaces/namespacetest-p1d/#variable-s1)**  |
| | **[sd](/documentation/yoda_py/namespaces/namespacetest-p1d/#variable-sd)**  |
| | **[su](/documentation/yoda_py/namespaces/namespacetest-p1d/#variable-su)** <br>Check the inclusion of underflow and overflow bins.  |
| | **[so](/documentation/yoda_py/namespaces/namespacetest-p1d/#variable-so)**  |
| | **[suo](/documentation/yoda_py/namespaces/namespacetest-p1d/#variable-suo)**  |



## Attributes Documentation

### variable p1

```python
p1 =  yoda.Profile1D(20, 0.0, 100.0, path="/foo", title="MyTitle");
```


### variable linspace

```python
linspace =  yoda.linspace(20, 0.0, 100.0);
```


### variable p2

```python
p2 =  yoda.Profile1D(linspace, path="/bar", title="Linearly spaced histo");
```


### variable logspace

```python
logspace =  yoda.logspace(20, 1.0, 64);
```


### variable p3

```python
p3 =  yoda.Profile1D(logspace, path="/baz", title="Log-spaced histo");
```


### variable NUM_SAMPLES

```python
int NUM_SAMPLES =  1000;
```


### variable val

```python
val =  random.uniform(0,100);
```


### variable aos

```python
aos =  yoda.read("p1d.yoda");
```


### variable s

```python
s =  yoda.mkScatter(p1);
```


### variable s2

```python
s2 =  s.mkScatter();
```


### variable s1

```python
s1 =  yoda.mkScatter(p1);
```


### variable sd

```python
sd;
```


### variable su

```python
su =  yoda.mkScatter(p1, uflow_binwidth=1.0);
```

Check the inclusion of underflow and overflow bins. 

### variable so

```python
so =  yoda.mkScatter(p1, oflow_binwidth=1.0);
```


### variable suo

```python
suo =  yoda.mkScatter(p1, uflow_binwidth=1.0, oflow_binwidth=1.0);
```





-------------------------------

Updated on 2022-08-08 at 20:05:55 +0100
