---

title: 'namespace test-iofilter'

description: "[Documentation update required.]"

---

# test-iofilter



## Attributes

|                | Name           |
| -------------- | -------------- |
| | **[srcpath](/documentation/yoda_py/namespaces/namespacetest-iofilter/#variable-srcpath)**  |
| | **[testfile](/documentation/yoda_py/namespaces/namespacetest-iofilter/#variable-testfile)**  |
| | **[aos](/documentation/yoda_py/namespaces/namespacetest-iofilter/#variable-aos)**  |
| | **[index](/documentation/yoda_py/namespaces/namespacetest-iofilter/#variable-index)**  |



## Attributes Documentation

### variable srcpath

```python
srcpath =  os.getenv('YODA_TESTS_SRC');
```


### variable testfile

```python
testfile =  os.path.join(srcpath,"iofilter.yoda");
```


### variable aos

```python
aos =  yoda.read(testfile, False, patterns=r".*_(eta|mass)_(2|4)");
```


### variable index

```python
index =  yoda.mkIndexYODA(testfile);
```





-------------------------------

Updated on 2022-08-08 at 20:05:55 +0100
