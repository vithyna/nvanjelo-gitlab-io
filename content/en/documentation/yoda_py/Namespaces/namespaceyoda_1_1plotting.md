---

title: 'namespace yoda::plotting'

description: "[Documentation update required.]"

---

# yoda::plotting



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[read_plot_keys](/documentation/yoda_py/namespaces/namespaceyoda_1_1plotting/#function-read-plot-keys)**(datfile datfile) |
| def | **[mplinit](/documentation/yoda_py/namespaces/namespaceyoda_1_1plotting/#function-mplinit)**(engine engine ="MPL", font font ="TeX Gyre Pagella", fontsize fontsize =17, mfont mfont =None, textfigs textfigs =True) |
| def | **[show](/documentation/yoda_py/namespaces/namespaceyoda_1_1plotting/#function-show)**() |
| def | **[mk_figaxes_1d](/documentation/yoda_py/namespaces/namespaceyoda_1_1plotting/#function-mk-figaxes-1d)**(ratio ratio =True, title title =None, figsize figsize =(8, 6)) |
| def | **[set_axis_labels_1d](/documentation/yoda_py/namespaces/namespaceyoda_1_1plotting/#function-set-axis-labels-1d)**(axmain axmain, axratio axratio, xlabel xlabel =None, ylabel ylabel =None, ratioylabel ratioylabel =None) |
| def | **[mk_lowcase_dict](/documentation/yoda_py/namespaces/namespaceyoda_1_1plotting/#function-mk-lowcase-dict)**(d d) |
| def | **[setup_axes_1d](/documentation/yoda_py/namespaces/namespaceyoda_1_1plotting/#function-setup-axes-1d)**(axmain axmain, axratio axratio, ** plotkeys) |
| def | **[plot_hist_on_axes_1d](/documentation/yoda_py/namespaces/namespaceyoda_1_1plotting/#function-plot-hist-on-axes-1d)**(axmain axmain, axratio axratio, h h, href href =None, default_color default_color ="black", default_linestyle default_linestyle ="-", ** plotkeys) |
| def | **[plot](/documentation/yoda_py/namespaces/namespaceyoda_1_1plotting/#function-plot)**(hs hs, outfile outfile =None, ratio ratio =True, <a href="/documentation/yoda_py/namespaces/namespaceyoda_1_1plotting/#function-show">show</a> show =False, axmain axmain =None, axratio axratio =None, ** plotkeys) |
| def | **[nplot](/documentation/yoda_py/namespaces/namespaceyoda_1_1plotting/#function-nplot)**(hs hs, outfiles outfiles =None, ratio ratio =True, <a href="/documentation/yoda_py/namespaces/namespaceyoda_1_1plotting/#function-show">show</a> show =False, nproc nproc =1, ** plotkeys) |

## Attributes

|                | Name           |
| -------------- | -------------- |
| def | **[initmpl](/documentation/yoda_py/namespaces/namespaceyoda_1_1plotting/#variable-initmpl)** <br>Alias.  |
| def | **[setup_mpl](/documentation/yoda_py/namespaces/namespaceyoda_1_1plotting/#variable-setup-mpl)**  |
| def | **[plot_hists_1d](/documentation/yoda_py/namespaces/namespaceyoda_1_1plotting/#variable-plot-hists-1d)** <br>Aliases.  |
| def | **[plot_hist_1d](/documentation/yoda_py/namespaces/namespaceyoda_1_1plotting/#variable-plot-hist-1d)**  |


## Functions Documentation

### function read_plot_keys

```python
def read_plot_keys(
    datfile datfile
)
```


### function mplinit

```python
def mplinit(
    engine engine ="MPL",
    font font ="TeX Gyre Pagella",
    fontsize fontsize =17,
    mfont mfont =None,
    textfigs textfigs =True
)
```




```
One-liner matplotlib (mpl) setup.

By default mpl will be configured with its native MathText rendering
backend, and a Palatino-like font for both text and math contexts, using
'lower-case numerals' if supported. Setting the engine to 'TEX' will use
standard mpl rendering, with calls to LaTeX for axis labels and other text;
setting it to 'PGF' will use the TeX PGF renderer: both these modes are much
slower than MPL mode, but the latter only supports a limited set of LaTeX
macros and does not render as nicely as the TeX backends.

The font and mfont optional arguments can be used to choose a different text
font and math font respectively; if mfont is None, it defaults to the same
as the text font. The textfigs boolean argument can be set false to disable
the lower-case/text/old-style numerals and use 'upper-case' numerals
everywhere. These options do not currently apply to the MPL rendering engine.
```


### function show

```python
def show()
```




```
Convenience call to matplotlib.pyplot.show()

NOTE: done this way to avoid import of pyplot before mplinit()
or mpl.use() has been (optionally) called.
```


### function mk_figaxes_1d

```python
def mk_figaxes_1d(
    ratio ratio =True,
    title title =None,
    figsize figsize =(8, 6)
)
```


### function set_axis_labels_1d

```python
def set_axis_labels_1d(
    axmain axmain,
    axratio axratio,
    xlabel xlabel =None,
    ylabel ylabel =None,
    ratioylabel ratioylabel =None
)
```


### function mk_lowcase_dict

```python
def mk_lowcase_dict(
    d d
)
```


### function setup_axes_1d

```python
def setup_axes_1d(
    axmain axmain,
    axratio axratio,
    ** plotkeys
)
```


### function plot_hist_on_axes_1d

```python
def plot_hist_on_axes_1d(
    axmain axmain,
    axratio axratio,
    h h,
    href href =None,
    default_color default_color ="black",
    default_linestyle default_linestyle ="-",
    ** plotkeys
)
```


### function plot

```python
def plot(
    hs hs,
    outfile outfile =None,
    ratio ratio =True,
    show show =False,
    axmain axmain =None,
    axratio axratio =None,
    ** plotkeys
)
```




```
Plot the given histograms on a single figure, returning (fig, (main_axis,
ratio_axis)). Show to screen if the second arg is True, and saving to outfile
if it is otherwise non-null.
```


### function nplot

```python
def nplot(
    hs hs,
    outfiles outfiles =None,
    ratio ratio =True,
    show show =False,
    nproc nproc =1,
    ** plotkeys
)
```




```
Plot the given list of histogram(s), cf. many calls to plot().

hs must be an iterable, each entry of which will be the content of a single
plot: the entries can either be single histograms or lists of histograms,
i.e. either kind of valid first argument to plot().

Outfiles must be an iterable corresponding to hs, and ratio may either be a
bool or such an iterable.

The return value is a list of the return tuples from each call to plot(), of
the same length as the hs arg.


MULTIPROCESSING -- *WARNING* CURRENTLY BROKEN

The main point of this function, other than convenience, is that the Python
multiprocessing module can be used to distribute the work on to multiple
parallel processes.

The nproc argument should be the integer number of parallel processes on
which to distribute the plotting. nproc = None (the default value) will use
Ncpu-1 or 1 process, whichever is larger. If nproc = 1, multiprocessing will
not be used -- this avoids overhead and eases debugging.
```



## Attributes Documentation

### variable initmpl

```python
def initmpl =  mplinit;
```

Alias. 

### variable setup_mpl

```python
def setup_mpl =  mplinit;
```


### variable plot_hists_1d

```python
def plot_hists_1d =  plot;
```

Aliases. 

### variable plot_hist_1d

```python
def plot_hist_1d =  plot;
```





-------------------------------

Updated on 2022-08-08 at 20:05:55 +0100
