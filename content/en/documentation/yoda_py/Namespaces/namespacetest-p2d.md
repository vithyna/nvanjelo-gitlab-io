---

title: 'namespace test-p2d'

description: "[Documentation update required.]"

---

# test-p2d



## Attributes

|                | Name           |
| -------------- | -------------- |
| | **[p](/documentation/yoda_py/namespaces/namespacetest-p2d/#variable-p)**  |
| | **[aos](/documentation/yoda_py/namespaces/namespacetest-p2d/#variable-aos)**  |
| | **[s](/documentation/yoda_py/namespaces/namespacetest-p2d/#variable-s)** <br>Check scatter conversion.  |
| | **[s2](/documentation/yoda_py/namespaces/namespacetest-p2d/#variable-s2)**  |



## Attributes Documentation

### variable p

```python
p =  yoda.Profile2D(5,0.,10., 5,0.,10., "/bar");
```


### variable aos

```python
aos =  yoda.read("p2d.yoda");
```


### variable s

```python
s =  yoda.mkScatter(p);
```

Check scatter conversion. 

### variable s2

```python
s2 =  s.mkScatter();
```





-------------------------------

Updated on 2022-08-08 at 20:05:55 +0100
