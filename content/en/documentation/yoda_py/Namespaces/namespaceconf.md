---

title: 'namespace conf'

description: "[Documentation update required.]"

---

# conf



## Attributes

|                | Name           |
| -------------- | -------------- |
| list | **[extensions](/documentation/yoda_py/namespaces/namespaceconf/#variable-extensions)**  |
| list | **[templates_path](/documentation/yoda_py/namespaces/namespaceconf/#variable-templates-path)**  |
| string | **[source_suffix](/documentation/yoda_py/namespaces/namespaceconf/#variable-source-suffix)**  |
| string | **[master_doc](/documentation/yoda_py/namespaces/namespaceconf/#variable-master-doc)**  |
| string | **[project](/documentation/yoda_py/namespaces/namespaceconf/#variable-project)**  |
| string | **[copyright](/documentation/yoda_py/namespaces/namespaceconf/#variable-copyright)**  |
| | **[version](/documentation/yoda_py/namespaces/namespaceconf/#variable-version)**  |
| | **[release](/documentation/yoda_py/namespaces/namespaceconf/#variable-release)**  |
| list | **[exclude_patterns](/documentation/yoda_py/namespaces/namespaceconf/#variable-exclude-patterns)**  |
| string | **[pygments_style](/documentation/yoda_py/namespaces/namespaceconf/#variable-pygments-style)**  |
| string | **[html_theme](/documentation/yoda_py/namespaces/namespaceconf/#variable-html-theme)**  |
| list | **[html_static_path](/documentation/yoda_py/namespaces/namespaceconf/#variable-html-static-path)**  |
| string | **[htmlhelp_basename](/documentation/yoda_py/namespaces/namespaceconf/#variable-htmlhelp-basename)**  |
| dictionary | **[latex_elements](/documentation/yoda_py/namespaces/namespaceconf/#variable-latex-elements)**  |
| list | **[latex_documents](/documentation/yoda_py/namespaces/namespaceconf/#variable-latex-documents)**  |
| list | **[man_pages](/documentation/yoda_py/namespaces/namespaceconf/#variable-man-pages)**  |
| list | **[texinfo_documents](/documentation/yoda_py/namespaces/namespaceconf/#variable-texinfo-documents)**  |



## Attributes Documentation

### variable extensions

```python
list extensions =  [
    'sphinx.ext.autodoc',
    'sphinx.ext.todo',
    'sphinx.ext.coverage',
    'sphinx.ext.mathjax',
    'sphinx.ext.viewcode',
];
```


### variable templates_path

```python
list templates_path =  ['_templates'];
```


### variable source_suffix

```python
string source_suffix =  '.rst';
```


### variable master_doc

```python
string master_doc =  'index';
```


### variable project

```python
string project =  u'YODA';
```


### variable copyright

```python
string copyright =  u'2010-2017, YODA Collaboration';
```


### variable version

```python
version =  yoda.__version__;
```


### variable release

```python
release =  yoda.__version__;
```


### variable exclude_patterns

```python
list exclude_patterns =  ['_build'];
```


### variable pygments_style

```python
string pygments_style =  'sphinx';
```


### variable html_theme

```python
string html_theme =  'default';
```


### variable html_static_path

```python
list html_static_path =  ['_static'];
```


### variable htmlhelp_basename

```python
string htmlhelp_basename =  'YODAdoc';
```


### variable latex_elements

```python
dictionary latex_elements =  {
# The paper size ('letterpaper' or 'a4paper').
#'papersize': 'letterpaper',

# The font size ('10pt', '11pt' or '12pt').
#'pointsize': '10pt',

# Additional stuff for the LaTeX preamble.
#'preamble': '',
};
```


### variable latex_documents

```python
list latex_documents =  [
  ('index', 'YODA.tex', u'YODA Documentation',
   u'YODA Collaboration', 'manual'),
];
```


### variable man_pages

```python
list man_pages =  [
    ('index', 'yoda', u'YODA Documentation',
     [u'YODA Collaboration'], 1)
];
```


### variable texinfo_documents

```python
list texinfo_documents =  [
  ('index', 'YODA', u'YODA Documentation',
   u'YODA Collaboration', 'YODA', 'One line description of project.',
   'Miscellaneous'),
];
```





-------------------------------

Updated on 2022-08-08 at 20:05:55 +0100
