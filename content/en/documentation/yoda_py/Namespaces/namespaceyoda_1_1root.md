---

title: 'namespace yoda::root'

description: "[Documentation update required.]"

---

# yoda::root



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[getall](/documentation/yoda_py/namespaces/namespaceyoda_1_1root/#function-getall)**(d d, basepath basepath ="/", verbose verbose =False) |


## Functions Documentation

### function getall

```python
def getall(
    d d,
    basepath basepath ="/",
    verbose verbose =False
)
```






-------------------------------

Updated on 2022-08-08 at 20:05:55 +0100
