---

title: 'namespace test-counter'

description: "[Documentation update required.]"

---

# test-counter



## Attributes

|                | Name           |
| -------------- | -------------- |
| | **[c](/documentation/yoda_py/namespaces/namespacetest-counter/#variable-c)**  |
| int | **[NUM_SAMPLES](/documentation/yoda_py/namespaces/namespacetest-counter/#variable-num-samples)**  |
| | **[aos](/documentation/yoda_py/namespaces/namespacetest-counter/#variable-aos)**  |



## Attributes Documentation

### variable c

```python
c =  yoda.Counter(path="/foo", title="MyTitle");
```


### variable NUM_SAMPLES

```python
int NUM_SAMPLES =  1000;
```


### variable aos

```python
aos =  yoda.read("counter.yoda");
```





-------------------------------

Updated on 2022-08-08 at 20:05:55 +0100
