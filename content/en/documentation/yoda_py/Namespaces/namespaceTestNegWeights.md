---

title: 'namespace TestNegWeights'

description: "[Documentation update required.]"

---

# TestNegWeights



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[testmatch](/documentation/yoda_py/namespaces/namespacetestnegweights/#function-testmatch)**(name name, v1 v1, v2 v2, tolerance tolerance =1e-3) |

## Attributes

|                | Name           |
| -------------- | -------------- |
| | **[h1](/documentation/yoda_py/namespaces/namespacetestnegweights/#variable-h1)**  |
| | **[h2](/documentation/yoda_py/namespaces/namespacetestnegweights/#variable-h2)**  |
| def | **[meanmatch](/documentation/yoda_py/namespaces/namespacetestnegweights/#variable-meanmatch)**  |
| def | **[stddevmatch](/documentation/yoda_py/namespaces/namespacetestnegweights/#variable-stddevmatch)**  |
| tuple | **[ok](/documentation/yoda_py/namespaces/namespacetestnegweights/#variable-ok)**  |
| | **[p1](/documentation/yoda_py/namespaces/namespacetestnegweights/#variable-p1)**  |
| | **[p2](/documentation/yoda_py/namespaces/namespacetestnegweights/#variable-p2)**  |


## Functions Documentation

### function testmatch

```python
def testmatch(
    name name,
    v1 v1,
    v2 v2,
    tolerance tolerance =1e-3
)
```



## Attributes Documentation

### variable h1

```python
h1 =  Histo1D(5, 0.0, 100.0);
```


### variable h2

```python
h2 =  Histo1D(5, 0.0, 100.0);
```


### variable meanmatch

```python
def meanmatch =  testmatch("means",    h1.mean(),   h2.mean());
```


### variable stddevmatch

```python
def stddevmatch =  testmatch("std devs", h1.stdDev(), h2.stdDev());
```


### variable ok

```python
tuple ok =  (stddevmatch and meanmatch);
```


### variable p1

```python
p1 =  Profile1D(5, 0.0, 100.0);
```


### variable p2

```python
p2 =  Profile1D(5, 0.0, 100.0);
```





-------------------------------

Updated on 2022-08-08 at 20:05:55 +0100
