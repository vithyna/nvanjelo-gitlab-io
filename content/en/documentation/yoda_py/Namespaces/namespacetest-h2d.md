---

title: 'namespace test-h2d'

description: "[Documentation update required.]"

---

# test-h2d



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[test_addBins](/documentation/yoda_py/namespaces/namespacetest-h2d/#function-test-addbins)**() |

## Attributes

|                | Name           |
| -------------- | -------------- |
| | **[h](/documentation/yoda_py/namespaces/namespacetest-h2d/#variable-h)**  |
| | **[aos](/documentation/yoda_py/namespaces/namespacetest-h2d/#variable-aos)**  |
| | **[s](/documentation/yoda_py/namespaces/namespacetest-h2d/#variable-s)**  |
| | **[s2](/documentation/yoda_py/namespaces/namespacetest-h2d/#variable-s2)**  |


## Functions Documentation

### function test_addBins

```python
def test_addBins()
```



## Attributes Documentation

### variable h

```python
h =  yoda.Histo2D(5,0.,10., 5,0.,10., "/foo");
```


### variable aos

```python
aos =  yoda.read("h2d.yoda");
```


### variable s

```python
s =  yoda.mkScatter(h);
```


### variable s2

```python
s2 =  s.mkScatter();
```





-------------------------------

Updated on 2022-08-08 at 20:05:55 +0100
