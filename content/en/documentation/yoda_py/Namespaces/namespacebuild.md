---

title: 'namespace build'

description: "[Documentation update required.]"

---

# build



## Attributes

|                | Name           |
| -------------- | -------------- |
| string | **[extsrcdir](/documentation/yoda_py/namespaces/namespacebuild/#variable-extsrcdir)** <br>Build dirs.  |
| string | **[extbuilddir](/documentation/yoda_py/namespaces/namespacebuild/#variable-extbuilddir)**  |
| | **[srcdir](/documentation/yoda_py/namespaces/namespacebuild/#variable-srcdir)**  |
| | **[libdir](/documentation/yoda_py/namespaces/namespacebuild/#variable-libdir)**  |
| list | **[incdirs](/documentation/yoda_py/namespaces/namespacebuild/#variable-incdirs)**  |
| list | **[lookupdirs](/documentation/yoda_py/namespaces/namespacebuild/#variable-lookupdirs)**  |
| string | **[incargs](/documentation/yoda_py/namespaces/namespacebuild/#variable-incargs)** <br>Include args.  |
| string | **[cmpargs](/documentation/yoda_py/namespaces/namespacebuild/#variable-cmpargs)** <br>Compile args.  |
| string | **[linkargs](/documentation/yoda_py/namespaces/namespacebuild/#variable-linkargs)** <br>Link args &ndash; base on install-prefix, or on local lib dirs for pre-install build.  |
| list | **[libraries](/documentation/yoda_py/namespaces/namespacebuild/#variable-libraries)** <br>Library args.  |
| string | **[libargs](/documentation/yoda_py/namespaces/namespacebuild/#variable-libargs)**  |
| string | **[pyargs](/documentation/yoda_py/namespaces/namespacebuild/#variable-pyargs)** <br>Python compile/link args.  |
| list | **[libpys](/documentation/yoda_py/namespaces/namespacebuild/#variable-libpys)**  |
| | **[libpy](/documentation/yoda_py/namespaces/namespacebuild/#variable-libpy)**  |
| string | **[builddir](/documentation/yoda_py/namespaces/namespacebuild/#variable-builddir)**  |
| list | **[srcnames](/documentation/yoda_py/namespaces/namespacebuild/#variable-srcnames)** <br>Modules to build, adding rootcompat if requested.  |
| | **[srcpath](/documentation/yoda_py/namespaces/namespacebuild/#variable-srcpath)** <br>Run the compilation in the build dir for each source file.  |
| | **[xcmpargs](/documentation/yoda_py/namespaces/namespacebuild/#variable-xcmpargs)** <br>Add errors source for core only.  |
| | **[xlinkargs](/documentation/yoda_py/namespaces/namespacebuild/#variable-xlinkargs)**  |
| string | **[compile_cmd](/documentation/yoda_py/namespaces/namespacebuild/#variable-compile-cmd)** <br>Assemble the compile & link command.  |
| | **[cwd](/documentation/yoda_py/namespaces/namespacebuild/#variable-cwd)**  |



## Attributes Documentation

### variable extsrcdir

```python
string extsrcdir =  "/home/anarendran/Documents/temp/yoda/pyext/yoda";
```

Build dirs. 

### variable extbuilddir

```python
string extbuilddir =  "/home/anarendran/Documents/temp/yoda/pyext/yoda";
```


### variable srcdir

```python
srcdir =  os.path.abspath("/home/anarendran/Documents/temp/yoda/src");
```


### variable libdir

```python
libdir =  os.path.abspath("/home/anarendran/Documents/temp/yoda/src/.libs");
```


### variable incdirs

```python
list incdirs =  [os.path.abspath("/home/anarendran/Documents/temp/yoda/include"),
           os.path.abspath("/home/anarendran/Documents/temp/yoda/include"),
           os.path.abspath(extsrcdir),
           os.path.abspath(extbuilddir)];
```


### variable lookupdirs

```python
list lookupdirs =  [];
```


### variable incargs

```python
string incargs =  " ".join("-I{}".format(d) for d in incdirs);
```

Include args. 

### variable cmpargs

```python
string cmpargs =  "-O3 -Wno-unused-but-set-variable -Wno-sign-compare";
```

Compile args. 

### variable linkargs

```python
string linkargs =  " ".join("-L{}".format(d) for d in lookupdirs);
```

Link args &ndash; base on install-prefix, or on local lib dirs for pre-install build. 

### variable libraries

```python
list libraries =  ["YODA"];
```

Library args. 

### variable libargs

```python
string libargs =  " ".join("-l{}".format(l) for l in libraries);
```


### variable pyargs

```python
string pyargs =  "-I" + sysconfig.get_config_var("INCLUDEPY");
```

Python compile/link args. 

### variable libpys

```python
list libpys =  [os.path.join(sysconfig.get_config_var(ld), sysconfig.get_config_var("LDLIBRARY")) for ld in ["LIBPL", "LIBDIR"]];
```


### variable libpy

```python
libpy =  None;
```


### variable builddir

```python
string builddir =  "/home/anarendran/Documents/temp/yoda/pyext/build/yoda";
```


### variable srcnames

```python
list srcnames =  ["core", "util"];
```

Modules to build, adding rootcompat if requested. 

### variable srcpath

```python
srcpath =  os.path.join(extbuilddir, srcname+".cpp");
```

Run the compilation in the build dir for each source file. 

Find the extension source file 


### variable xcmpargs

```python
xcmpargs;
```

Add errors source for core only. 

Add ROOT args for rootcompat only 


### variable xlinkargs

```python
xlinkargs;
```


### variable compile_cmd

```python
string compile_cmd =  "  ".join([os.environ.get("CXX", "g++"), "-shared -fPIC", "-o {}.so".format(srcname),
                             srcpath, incargs, xcmpargs, xlinkargs, libargs, pyargs]);
```

Assemble the compile & link command. 

### variable cwd

```python
cwd;
```





-------------------------------

Updated on 2022-08-08 at 20:05:55 +0100
