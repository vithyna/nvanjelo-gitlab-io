---

title: 'namespace yoda'

description: "[Documentation update required.]"

---

# yoda



## Namespaces

| Name           |
| -------------- |
| **[yoda::plotting](/documentation/yoda_py/namespaces/namespaceyoda_1_1plotting/)**  |
| **[yoda::root](/documentation/yoda_py/namespaces/namespaceyoda_1_1root/)**  |
| **[yoda::script_helpers](/documentation/yoda_py/namespaces/namespaceyoda_1_1script__helpers/)**  |
| **[yoda::search](/documentation/yoda_py/namespaces/namespaceyoda_1_1search/)**  |

## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[to_root](/documentation/yoda_py/namespaces/namespaceyoda/#function-to-root)**(ao ao) |

## Attributes

|                | Name           |
| -------------- | -------------- |
| bool | **[HAS_PLOTTING](/documentation/yoda_py/namespaces/namespaceyoda/#variable-has-plotting)**  |
| bool | **[HAS_ROOT_SUPPORT](/documentation/yoda_py/namespaces/namespaceyoda/#variable-has-root-support)**  |


## Functions Documentation

### function to_root

```python
def to_root(
    ao ao
)
```



## Attributes Documentation

### variable HAS_PLOTTING

```python
bool HAS_PLOTTING =  True;
```


### variable HAS_ROOT_SUPPORT

```python
bool HAS_ROOT_SUPPORT =  True;
```





-------------------------------

Updated on 2022-08-08 at 20:05:55 +0100
