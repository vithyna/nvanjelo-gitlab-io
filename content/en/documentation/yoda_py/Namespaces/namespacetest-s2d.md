---

title: 'namespace test-s2d'

description: "[Documentation update required.]"

---

# test-s2d



## Attributes

|                | Name           |
| -------------- | -------------- |
| | **[s](/documentation/yoda_py/namespaces/namespacetest-s2d/#variable-s)**  |
| | **[s2](/documentation/yoda_py/namespaces/namespacetest-s2d/#variable-s2)**  |
| | **[aos](/documentation/yoda_py/namespaces/namespacetest-s2d/#variable-aos)**  |



## Attributes Documentation

### variable s

```python
s =  yoda.Scatter2D("/foo");
```


### variable s2

```python
s2 =  s.clone();
```


### variable aos

```python
aos =  yoda.read("s2d.yoda");
```





-------------------------------

Updated on 2022-08-08 at 20:05:55 +0100
