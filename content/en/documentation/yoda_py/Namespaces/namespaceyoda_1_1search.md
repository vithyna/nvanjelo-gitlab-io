---

title: 'namespace yoda::search'

description: "[Documentation update required.]"

---

# yoda::search



## Classes

|                | Name           |
| -------------- | -------------- |
| class | **[yoda::search::Point](/documentation/yoda_py/classes/classyoda_1_1search_1_1point/)**  |
| class | **[yoda::search::PointMatcher](/documentation/yoda_py/classes/classyoda_1_1search_1_1pointmatcher/)**  |

## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[match_aos](/documentation/yoda_py/namespaces/namespaceyoda_1_1search/#function-match-aos)**(aos aos, patts patts, unpatts unpatts =None, search search =False)<br>A tool for filtering AO collections by path patterns.  |

## Attributes

|                | Name           |
| -------------- | -------------- |
| | **[POINTS](/documentation/yoda_py/namespaces/namespaceyoda_1_1search/#variable-points)**  |
| | **[xs](/documentation/yoda_py/namespaces/namespaceyoda_1_1search/#variable-xs)**  |
| list | **[pms](/documentation/yoda_py/namespaces/namespaceyoda_1_1search/#variable-pms)**  |


## Functions Documentation

### function match_aos

```python
def match_aos(
    aos aos,
    patts patts,
    unpatts unpatts =None,
    search search =False
)
```

A tool for filtering AO collections by path patterns. 



```
Filter a list of analysis objects to those which match given path-matching patterns.

@a patts is a regex or iterable of regexes for positive matching, i.e. retention;
@a unpatts is the equivalent for negative matching, i.e. rejection even if a patt matches.

@a search will use Python regex search mode rather than match mode, i.e. match if any
part of the path fits the regex, rather than requiring a match from the start of the path.
```



## Attributes Documentation

### variable POINTS

```python
POINTS;
```


### variable xs

```python
xs =  linspace(5, 0, 10);
```


### variable pms

```python
list pms =  [PointMatcher("/foo   # this bit is a comment"),
           PointMatcher("/bar#1  # this bit is a comment"),
           PointMatcher("/baz/pi@2.76  # this bit is a comment"),
           PointMatcher("/baz/.*@:1.32 # this bit is a comment"),
           PointMatcher("/baz/.*@2.76: # this bit is a comment")];
```





-------------------------------

Updated on 2022-08-08 at 20:05:55 +0100
