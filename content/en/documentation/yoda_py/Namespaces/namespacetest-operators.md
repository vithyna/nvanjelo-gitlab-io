---

title: 'namespace test-operators'

description: "[Documentation update required.]"

---

# test-operators



## Attributes

|                | Name           |
| -------------- | -------------- |
| list | **[hs](/documentation/yoda_py/namespaces/namespacetest-operators/#variable-hs)**  |
| list | **[h](/documentation/yoda_py/namespaces/namespacetest-operators/#variable-h)**  |



## Attributes Documentation

### variable hs

```python
list hs =  [yoda.Histo1D(5, 0, 10, "/H%d" % i) for i in range(3)];
```


### variable h

```python
list h =  hs[0] + hs[1];
```





-------------------------------

Updated on 2022-08-08 at 20:05:55 +0100
