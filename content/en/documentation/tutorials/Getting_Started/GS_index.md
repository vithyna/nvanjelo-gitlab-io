---
title: "Getting Started"
description: ""
date: 2022-08-03T16:25:52+01:00
lastmod: 2022-08-03T16:25:52+01:00
draft: false
images: []
menu:
  documentation:
    parent: "tutorials"
    identifier: "gs"
    weight: 10
---
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b> <a href=/tutorials/getting_started/installation/>Installation<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b> <a href=/tutorials/getting_started/docker/>Docker Instructions<a></b><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b> <a href=/tutorials/getting_started/firstrun/>First Run<a></b><br>
