---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/build/rivet/hepdatapatches/CMS_2017_I1608166.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/build/rivet/hepdatapatches/CMS_2017_I1608166.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::CMS_2017_I1608166](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1cms__2017__i1608166/)**  |




## Source code

```python

def patch(path, ao):
    # fix bin widths
    if "CMS_2017_I1608166" in path:
        step=0.025
        for p in ao.points():
            p.setXErrs(step)
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
