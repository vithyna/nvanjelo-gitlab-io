---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/build/rivet/hepdatapatches/TASSO_1984_I195333.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/build/rivet/hepdatapatches/TASSO_1984_I195333.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::TASSO_1984_I195333](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1tasso__1984__i195333/)**  |




## Source code

```python
import yoda,math
# fix bin widths 
def patch(path, ao):
    if "TASSO_1984_I195333" in path and "d03" in path:
        for p in ao.points() :
            p.setXErrs(1)
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
