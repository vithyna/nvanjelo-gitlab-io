---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/rivet/hepdatapatches/DM2_1990_I297706.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/rivet/hepdatapatches/DM2_1990_I297706.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::DM2_1990_I297706](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1dm2__1990__i297706/)**  |




## Source code

```python

def patch(path, ao):
    needs_patching = [ 
      '/REF/DM2_1990_I297706/d01-x01-y01',
      '/REF/DM2_1990_I297706/d02-x01-y01'
    ]
    if path in needs_patching:
      for p in ao.points():
          p.setErrs(1, (0.5, 0.5))
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
