---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/rivet/hepdatapatches/CELLO_1992_I345437.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/rivet/hepdatapatches/CELLO_1992_I345437.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::CELLO_1992_I345437](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1cello__1992__i345437/)**  |




## Source code

```python

def patch(path, ao):
    if "CELLO_1992_I345437" in path :
        for p in ao.points():
            if "d01" in path  :
                if(p.x()<1.5) :
                    p.setXErrs(0.0125)
                else :
                    p.setXErrs(0.05)
            else :
                p.setXErrs(0.025)
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
