---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/rivet/hepdatapatches/HRS_1986_I17781.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/rivet/hepdatapatches/HRS_1986_I17781.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::HRS_1986_I17781](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1hrs__1986__i17781/)**  |




## Source code

```python
def patch(path, ao):
    if "HRS_1986_I17781" in path and "d03" in path :
        # add bin width for plottings
            for p in ao.points() : p.setXErrs(0.5)
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
