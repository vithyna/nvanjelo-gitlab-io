---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/build/rivet/hepdatapatches/CLEOII_1999_I478217.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/build/rivet/hepdatapatches/CLEOII_1999_I478217.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::CLEOII_1999_I478217](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1cleoii__1999__i478217/)**  |




## Source code

```python
def patch(path, ao):
    # fix bin widths
    if "CLEOII_1999_I478217" in path and "d01" in path:
        for p in ao.points():
            p.setXErrs(0.05)
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
