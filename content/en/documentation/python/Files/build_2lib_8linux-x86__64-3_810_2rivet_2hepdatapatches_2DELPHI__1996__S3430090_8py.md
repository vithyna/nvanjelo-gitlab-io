---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/build/lib.linux-x86_64-3.10/rivet/hepdatapatches/DELPHI_1996_S3430090.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/build/lib.linux-x86_64-3.10/rivet/hepdatapatches/DELPHI_1996_S3430090.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::DELPHI_1996_S3430090](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1delphi__1996__s3430090/)**  |




## Source code

```python
def patch(path, ao):
    # fix bin widths
    if "DELPHI_1996_S3430090" in path and ("d35" in path or "d36" in path or "d37" in path or
                                           "d38" in path or "d39" in path or "d40" in path):
        for p in ao.points():
            p.setXErrs(0.5)
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
