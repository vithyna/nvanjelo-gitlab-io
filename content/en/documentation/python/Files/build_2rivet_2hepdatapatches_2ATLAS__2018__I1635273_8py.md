---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/build/rivet/hepdatapatches/ATLAS_2018_I1635273.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/build/rivet/hepdatapatches/ATLAS_2018_I1635273.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::ATLAS_2018_I1635273](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1atlas__2018__i1635273/)**  |




## Source code

```python

def patch(path, ao):
    needs_patching = [ 
      '/REF/ATLAS_2018_I1635273/d01-x01-y01', 
      '/REF/ATLAS_2018_I1635273/d03-x01-y01'
      '/REF/ATLAS_2018_I1635273/d03-x01-y02'
      '/REF/ATLAS_2018_I1635273/d03-x01-y03'
      '/REF/ATLAS_2018_I1635273/d34-x01-y01'
    ]
    if path in needs_patching:
      for p in ao.points():
          p.setErrs(1, (0.5, 0.5))
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
