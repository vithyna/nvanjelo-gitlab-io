---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/build/rivet/hepdatapatches/OPAL_1997_I440721.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/build/rivet/hepdatapatches/OPAL_1997_I440721.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::OPAL_1997_I440721](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1opal__1997__i440721/)**  |




## Source code

```python
import yoda
def patch(path, ao):
    # bin widths
    if path == "/REF/OPAL_1997_I440721/d26-x01-y01":
        for p in ao.points() :
            p.setXErrs(1)
    elif path == "/REF/OPAL_1997_I440721/d02-x01-y01":
        for p in ao.points() :
            p.setXErrs(0.5)
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
