---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/rivet/hepdatapatches/TASSO_1989_I277658.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/rivet/hepdatapatches/TASSO_1989_I277658.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::TASSO_1989_I277658](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1tasso__1989__i277658/)**  |




## Source code

```python
import yoda
def patch(path, ao):
    # bin widths (still more things need fixing)
    if "TASSO_1989_I277658" in path :
        if "d05" in path :
            for p in ao.points() :
                p.setXErrs(1)
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
