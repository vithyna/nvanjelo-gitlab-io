---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/rivet/hepdatapatches/L3_1995_I381046.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/rivet/hepdatapatches/L3_1995_I381046.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::L3_1995_I381046](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1l3__1995__i381046/)**  |




## Source code

```python
import yoda
def patch(path, ao):
    # fix bin widths
    if path == "/REF/L3_1995_I381046/d01-x01-y01" :
        for p in ao.points() :
            p.setXErrs(0.5)
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
