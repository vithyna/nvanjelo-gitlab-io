---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/build/lib.linux-x86_64-3.10/rivet/hepdatapatches/DELPHI_1998_I473409.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/build/lib.linux-x86_64-3.10/rivet/hepdatapatches/DELPHI_1998_I473409.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::DELPHI_1998_I473409](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1delphi__1998__i473409/)**  |




## Source code

```python
def patch(path, ao):
    # fix bin widths
    if "DELPHI_1998_I473409" in path and ("d01" in path or "d02" in path or "d03" in path):
        for p in ao.points():
            p.setXErrs(0.5)
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
