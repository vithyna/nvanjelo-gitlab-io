---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/build/rivet/hepdatapatches/ATLAS_2019_I1734263.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/build/rivet/hepdatapatches/ATLAS_2019_I1734263.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::ATLAS_2019_I1734263](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1atlas__2019__i1734263/)**  |




## Source code

```python
def patch(path, ao):

    needs_patching = ['/REF/ATLAS_2019_I1734263/d01-x01-y01']

    if path in needs_patching:
        for p in ao.points():
            p.setErrs(1, (2.5, 2.5))
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
