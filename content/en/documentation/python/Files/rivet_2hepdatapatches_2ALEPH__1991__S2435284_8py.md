---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/rivet/hepdatapatches/ALEPH_1991_S2435284.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/rivet/hepdatapatches/ALEPH_1991_S2435284.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::ALEPH_1991_S2435284](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1aleph__1991__s2435284/)**  |




## Source code

```python
def patch(path, ao):
    if "ALEPH_1991_S2435284" in path:
        step = 1.
        if "d01" not in path : step=0.5
        for p in ao.points():
            p.setXErrs(step)
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
