---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/build/rivet/hepdatapatches/CLEO_2004_S5809304.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/build/rivet/hepdatapatches/CLEO_2004_S5809304.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::CLEO_2004_S5809304](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1cleo__2004__s5809304/)**  |




## Source code

```python
def patch(path, ao):
    # fix bin widths
    if "CLEO_2004_S5809304" in path and "d01" in path :
        for p in ao.points():
            p.setXErrs(0.5)
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
