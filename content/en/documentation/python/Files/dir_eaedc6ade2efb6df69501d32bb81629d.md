---

title: 'dir /home/anarendran/Documents/temp/rivet/pyext/build'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/build



## Directories

| Name           |
| -------------- |
| **[/home/anarendran/Documents/temp/rivet/pyext/build/lib.linux-x86_64-3.10](/documentation/python/files/dir_248c53b4c02d5c18d94876c6aae05ced/#dir-/home/anarendran/documents/temp/rivet/pyext/build/lib.linux-x86-64-3.10)**  |
| **[/home/anarendran/Documents/temp/rivet/pyext/build/rivet](/documentation/python/files/dir_d179a823c1e04fb6ab948c095d9ab401/#dir-/home/anarendran/documents/temp/rivet/pyext/build/rivet)**  |






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
