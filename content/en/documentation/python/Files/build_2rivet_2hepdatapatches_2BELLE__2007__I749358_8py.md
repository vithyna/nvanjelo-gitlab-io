---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/build/rivet/hepdatapatches/BELLE_2007_I749358.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/build/rivet/hepdatapatches/BELLE_2007_I749358.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::BELLE_2007_I749358](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1belle__2007__i749358/)**  |




## Source code

```python
def patch(path, ao):
    if "BELLE_2007_I749358" in path:
        step = 0.025
        if "d01" in path : step=0.0025
        for p in ao.points():
            p.setXErrs(step)
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
