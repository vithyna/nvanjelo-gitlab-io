---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/build/rivet/hepdatapatches/CRYSTAL_BALL_1988_I261078.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/build/rivet/hepdatapatches/CRYSTAL_BALL_1988_I261078.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::CRYSTAL_BALL_1988_I261078](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1crystal__ball__1988__i261078/)**  |




## Source code

```python

def patch(path, ao):
    needs_patching = [ 
      '/REF/CRYSTAL_BALL_1988_I261078/d01-x01-y01'
    ]
    if path in needs_patching:
      for p in ao.points():
          p.setErrs(1, (0.1, 0.1))
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
