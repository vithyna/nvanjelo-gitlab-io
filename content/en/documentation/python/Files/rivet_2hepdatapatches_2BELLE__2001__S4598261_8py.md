---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/rivet/hepdatapatches/BELLE_2001_S4598261.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/rivet/hepdatapatches/BELLE_2001_S4598261.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::BELLE_2001_S4598261](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1belle__2001__s4598261/)**  |




## Source code

```python
def patch(path, ao):
    # fix bin widths
    if "BELLE_2001_S4598261" in path and "d02" in path:
        for p in ao.points():
            p.setXErrs(0.5)
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
