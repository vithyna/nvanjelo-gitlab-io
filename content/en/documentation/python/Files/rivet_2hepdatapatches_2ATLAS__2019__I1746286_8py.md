---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/rivet/hepdatapatches/ATLAS_2019_I1746286.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/rivet/hepdatapatches/ATLAS_2019_I1746286.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::ATLAS_2019_I1746286](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1atlas__2019__i1746286/)**  |




## Source code

```python

def patch(path, ao):
    needs_patching = [ 
      '/REF/ATLAS_2019_I1746286/d05-x01-y01',
      '/REF/ATLAS_2019_I1746286/d10-x01-y01',
      '/REF/ATLAS_2019_I1746286/d14-x01-y01',
      '/REF/ATLAS_2019_I1746286/d18-x01-y01',
    ]
    if path in needs_patching:
      for p in ao.points():
          xLo, xHi = p.xErrs()
          p.setErrs(1, (xLo + 0.5, xHi + 0.5))
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
