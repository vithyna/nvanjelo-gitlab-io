---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/build/lib.linux-x86_64-3.10/rivet/hepdatapatches/MARKII_1988_I261194.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/build/lib.linux-x86_64-3.10/rivet/hepdatapatches/MARKII_1988_I261194.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::MARKII_1988_I261194](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1markii__1988__i261194/)**  |




## Source code

```python
def patch(path, ao):
    if "MARKII_1988_I261194" in path and "d01" in path:
        step = 0.05
        for p in ao.points():
            if p.x()<0.45 :
                p.setXErrs(step)
                step=0.025
            elif p.x()>0.5 :
                p.setXErrs(step)
                step=0.05
            else :
                p.setXErrs((step,0.05))
                step=0.05
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
