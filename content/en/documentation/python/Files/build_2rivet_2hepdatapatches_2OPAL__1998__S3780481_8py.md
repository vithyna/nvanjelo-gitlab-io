---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/build/rivet/hepdatapatches/OPAL_1998_S3780481.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/build/rivet/hepdatapatches/OPAL_1998_S3780481.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::OPAL_1998_S3780481](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1opal__1998__s3780481/)**  |




## Source code

```python
import yoda
def patch(path, ao):
    # bin widths
    if "OPAL_1998_S3780481" in path and "d09" in path :
        for p in ao.points() :
            p.setXErrs(0.5)
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
