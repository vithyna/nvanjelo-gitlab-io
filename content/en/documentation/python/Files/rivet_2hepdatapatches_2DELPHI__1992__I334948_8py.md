---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/rivet/hepdatapatches/DELPHI_1992_I334948.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/rivet/hepdatapatches/DELPHI_1992_I334948.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::DELPHI_1992_I334948](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1delphi__1992__i334948/)**  |




## Source code

```python
def patch(path, ao):
    # fix bin widths
    if "DELPHI_1992_I334948" in path:
        step=1.
        for p in ao.points():
            p.setXErrs(step)
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
