---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/rivet/hepdatapatches/JADE_1984_I202785.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/rivet/hepdatapatches/JADE_1984_I202785.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::JADE_1984_I202785](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1jade__1984__i202785/)**  |




## Source code

```python
def patch(path, ao):
    # fix bin widths
    if path == "/REF/JADE_1984_I202785/d03-x01-y01" :
        for p in ao.points() :
            p.setXErrs(0.15)
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
