---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/rivet/hepdatapatches/DELPHI_1991_I324035.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/rivet/hepdatapatches/DELPHI_1991_I324035.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::DELPHI_1991_I324035](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1delphi__1991__i324035/)**  |




## Source code

```python
def patch(path, ao):
    # fix bin widths
    if "DELPHI_1991_I324035" in path:
        step=0.5
        if("d05" in path) : step=1.
        for p in ao.points():
            p.setXErrs(step)
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
