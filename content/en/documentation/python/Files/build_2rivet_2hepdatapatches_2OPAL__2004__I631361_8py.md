---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/build/rivet/hepdatapatches/OPAL_2004_I631361.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/build/rivet/hepdatapatches/OPAL_2004_I631361.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::OPAL_2004_I631361](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1opal__2004__i631361/)**  |




## Source code

```python
import yoda
def patch(path, ao):
    # fix bin widths
    if ("OPAL_2004_I631361" in path and
        ("d01" in path or "d02" in path or "d03" in path ) ) :
        for p in ao.points() :
            p.setXErrs(0.5)
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
