---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/rivet/hepdatapatches/OPAL_1998_I474012.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/rivet/hepdatapatches/OPAL_1998_I474012.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::OPAL_1998_I474012](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1opal__1998__i474012/)**  |




## Source code

```python
import yoda
def patch(path, ao):
    # sign issue with bin widths
    if path == "/REF/OPAL_1998_I474012/d01-x01-y01":
        for p in ao.points() :
            p.setXErrs(0.5)
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
