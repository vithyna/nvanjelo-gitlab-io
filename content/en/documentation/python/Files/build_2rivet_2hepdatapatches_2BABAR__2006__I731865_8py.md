---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/build/rivet/hepdatapatches/BABAR_2006_I731865.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/build/rivet/hepdatapatches/BABAR_2006_I731865.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::BABAR_2006_I731865](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1babar__2006__i731865/)**  |




## Source code

```python

def patch(path, ao):
    needs_patching = [ 
      '/REF/BABAR_2006_I731865/d01-x01-y01',
      '/REF/BABAR_2006_I731865/d01-x01-y02'
    ]
    if path in needs_patching:
      for p in ao.points():
          p.setErrs(1, (0.5, 0.5))
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
