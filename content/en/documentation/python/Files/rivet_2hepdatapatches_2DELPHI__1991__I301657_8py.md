---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/rivet/hepdatapatches/DELPHI_1991_I301657.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/rivet/hepdatapatches/DELPHI_1991_I301657.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::DELPHI_1991_I301657](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1delphi__1991__i301657/)**  |




## Source code

```python
def patch(path, ao):
    # fix bin widths
    if "DELPHI_1991_I301657" in path and "d02" in path:
        for p in ao.points():
            p.setXErrs(1.)
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
