---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/build/rivet/hepdatapatches/NMD_1974_I745.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/build/rivet/hepdatapatches/NMD_1974_I745.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::NMD_1974_I745](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1nmd__1974__i745/)**  |




## Source code

```python

def patch(path, ao):
    needs_patching = [ 
      '/REF/NMD_1974_I745/d01-x01-y01',
      '/REF/NMD_1974_I745/d01-x01-y02',
    ]
    if path in needs_patching:
      for p in ao.points():
          p.setErrs(1, (0.1, 0.1))
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
