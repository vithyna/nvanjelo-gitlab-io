---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/build/lib.linux-x86_64-3.10/rivet/hepdatapatches/HRS_1986_I18688.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/build/lib.linux-x86_64-3.10/rivet/hepdatapatches/HRS_1986_I18688.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::HRS_1986_I18688](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1hrs__1986__i18688/)**  |




## Source code

```python
def patch(path, ao):
    # fix bin widths
    if "/REF/HRS_1986_I18688/d01"  in path:
        for p in ao.points() :
            if p.x()<0.4 : p.setXErrs(0.05)
            else :         p.setXErrs(0.15)
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
