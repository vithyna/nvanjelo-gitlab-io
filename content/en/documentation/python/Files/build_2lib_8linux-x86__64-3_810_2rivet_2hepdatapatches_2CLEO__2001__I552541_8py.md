---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/build/lib.linux-x86_64-3.10/rivet/hepdatapatches/CLEO_2001_I552541.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/build/lib.linux-x86_64-3.10/rivet/hepdatapatches/CLEO_2001_I552541.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::CLEO_2001_I552541](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1cleo__2001__i552541/)**  |




## Source code

```python
def patch(path, ao):
    # fix bin widths
    if "CLEO_2001_I552541" in path and ("d03" in path or "d04" in path):
        for p in ao.points():
            p.setXErrs(0.5)
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
