---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/rivet/hepdatapatches/DELPHI_1993_I360638.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/rivet/hepdatapatches/DELPHI_1993_I360638.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::DELPHI_1993_I360638](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1delphi__1993__i360638/)**  |




## Source code

```python
def patch(path, ao):
    # fix bin widths
    if "DELPHI_1993_I360638" in path and ("d02" in path or "d05" in path or "d06" in path):
        step=0.5
        for p in ao.points():
            p.setXErrs(step)
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
