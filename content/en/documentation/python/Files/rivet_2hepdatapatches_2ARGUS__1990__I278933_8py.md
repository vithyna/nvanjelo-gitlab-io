---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/rivet/hepdatapatches/ARGUS_1990_I278933.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/rivet/hepdatapatches/ARGUS_1990_I278933.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::ARGUS_1990_I278933](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1argus__1990__i278933/)**  |




## Source code

```python
def patch(path, ao):
    # fix bin widths
    if "ARGUS_1990_I278933" in path and ("d01" in path or "d02" in path ):
        for p in ao.points():
            p.setXErrs(0.5)
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
