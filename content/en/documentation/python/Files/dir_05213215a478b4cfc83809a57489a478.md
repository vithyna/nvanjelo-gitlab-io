---

title: 'dir /home/anarendran/Documents/temp/rivet/pyext'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext



## Directories

| Name           |
| -------------- |
| **[/home/anarendran/Documents/temp/rivet/pyext/build](/documentation/python/files/dir_eaedc6ade2efb6df69501d32bb81629d/#dir-/home/anarendran/documents/temp/rivet/pyext/build)**  |
| **[/home/anarendran/Documents/temp/rivet/pyext/rivet](/documentation/python/files/dir_f2741331efd8324b1d7271cf11af3522/#dir-/home/anarendran/documents/temp/rivet/pyext/rivet)**  |

## Files

| Name           |
| -------------- |
| **[/home/anarendran/Documents/temp/rivet/pyext/build.py](/documentation/python/files/build_8py/#file-build.py)**  |
| **[/home/anarendran/Documents/temp/rivet/pyext/setup.py](/documentation/python/files/setup_8py/#file-setup.py)**  |






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
