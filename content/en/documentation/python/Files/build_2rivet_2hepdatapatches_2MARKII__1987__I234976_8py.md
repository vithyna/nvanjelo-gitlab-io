---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/build/rivet/hepdatapatches/MARKII_1987_I234976.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/build/rivet/hepdatapatches/MARKII_1987_I234976.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::MARKII_1987_I234976](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1markii__1987__i234976/)**  |




## Source code

```python
def patch(path, ao):
    if "MARKII_1987_I234976" in path and "d01" not in path:
        step = 0.5
        for p in ao.points():
            p.setXErrs(step)
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
