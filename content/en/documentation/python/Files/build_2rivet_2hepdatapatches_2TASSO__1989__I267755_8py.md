---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/build/rivet/hepdatapatches/TASSO_1989_I267755.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/build/rivet/hepdatapatches/TASSO_1989_I267755.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::TASSO_1989_I267755](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1tasso__1989__i267755/)**  |




## Source code

```python
import yoda
def patch(path, ao):
    # sign issue with bin widths (still need to fix ordering issue)
    if path == "/REF/TASSO_1989_I267755/d05-x01-y01":
        for p in ao.points() :
            errs=p.xErrs()
            if(errs[0]<0.) :
                p.setXErrs((-errs[1],-errs[0]))
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
