---

title: 'dir /home/anarendran/Documents/temp/rivet/pyext/build/lib.linux-x86_64-3.10/rivet'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/build/lib.linux-x86_64-3.10/rivet



## Directories

| Name           |
| -------------- |
| **[/home/anarendran/Documents/temp/rivet/pyext/build/lib.linux-x86_64-3.10/rivet/hepdatapatches](/documentation/python/files/dir_2897384d2f797a7dac6bf546ac36da0c/#dir-/home/anarendran/documents/temp/rivet/pyext/build/lib.linux-x86-64-3.10/rivet/hepdatapatches)**  |

## Files

| Name           |
| -------------- |
| **[/home/anarendran/Documents/temp/rivet/pyext/build/lib.linux-x86_64-3.10/rivet/__init__.py](/documentation/python/files/build_2lib_8linux-x86__64-3_810_2rivet_2____init_____8py/#file---init--.py)**  |
| **[/home/anarendran/Documents/temp/rivet/pyext/build/lib.linux-x86_64-3.10/rivet/aopaths.py](/documentation/python/files/build_2lib_8linux-x86__64-3_810_2rivet_2aopaths_8py/#file-aopaths.py)**  |
| **[/home/anarendran/Documents/temp/rivet/pyext/build/lib.linux-x86_64-3.10/rivet/hepdatautils.py](/documentation/python/files/build_2lib_8linux-x86__64-3_810_2rivet_2hepdatautils_8py/#file-hepdatautils.py)**  |
| **[/home/anarendran/Documents/temp/rivet/pyext/build/lib.linux-x86_64-3.10/rivet/plotinfo.py](/documentation/python/files/build_2lib_8linux-x86__64-3_810_2rivet_2plotinfo_8py/#file-plotinfo.py)**  |
| **[/home/anarendran/Documents/temp/rivet/pyext/build/lib.linux-x86_64-3.10/rivet/spiresbib.py](/documentation/python/files/build_2lib_8linux-x86__64-3_810_2rivet_2spiresbib_8py/#file-spiresbib.py)**  |
| **[/home/anarendran/Documents/temp/rivet/pyext/build/lib.linux-x86_64-3.10/rivet/util.py](/documentation/python/files/build_2lib_8linux-x86__64-3_810_2rivet_2util_8py/#file-util.py)**  |






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
