---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/build/rivet/hepdatapatches/L3_1990_I298078.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/build/rivet/hepdatapatches/L3_1990_I298078.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::L3_1990_I298078](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1l3__1990__i298078/)**  |




## Source code

```python
# bin widths
def patch(path, ao):
    if "L3_1990_I298078" in path and "d01" in path:
        for i in range(0,len(ao.points())) :
            if(i<6) :
                ao.points()[i].setXErrs(5e-3)
            elif(i==6) :
                ao.points()[i].setXErrs((5e-3,1e-2))
            else :
                ao.points()[i].setXErrs(0.01)
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
