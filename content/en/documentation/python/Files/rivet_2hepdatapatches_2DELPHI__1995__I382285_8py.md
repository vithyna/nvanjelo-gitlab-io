---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/rivet/hepdatapatches/DELPHI_1995_I382285.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/rivet/hepdatapatches/DELPHI_1995_I382285.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::DELPHI_1995_I382285](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1delphi__1995__i382285/)**  |




## Source code

```python
def patch(path, ao):
    # fix bin widths
    if "DELPHI_1995_I382285" in path and "d01" in path:
        for p in ao.points():
            p.setXErrs(0.5)
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
