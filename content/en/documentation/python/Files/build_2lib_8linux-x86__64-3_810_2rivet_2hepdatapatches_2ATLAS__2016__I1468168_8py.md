---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/build/lib.linux-x86_64-3.10/rivet/hepdatapatches/ATLAS_2016_I1468168.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/build/lib.linux-x86_64-3.10/rivet/hepdatapatches/ATLAS_2016_I1468168.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::ATLAS_2016_I1468168](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1atlas__2016__i1468168/)**  |




## Source code

```python

def patch(path, ao):
    needs_patching = [ 
      '/REF/ATLAS_2016_I1468168/d01-x01-y01', 
      '/REF/ATLAS_2016_I1468168/d02-x01-y01'
    ]
    if path in needs_patching:
      for p in ao.points():
          p.setErrs(1, (0.5, 0.5))
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
