---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/rivet/hepdatapatches/VENUS_1995_I392360.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/rivet/hepdatapatches/VENUS_1995_I392360.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::VENUS_1995_I392360](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1venus__1995__i392360/)**  |




## Source code

```python
def patch(path, ao):
    if "VENUS_1995_I392360" in path:
        step = 0.05
        if "d01" in path : step=0.025
        for p in ao.points():
            p.setXErrs(step)
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
