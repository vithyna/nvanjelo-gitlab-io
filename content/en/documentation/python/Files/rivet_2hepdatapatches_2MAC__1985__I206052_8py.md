---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/rivet/hepdatapatches/MAC_1985_I206052.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/rivet/hepdatapatches/MAC_1985_I206052.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::MAC_1985_I206052](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1mac__1985__i206052/)**  |




## Source code

```python

def patch(path, ao):
    needs_patching = [ 
      '/REF/MAC_1985_I206052/d01-x01-y01',
      '/REF/MAC_1985_I206052/d02-x01-y01',
      '/REF/MAC_1985_I206052/d02-x01-y02',
    ]
    if path in needs_patching:
      for p in ao.points():
          p.setErrs(1, (0.1, 0.1))
    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
