---

title: 'file /home/anarendran/Documents/temp/rivet/pyext/build/lib.linux-x86_64-3.10/rivet/hepdatapatches/PLUTO_1980_I154270.py'

description: "[Documentation update required.]"

---

# /home/anarendran/Documents/temp/rivet/pyext/build/lib.linux-x86_64-3.10/rivet/hepdatapatches/PLUTO_1980_I154270.py



## Namespaces

| Name           |
| -------------- |
| **[rivet](/documentation/python/namespaces/namespacerivet/)**  |
| **[rivet::hepdatapatches](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches/)**  |
| **[rivet::hepdatapatches::PLUTO_1980_I154270](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1pluto__1980__i154270/)**  |




## Source code

```python
import yoda,math
# removal of average bins and bin number -> cms energy
def patch(path, ao):
    if path == "/REF/PLUTO_1980_I154270/d01-x01-y01" :
        newAO = yoda.core.Scatter2D()
        newAO.setPath(ao.path())
        for i in range(0,len(ao.points())) :
            if ao.points()[i].x()!=30.75 :
                newAO.addPoint(ao.points()[i])
        ao=newAO

    return ao
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
