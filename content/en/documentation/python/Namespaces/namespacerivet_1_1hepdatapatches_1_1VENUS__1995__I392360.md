---

title: 'namespace rivet::hepdatapatches::VENUS_1995_I392360'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::VENUS_1995_I392360



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1venus__1995__i392360/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
