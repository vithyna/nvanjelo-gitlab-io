---

title: 'namespace rivet::hepdatapatches'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches



## Namespaces

| Name           |
| -------------- |
| **[rivet::hepdatapatches::ALEPH_1991_S2435284](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1aleph__1991__s2435284/)**  |
| **[rivet::hepdatapatches::ALEPH_1996_I402895](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1aleph__1996__i402895/)**  |
| **[rivet::hepdatapatches::ALEPH_1996_S3486095](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1aleph__1996__s3486095/)**  |
| **[rivet::hepdatapatches::AMY_1990_I283337](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1amy__1990__i283337/)**  |
| **[rivet::hepdatapatches::AMY_1990_I295160](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1amy__1990__i295160/)**  |
| **[rivet::hepdatapatches::AMY_1995_I406129](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1amy__1995__i406129/)**  |
| **[rivet::hepdatapatches::ARGUS_1988_I251097](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1argus__1988__i251097/)**  |
| **[rivet::hepdatapatches::ARGUS_1989_I262551](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1argus__1989__i262551/)**  |
| **[rivet::hepdatapatches::ARGUS_1989_I268577](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1argus__1989__i268577/)**  |
| **[rivet::hepdatapatches::ARGUS_1989_I278932](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1argus__1989__i278932/)**  |
| **[rivet::hepdatapatches::ARGUS_1989_I280943](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1argus__1989__i280943/)**  |
| **[rivet::hepdatapatches::ARGUS_1989_I282570](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1argus__1989__i282570/)**  |
| **[rivet::hepdatapatches::ARGUS_1990_I278933](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1argus__1990__i278933/)**  |
| **[rivet::hepdatapatches::ARGUS_1991_I315059](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1argus__1991__i315059/)**  |
| **[rivet::hepdatapatches::ARGUS_1992_I319102](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1argus__1992__i319102/)**  |
| **[rivet::hepdatapatches::ARGUS_1993_S2653028](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1argus__1993__s2653028/)**  |
| **[rivet::hepdatapatches::ARGUS_1993_S2789213](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1argus__1993__s2789213/)**  |
| **[rivet::hepdatapatches::ARGUS_1994_I354224](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1argus__1994__i354224/)**  |
| **[rivet::hepdatapatches::ATLAS_2016_I1468168](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1atlas__2016__i1468168/)**  |
| **[rivet::hepdatapatches::ATLAS_2018_I1635273](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1atlas__2018__i1635273/)**  |
| **[rivet::hepdatapatches::ATLAS_2018_I1711223](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1atlas__2018__i1711223/)**  |
| **[rivet::hepdatapatches::ATLAS_2019_I1720438](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1atlas__2019__i1720438/)**  |
| **[rivet::hepdatapatches::ATLAS_2019_I1734263](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1atlas__2019__i1734263/)**  |
| **[rivet::hepdatapatches::ATLAS_2019_I1746286](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1atlas__2019__i1746286/)**  |
| **[rivet::hepdatapatches::ATLAS_2020_I1790439](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1atlas__2020__i1790439/)**  |
| **[rivet::hepdatapatches::ATLAS_2022_I2077570](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1atlas__2022__i2077570/)**  |
| **[rivet::hepdatapatches::BABAR_2001_I558091](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1babar__2001__i558091/)**  |
| **[rivet::hepdatapatches::BABAR_2005_S6181155](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1babar__2005__s6181155/)**  |
| **[rivet::hepdatapatches::BABAR_2006_I731865](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1babar__2006__i731865/)**  |
| **[rivet::hepdatapatches::BABAR_2007_S6895344](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1babar__2007__s6895344/)**  |
| **[rivet::hepdatapatches::BELLE_2001_S4598261](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1belle__2001__s4598261/)**  |
| **[rivet::hepdatapatches::BELLE_2007_I749358](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1belle__2007__i749358/)**  |
| **[rivet::hepdatapatches::BELLE_2009_I823878](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1belle__2009__i823878/)**  |
| **[rivet::hepdatapatches::BELLE_2016_I1389855](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1belle__2016__i1389855/)**  |
| **[rivet::hepdatapatches::BESII_2004_I622224](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1besii__2004__i622224/)**  |
| **[rivet::hepdatapatches::BESIII_2015_I1391138](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1besiii__2015__i1391138/)**  |
| **[rivet::hepdatapatches::CELLO_1982_I12010](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1cello__1982__i12010/)**  |
| **[rivet::hepdatapatches::CELLO_1983_I191415](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1cello__1983__i191415/)**  |
| **[rivet::hepdatapatches::CELLO_1990_I283026](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1cello__1990__i283026/)**  |
| **[rivet::hepdatapatches::CELLO_1992_I345437](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1cello__1992__i345437/)**  |
| **[rivet::hepdatapatches::CLEO_1984_I193577](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1cleo__1984__i193577/)**  |
| **[rivet::hepdatapatches::CLEO_1985_I205668](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1cleo__1985__i205668/)**  |
| **[rivet::hepdatapatches::CLEO_1998_I445351](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1cleo__1998__i445351/)**  |
| **[rivet::hepdatapatches::CLEO_2000_I526554](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1cleo__2000__i526554/)**  |
| **[rivet::hepdatapatches::CLEO_2001_I552541](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1cleo__2001__i552541/)**  |
| **[rivet::hepdatapatches::CLEO_2004_S5809304](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1cleo__2004__s5809304/)**  |
| **[rivet::hepdatapatches::CLEOII_1999_I478217](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1cleoii__1999__i478217/)**  |
| **[rivet::hepdatapatches::CLEOII_1999_I504672](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1cleoii__1999__i504672/)**  |
| **[rivet::hepdatapatches::CMD3_2016_I1385598](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1cmd3__2016__i1385598/)**  |
| **[rivet::hepdatapatches::CMS_2016_I1487288](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1cms__2016__i1487288/)**  |
| **[rivet::hepdatapatches::CMS_2017_I1608166](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1cms__2017__i1608166/)**  |
| **[rivet::hepdatapatches::CRYSTAL_BALL_1988_I261078](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1crystal__ball__1988__i261078/)**  |
| **[rivet::hepdatapatches::DELPHI_1990_I297698](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1delphi__1990__i297698/)**  |
| **[rivet::hepdatapatches::DELPHI_1991_I301657](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1delphi__1991__i301657/)**  |
| **[rivet::hepdatapatches::DELPHI_1991_I324035](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1delphi__1991__i324035/)**  |
| **[rivet::hepdatapatches::DELPHI_1992_I334948](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1delphi__1992__i334948/)**  |
| **[rivet::hepdatapatches::DELPHI_1993_I356732](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1delphi__1993__i356732/)**  |
| **[rivet::hepdatapatches::DELPHI_1993_I360638](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1delphi__1993__i360638/)**  |
| **[rivet::hepdatapatches::DELPHI_1995_I377487](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1delphi__1995__i377487/)**  |
| **[rivet::hepdatapatches::DELPHI_1995_I382285](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1delphi__1995__i382285/)**  |
| **[rivet::hepdatapatches::DELPHI_1995_I394052](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1delphi__1995__i394052/)**  |
| **[rivet::hepdatapatches::DELPHI_1995_S3137023](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1delphi__1995__s3137023/)**  |
| **[rivet::hepdatapatches::DELPHI_1996_S3430090](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1delphi__1996__s3430090/)**  |
| **[rivet::hepdatapatches::DELPHI_1998_I473409](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1delphi__1998__i473409/)**  |
| **[rivet::hepdatapatches::DELPHI_1999_I448370](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1delphi__1999__i448370/)**  |
| **[rivet::hepdatapatches::DELPHI_2001_I526164](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1delphi__2001__i526164/)**  |
| **[rivet::hepdatapatches::DELPHI_2003_I620250](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1delphi__2003__i620250/)**  |
| **[rivet::hepdatapatches::DELPHI_2011_I890503](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1delphi__2011__i890503/)**  |
| **[rivet::hepdatapatches::DM1_1979_I132828](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1dm1__1979__i132828/)**  |
| **[rivet::hepdatapatches::DM2_1990_I297706](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1dm2__1990__i297706/)**  |
| **[rivet::hepdatapatches::E605_1991_I302822](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1e605__1991__i302822/)**  |
| **[rivet::hepdatapatches::HRS_1985_I201482](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1hrs__1985__i201482/)**  |
| **[rivet::hepdatapatches::HRS_1986_I17781](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1hrs__1986__i17781/)**  |
| **[rivet::hepdatapatches::HRS_1986_I18502](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1hrs__1986__i18502/)**  |
| **[rivet::hepdatapatches::HRS_1986_I18688](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1hrs__1986__i18688/)**  |
| **[rivet::hepdatapatches::HRS_1987_I215848](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1hrs__1987__i215848/)**  |
| **[rivet::hepdatapatches::HRS_1988_I250824](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1hrs__1988__i250824/)**  |
| **[rivet::hepdatapatches::HRS_1989_I276948](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1hrs__1989__i276948/)**  |
| **[rivet::hepdatapatches::HRS_1990_I280958](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1hrs__1990__i280958/)**  |
| **[rivet::hepdatapatches::HRS_1992_I339573](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1hrs__1992__i339573/)**  |
| **[rivet::hepdatapatches::JADE_1984_I202785](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1jade__1984__i202785/)**  |
| **[rivet::hepdatapatches::JADE_1985_I213948](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1jade__1985__i213948/)**  |
| **[rivet::hepdatapatches::JADE_1990_I282847](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1jade__1990__i282847/)**  |
| **[rivet::hepdatapatches::L3_1990_I298078](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1l3__1990__i298078/)**  |
| **[rivet::hepdatapatches::L3_1992_I334954](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1l3__1992__i334954/)**  |
| **[rivet::hepdatapatches::L3_1995_I381046](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1l3__1995__i381046/)**  |
| **[rivet::hepdatapatches::L3_1997_I427107](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1l3__1997__i427107/)**  |
| **[rivet::hepdatapatches::L3_2004_I652683](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1l3__2004__i652683/)**  |
| **[rivet::hepdatapatches::LENA_1982_I179431](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1lena__1982__i179431/)**  |
| **[rivet::hepdatapatches::LHCB_2016_I1454404](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1lhcb__2016__i1454404/)**  |
| **[rivet::hepdatapatches::LHCB_2018_I1662483](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1lhcb__2018__i1662483/)**  |
| **[rivet::hepdatapatches::MAC_1985_I202924](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1mac__1985__i202924/)**  |
| **[rivet::hepdatapatches::MAC_1985_I206052](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1mac__1985__i206052/)**  |
| **[rivet::hepdatapatches::MARKI_1975_I100733](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1marki__1975__i100733/)**  |
| **[rivet::hepdatapatches::MARKII_1982_I177606](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1markii__1982__i177606/)**  |
| **[rivet::hepdatapatches::MARKII_1985_I207785](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1markii__1985__i207785/)**  |
| **[rivet::hepdatapatches::MARKII_1985_I209198](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1markii__1985__i209198/)**  |
| **[rivet::hepdatapatches::MARKII_1987_I234976](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1markii__1987__i234976/)**  |
| **[rivet::hepdatapatches::MARKII_1987_I247900](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1markii__1987__i247900/)**  |
| **[rivet::hepdatapatches::MARKII_1988_I246184](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1markii__1988__i246184/)**  |
| **[rivet::hepdatapatches::MARKII_1988_I261194](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1markii__1988__i261194/)**  |
| **[rivet::hepdatapatches::MARKII_1991_I295286](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1markii__1991__i295286/)**  |
| **[rivet::hepdatapatches::MARKJ_1979_I141976](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1markj__1979__i141976/)**  |
| **[rivet::hepdatapatches::NA22_1986_I18431](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1na22__1986__i18431/)**  |
| **[rivet::hepdatapatches::NMD_1974_I745](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1nmd__1974__i745/)**  |
| **[rivet::hepdatapatches::OPAL_1992_I321190](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1opal__1992__i321190/)**  |
| **[rivet::hepdatapatches::OPAL_1993_I342766](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1opal__1993__i342766/)**  |
| **[rivet::hepdatapatches::OPAL_1993_S2692198](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1opal__1993__s2692198/)**  |
| **[rivet::hepdatapatches::OPAL_1994_S2927284](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1opal__1994__s2927284/)**  |
| **[rivet::hepdatapatches::OPAL_1997_I440103](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1opal__1997__i440103/)**  |
| **[rivet::hepdatapatches::OPAL_1997_I440721](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1opal__1997__i440721/)**  |
| **[rivet::hepdatapatches::OPAL_1998_I474012](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1opal__1998__i474012/)**  |
| **[rivet::hepdatapatches::OPAL_1998_S3749908](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1opal__1998__s3749908/)**  |
| **[rivet::hepdatapatches::OPAL_1998_S3780481](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1opal__1998__s3780481/)**  |
| **[rivet::hepdatapatches::OPAL_2000_I474010](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1opal__2000__i474010/)**  |
| **[rivet::hepdatapatches::OPAL_2000_I513476](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1opal__2000__i513476/)**  |
| **[rivet::hepdatapatches::OPAL_2001_I536266](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1opal__2001__i536266/)**  |
| **[rivet::hepdatapatches::OPAL_2003_I599181](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1opal__2003__i599181/)**  |
| **[rivet::hepdatapatches::OPAL_2004_I631361](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1opal__2004__i631361/)**  |
| **[rivet::hepdatapatches::OPAL_2004_S6132243](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1opal__2004__s6132243/)**  |
| **[rivet::hepdatapatches::PLUTO_1979_I142517](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1pluto__1979__i142517/)**  |
| **[rivet::hepdatapatches::PLUTO_1980_I154270](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1pluto__1980__i154270/)**  |
| **[rivet::hepdatapatches::PLUTO_1981_I165122](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1pluto__1981__i165122/)**  |
| **[rivet::hepdatapatches::SLD_1995_I378545](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1sld__1995__i378545/)**  |
| **[rivet::hepdatapatches::TASSO_1980_I153656](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1tasso__1980__i153656/)**  |
| **[rivet::hepdatapatches::TASSO_1984_I194774](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1tasso__1984__i194774/)**  |
| **[rivet::hepdatapatches::TASSO_1984_I195333](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1tasso__1984__i195333/)**  |
| **[rivet::hepdatapatches::TASSO_1986_I230950](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1tasso__1986__i230950/)**  |
| **[rivet::hepdatapatches::TASSO_1988_I263859](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1tasso__1988__i263859/)**  |
| **[rivet::hepdatapatches::TASSO_1989_I266893](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1tasso__1989__i266893/)**  |
| **[rivet::hepdatapatches::TASSO_1989_I267755](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1tasso__1989__i267755/)**  |
| **[rivet::hepdatapatches::TASSO_1989_I277658](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1tasso__1989__i277658/)**  |
| **[rivet::hepdatapatches::TASSO_1990_S2148048](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1tasso__1990__s2148048/)**  |
| **[rivet::hepdatapatches::TOPAZ_1993_I361661](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1topaz__1993__i361661/)**  |
| **[rivet::hepdatapatches::TOPAZ_1995_I381777](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1topaz__1995__i381777/)**  |
| **[rivet::hepdatapatches::TOPAZ_1995_I381900](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1topaz__1995__i381900/)**  |
| **[rivet::hepdatapatches::TPC_1985_I205868](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1tpc__1985__i205868/)**  |
| **[rivet::hepdatapatches::TPC_1987_I235694](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1tpc__1987__i235694/)**  |
| **[rivet::hepdatapatches::TPC_1988_I262143](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1tpc__1988__i262143/)**  |
| **[rivet::hepdatapatches::UA5_1982_S875503](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1ua5__1982__s875503/)**  |
| **[rivet::hepdatapatches::UA5_1987_S1640666](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1ua5__1987__s1640666/)**  |
| **[rivet::hepdatapatches::UA5_1989_S1926373](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1ua5__1989__s1926373/)**  |
| **[rivet::hepdatapatches::VENUS_1995_I392360](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1venus__1995__i392360/)**  |
| **[rivet::hepdatapatches::VENUS_1999_I500179](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1venus__1999__i500179/)**  |






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
