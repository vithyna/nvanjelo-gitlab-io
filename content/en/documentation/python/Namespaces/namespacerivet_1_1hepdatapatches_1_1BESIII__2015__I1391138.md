---

title: 'namespace rivet::hepdatapatches::BESIII_2015_I1391138'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::BESIII_2015_I1391138



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1besiii__2015__i1391138/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
