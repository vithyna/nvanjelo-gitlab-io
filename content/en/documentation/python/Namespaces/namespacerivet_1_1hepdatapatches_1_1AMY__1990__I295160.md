---

title: 'namespace rivet::hepdatapatches::AMY_1990_I295160'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::AMY_1990_I295160



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1amy__1990__i295160/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
