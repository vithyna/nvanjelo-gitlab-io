---

title: 'namespace rivet::hepdatapatches::CLEO_2000_I526554'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::CLEO_2000_I526554



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1cleo__2000__i526554/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
