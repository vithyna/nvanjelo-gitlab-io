---

title: 'namespace rivet::hepdatapatches::DELPHI_1995_I377487'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::DELPHI_1995_I377487



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1delphi__1995__i377487/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
