---

title: 'namespace rivet::hepdatapatches::CELLO_1992_I345437'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::CELLO_1992_I345437



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1cello__1992__i345437/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
