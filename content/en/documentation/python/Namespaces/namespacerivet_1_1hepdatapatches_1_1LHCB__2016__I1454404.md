---

title: 'namespace rivet::hepdatapatches::LHCB_2016_I1454404'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::LHCB_2016_I1454404



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1lhcb__2016__i1454404/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
