---

title: 'namespace rivet::hepdatapatches::TASSO_1988_I263859'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::TASSO_1988_I263859



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1tasso__1988__i263859/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
