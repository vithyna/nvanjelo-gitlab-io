---

title: 'namespace rivet::hepdatapatches::L3_2004_I652683'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::L3_2004_I652683



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1l3__2004__i652683/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
