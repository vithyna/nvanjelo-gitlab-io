---

title: 'namespace rivet::hepdatapatches::SLD_1995_I378545'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::SLD_1995_I378545



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1sld__1995__i378545/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
