---

title: 'namespace rivet::hepdatapatches::E605_1991_I302822'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::E605_1991_I302822



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1e605__1991__i302822/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
