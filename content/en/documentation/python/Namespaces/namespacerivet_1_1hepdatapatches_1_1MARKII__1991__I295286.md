---

title: 'namespace rivet::hepdatapatches::MARKII_1991_I295286'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::MARKII_1991_I295286



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1markii__1991__i295286/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
