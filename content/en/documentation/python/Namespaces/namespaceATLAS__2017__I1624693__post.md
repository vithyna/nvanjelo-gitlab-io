---

title: 'namespace ATLAS_2017_I1624693_post'

description: "[Documentation update required.]"

---

# ATLAS_2017_I1624693_post



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[getRivetRefData](/documentation/python/namespaces/namespaceatlas__2017__i1624693__post/#function-getrivetrefdata)**(anas anas =None) |
| def | **[constructDiff](/documentation/python/namespaces/namespaceatlas__2017__i1624693__post/#function-constructdiff)**(hist hist) |

## Attributes

|                | Name           |
| -------------- | -------------- |
| string | **[inFile](/documentation/python/namespaces/namespaceatlas__2017__i1624693__post/#variable-infile)**  |
| | **[hists](/documentation/python/namespaces/namespaceatlas__2017__i1624693__post/#variable-hists)**  |
| | **[tags](/documentation/python/namespaces/namespaceatlas__2017__i1624693__post/#variable-tags)**  |
| def | **[refhistos](/documentation/python/namespaces/namespaceatlas__2017__i1624693__post/#variable-refhistos)**  |
| | **[f](/documentation/python/namespaces/namespaceatlas__2017__i1624693__post/#variable-f)**  |
| def | **[hdiff](/documentation/python/namespaces/namespaceatlas__2017__i1624693__post/#variable-hdiff)**  |
| | **[outName](/documentation/python/namespaces/namespaceatlas__2017__i1624693__post/#variable-outname)**  |


## Functions Documentation

### function getRivetRefData

```python
def getRivetRefData(
    anas anas =None
)
```


### function constructDiff

```python
def constructDiff(
    hist hist
)
```




```
This function produces a (data - MC)/sigma version of the Dalitz (2D) plot.```



## Attributes Documentation

### variable inFile

```python
string inFile =  'Rivet.yoda';
```


### variable hists

```python
hists =  yoda.read( inFile );
```


### variable tags

```python
tags =  sorted(hists.keys());
```


### variable refhistos

```python
def refhistos =  getRivetRefData(['ATLAS_2017_I1624693']);
```


### variable f

```python
f =  open('%s_processed.yoda' % inFile[:-5], 'w');
```


### variable hdiff

```python
def hdiff =  constructDiff(hists[h]);
```


### variable outName

```python
outName =  h.replace('y01', 'y02');
```





-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
