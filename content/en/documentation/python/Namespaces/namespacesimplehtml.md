---

title: 'namespace simplehtml'

description: "[Documentation update required.]"

---

# simplehtml



## Classes

|                | Name           |
| -------------- | -------------- |
| class | **[simplehtml::HTML](/documentation/python/classes/classsimplehtml_1_1html/)**  |
| class | **[simplehtml::TestCase](/documentation/python/classes/classsimplehtml_1_1testcase/)**  |
| class | **[simplehtml::XHTML](/documentation/python/classes/classsimplehtml_1_1xhtml/)**  |
| class | **[simplehtml::XML](/documentation/python/classes/classsimplehtml_1_1xml/)**  |






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
