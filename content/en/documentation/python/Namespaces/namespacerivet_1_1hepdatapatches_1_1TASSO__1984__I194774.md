---

title: 'namespace rivet::hepdatapatches::TASSO_1984_I194774'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::TASSO_1984_I194774



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1tasso__1984__i194774/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
