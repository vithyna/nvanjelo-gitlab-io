---

title: 'namespace rivet::hepdatapatches::HRS_1986_I17781'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::HRS_1986_I17781



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1hrs__1986__i17781/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
