---

title: 'namespace rivet::hepdatapatches::DELPHI_1995_S3137023'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::DELPHI_1995_S3137023



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1delphi__1995__s3137023/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
