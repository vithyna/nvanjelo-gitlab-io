---

title: 'namespace rivet::hepdatapatches::L3_1997_I427107'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::L3_1997_I427107



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1l3__1997__i427107/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
