---

title: 'namespace rivet::hepdatapatches::BELLE_2009_I823878'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::BELLE_2009_I823878



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1belle__2009__i823878/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
