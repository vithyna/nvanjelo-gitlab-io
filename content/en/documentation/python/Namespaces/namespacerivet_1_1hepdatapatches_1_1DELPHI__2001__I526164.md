---

title: 'namespace rivet::hepdatapatches::DELPHI_2001_I526164'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::DELPHI_2001_I526164



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1delphi__2001__i526164/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
