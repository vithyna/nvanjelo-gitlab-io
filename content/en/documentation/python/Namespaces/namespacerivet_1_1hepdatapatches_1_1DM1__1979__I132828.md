---

title: 'namespace rivet::hepdatapatches::DM1_1979_I132828'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::DM1_1979_I132828



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1dm1__1979__i132828/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
