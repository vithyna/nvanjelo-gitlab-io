---

title: 'namespace rivet::hepdatapatches::CMD3_2016_I1385598'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::CMD3_2016_I1385598



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1cmd3__2016__i1385598/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
