---

title: 'namespace getallauthors'

description: "[Documentation update required.]"

---

# getallauthors



## Attributes

|                | Name           |
| -------------- | -------------- |
| dictionary | **[authors_emails](/documentation/python/namespaces/namespacegetallauthors/#variable-authors-emails)**  |
| | **[ana](/documentation/python/namespaces/namespacegetallauthors/#variable-ana)**  |
| | **[au](/documentation/python/namespaces/namespacegetallauthors/#variable-au)**  |
| | **[em](/documentation/python/namespaces/namespacegetallauthors/#variable-em)**  |
| | **[m](/documentation/python/namespaces/namespacegetallauthors/#variable-m)**  |



## Attributes Documentation

### variable authors_emails

```python
dictionary authors_emails =  {};
```


### variable ana

```python
ana =  rivet.AnalysisLoader.getAnalysis(aname);
```


### variable au

```python
au =  au_em;
```


### variable em

```python
em =  au_em;
```


### variable m

```python
m =  re.search("(.*)<(.*)>.*", au_em);
```





-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
