---

title: 'namespace rivet::hepdatapatches::ARGUS_1989_I262551'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::ARGUS_1989_I262551



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1argus__1989__i262551/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
