---

title: 'namespace rivet::hepdatapatches::VENUS_1999_I500179'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::VENUS_1999_I500179



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1venus__1999__i500179/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
