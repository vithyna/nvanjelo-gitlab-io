---

title: 'namespace rivet::hepdatapatches::PLUTO_1980_I154270'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::PLUTO_1980_I154270



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1pluto__1980__i154270/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
