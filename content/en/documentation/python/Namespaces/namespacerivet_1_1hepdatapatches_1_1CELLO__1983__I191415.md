---

title: 'namespace rivet::hepdatapatches::CELLO_1983_I191415'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::CELLO_1983_I191415



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1cello__1983__i191415/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
