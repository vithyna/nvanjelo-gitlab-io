---

title: 'namespace rivet::hepdatapatches::PLUTO_1981_I165122'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::PLUTO_1981_I165122



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1pluto__1981__i165122/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
