---

title: 'namespace rivet::hepdatapatches::LHCB_2018_I1662483'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::LHCB_2018_I1662483



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1lhcb__2018__i1662483/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
