---

title: 'namespace rivet::hepdatapatches::HRS_1992_I339573'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::HRS_1992_I339573



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1hrs__1992__i339573/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
