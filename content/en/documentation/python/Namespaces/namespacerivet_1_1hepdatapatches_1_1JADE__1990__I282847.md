---

title: 'namespace rivet::hepdatapatches::JADE_1990_I282847'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::JADE_1990_I282847



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1jade__1990__i282847/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
