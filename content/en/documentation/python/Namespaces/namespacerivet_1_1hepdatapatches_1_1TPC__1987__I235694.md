---

title: 'namespace rivet::hepdatapatches::TPC_1987_I235694'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::TPC_1987_I235694



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1tpc__1987__i235694/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
