---

title: 'namespace rivet::hepdatapatches::TASSO_1989_I266893'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::TASSO_1989_I266893



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1tasso__1989__i266893/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
