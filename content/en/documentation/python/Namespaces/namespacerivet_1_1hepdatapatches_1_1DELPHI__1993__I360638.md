---

title: 'namespace rivet::hepdatapatches::DELPHI_1993_I360638'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::DELPHI_1993_I360638



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1delphi__1993__i360638/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
