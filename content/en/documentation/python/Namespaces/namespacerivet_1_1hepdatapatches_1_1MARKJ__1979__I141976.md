---

title: 'namespace rivet::hepdatapatches::MARKJ_1979_I141976'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::MARKJ_1979_I141976



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1markj__1979__i141976/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
