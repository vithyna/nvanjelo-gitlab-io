---

title: 'namespace rivet::hepdatapatches::LENA_1982_I179431'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::LENA_1982_I179431



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1lena__1982__i179431/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
