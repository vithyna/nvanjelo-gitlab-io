---

title: 'namespace rivet::hepdatapatches::TOPAZ_1993_I361661'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::TOPAZ_1993_I361661



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1topaz__1993__i361661/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
