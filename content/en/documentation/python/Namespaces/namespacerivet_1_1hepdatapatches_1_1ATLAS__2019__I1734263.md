---

title: 'namespace rivet::hepdatapatches::ATLAS_2019_I1734263'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::ATLAS_2019_I1734263



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1atlas__2019__i1734263/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
