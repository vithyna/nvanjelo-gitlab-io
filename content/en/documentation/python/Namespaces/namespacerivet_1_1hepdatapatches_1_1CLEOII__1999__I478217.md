---

title: 'namespace rivet::hepdatapatches::CLEOII_1999_I478217'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::CLEOII_1999_I478217



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1cleoii__1999__i478217/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
