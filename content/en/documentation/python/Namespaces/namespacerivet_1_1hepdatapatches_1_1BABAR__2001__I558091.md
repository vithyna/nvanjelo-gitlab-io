---

title: 'namespace rivet::hepdatapatches::BABAR_2001_I558091'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::BABAR_2001_I558091



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1babar__2001__i558091/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
