---

title: 'namespace rivet::hepdatapatches::CRYSTAL_BALL_1988_I261078'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::CRYSTAL_BALL_1988_I261078



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1crystal__ball__1988__i261078/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
