---

title: 'namespace rivet::hepdatapatches::DM2_1990_I297706'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::DM2_1990_I297706



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1dm2__1990__i297706/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
