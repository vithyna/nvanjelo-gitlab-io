---

title: 'namespace rivet::hepdatapatches::ATLAS_2018_I1635273'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::ATLAS_2018_I1635273



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1atlas__2018__i1635273/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
