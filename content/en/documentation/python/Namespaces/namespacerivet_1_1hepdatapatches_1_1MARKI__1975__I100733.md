---

title: 'namespace rivet::hepdatapatches::MARKI_1975_I100733'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::MARKI_1975_I100733



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1marki__1975__i100733/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
