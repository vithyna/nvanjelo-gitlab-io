---

title: 'namespace rivet::hepdatapatches::BESII_2004_I622224'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::BESII_2004_I622224



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1besii__2004__i622224/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
