---

title: 'namespace rivet::hepdatapatches::NMD_1974_I745'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::NMD_1974_I745



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1nmd__1974__i745/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
