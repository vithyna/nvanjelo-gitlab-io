---

title: 'namespace rivet::hepdatapatches::ALEPH_1991_S2435284'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::ALEPH_1991_S2435284



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1aleph__1991__s2435284/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
