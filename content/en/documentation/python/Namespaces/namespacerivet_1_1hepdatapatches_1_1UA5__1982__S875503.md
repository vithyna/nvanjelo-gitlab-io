---

title: 'namespace rivet::hepdatapatches::UA5_1982_S875503'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::UA5_1982_S875503



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1ua5__1982__s875503/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
