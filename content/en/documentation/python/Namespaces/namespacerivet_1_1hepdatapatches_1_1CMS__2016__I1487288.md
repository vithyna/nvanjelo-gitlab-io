---

title: 'namespace rivet::hepdatapatches::CMS_2016_I1487288'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::CMS_2016_I1487288



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1cms__2016__i1487288/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
