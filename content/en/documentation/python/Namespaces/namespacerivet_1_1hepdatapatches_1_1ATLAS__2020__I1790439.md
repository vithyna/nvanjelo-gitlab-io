---

title: 'namespace rivet::hepdatapatches::ATLAS_2020_I1790439'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::ATLAS_2020_I1790439



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1atlas__2020__i1790439/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
