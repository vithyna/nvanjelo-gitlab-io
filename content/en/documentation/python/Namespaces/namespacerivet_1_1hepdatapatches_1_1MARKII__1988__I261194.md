---

title: 'namespace rivet::hepdatapatches::MARKII_1988_I261194'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::MARKII_1988_I261194



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1markii__1988__i261194/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
