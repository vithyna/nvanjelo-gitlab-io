---

title: 'namespace rivet::hepdatapatches::NA22_1986_I18431'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::NA22_1986_I18431



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1na22__1986__i18431/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
