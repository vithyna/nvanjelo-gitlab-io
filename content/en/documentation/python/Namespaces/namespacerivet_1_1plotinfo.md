---

title: 'namespace rivet::plotinfo'

description: "[Documentation update required.]"

---

# rivet::plotinfo



## Classes

|                | Name           |
| -------------- | -------------- |
| class | **[rivet::plotinfo::PlotParser](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/)**  |

## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[mkStdPlotParser](/documentation/python/namespaces/namespacerivet_1_1plotinfo/#function-mkstdplotparser)**(dirs dirs =None, addfiles addfiles =[]) |


## Functions Documentation

### function mkStdPlotParser

```python
def mkStdPlotParser(
    dirs dirs =None,
    addfiles addfiles =[]
)
```




```
Make a PlotParser with the standard Rivet .plot locations automatically added to
the manually set plot info dirs and additional files.
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
