---

title: 'namespace rivet::hepdatapatches::HRS_1989_I276948'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::HRS_1989_I276948



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1hrs__1989__i276948/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
