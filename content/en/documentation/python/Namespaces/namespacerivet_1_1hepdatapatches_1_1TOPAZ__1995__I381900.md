---

title: 'namespace rivet::hepdatapatches::TOPAZ_1995_I381900'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::TOPAZ_1995_I381900



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1topaz__1995__i381900/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
