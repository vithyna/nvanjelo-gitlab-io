---

title: 'namespace rivet::hepdatapatches::JADE_1985_I213948'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::JADE_1985_I213948



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1jade__1985__i213948/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
