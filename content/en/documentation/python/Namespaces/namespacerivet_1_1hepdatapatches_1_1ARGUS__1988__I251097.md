---

title: 'namespace rivet::hepdatapatches::ARGUS_1988_I251097'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::ARGUS_1988_I251097



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1argus__1988__i251097/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
