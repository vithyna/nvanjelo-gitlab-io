---

title: 'namespace rivet::hepdatapatches::ARGUS_1994_I354224'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::ARGUS_1994_I354224



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1argus__1994__i354224/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
