---

title: 'namespace rivet::hepdatapatches::JADE_1984_I202785'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::JADE_1984_I202785



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1jade__1984__i202785/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
