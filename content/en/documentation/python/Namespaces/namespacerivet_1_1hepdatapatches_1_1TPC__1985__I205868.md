---

title: 'namespace rivet::hepdatapatches::TPC_1985_I205868'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::TPC_1985_I205868



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1tpc__1985__i205868/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
