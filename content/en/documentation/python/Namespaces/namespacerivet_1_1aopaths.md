---

title: 'namespace rivet::aopaths'

description: "[Documentation update required.]"

---

# rivet::aopaths



## Classes

|                | Name           |
| -------------- | -------------- |
| class | **[rivet::aopaths::AOPath](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/)**  |

## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[isRefPath](/documentation/python/namespaces/namespacerivet_1_1aopaths/#function-isrefpath)**(path path) |
| def | **[isTheoryPath](/documentation/python/namespaces/namespacerivet_1_1aopaths/#function-istheorypath)**(path path) |
| def | **[isRawPath](/documentation/python/namespaces/namespacerivet_1_1aopaths/#function-israwpath)**(path path) |
| def | **[isRawAO](/documentation/python/namespaces/namespacerivet_1_1aopaths/#function-israwao)**(ao ao) |
| def | **[isTheoryAO](/documentation/python/namespaces/namespacerivet_1_1aopaths/#function-istheoryao)**(ao ao) |
| def | **[stripOptions](/documentation/python/namespaces/namespacerivet_1_1aopaths/#function-stripoptions)**(path path) |
| def | **[stripWeightName](/documentation/python/namespaces/namespacerivet_1_1aopaths/#function-stripweightname)**(path path) |
| def | **[extractWeightName](/documentation/python/namespaces/namespacerivet_1_1aopaths/#function-extractweightname)**(path path) |
| def | **[extractOptionString](/documentation/python/namespaces/namespacerivet_1_1aopaths/#function-extractoptionstring)**(path path) |
| def | **[isRefAO](/documentation/python/namespaces/namespacerivet_1_1aopaths/#function-isrefao)**(ao ao) |
| def | **[isTmpPath](/documentation/python/namespaces/namespacerivet_1_1aopaths/#function-istmppath)**(path path) |
| def | **[isTmpAO](/documentation/python/namespaces/namespacerivet_1_1aopaths/#function-istmpao)**(ao ao) |


## Functions Documentation

### function isRefPath

```python
def isRefPath(
    path path
)
```


### function isTheoryPath

```python
def isTheoryPath(
    path path
)
```


### function isRawPath

```python
def isRawPath(
    path path
)
```


### function isRawAO

```python
def isRawAO(
    ao ao
)
```


### function isTheoryAO

```python
def isTheoryAO(
    ao ao
)
```


### function stripOptions

```python
def stripOptions(
    path path
)
```


### function stripWeightName

```python
def stripWeightName(
    path path
)
```


### function extractWeightName

```python
def extractWeightName(
    path path
)
```


### function extractOptionString

```python
def extractOptionString(
    path path
)
```


### function isRefAO

```python
def isRefAO(
    ao ao
)
```


### function isTmpPath

```python
def isTmpPath(
    path path
)
```


### function isTmpAO

```python
def isTmpAO(
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
