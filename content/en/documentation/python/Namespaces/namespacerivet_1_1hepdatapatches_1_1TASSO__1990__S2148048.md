---

title: 'namespace rivet::hepdatapatches::TASSO_1990_S2148048'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::TASSO_1990_S2148048



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1tasso__1990__s2148048/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
