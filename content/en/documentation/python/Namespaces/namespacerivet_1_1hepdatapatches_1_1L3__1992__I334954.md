---

title: 'namespace rivet::hepdatapatches::L3_1992_I334954'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::L3_1992_I334954



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1l3__1992__i334954/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
