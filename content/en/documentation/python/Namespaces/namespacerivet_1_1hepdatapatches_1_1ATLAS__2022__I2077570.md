---

title: 'namespace rivet::hepdatapatches::ATLAS_2022_I2077570'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::ATLAS_2022_I2077570



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1atlas__2022__i2077570/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
