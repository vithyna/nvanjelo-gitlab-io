---

title: 'namespace rivet::hepdatapatches::MAC_1985_I202924'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::MAC_1985_I202924



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1mac__1985__i202924/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
