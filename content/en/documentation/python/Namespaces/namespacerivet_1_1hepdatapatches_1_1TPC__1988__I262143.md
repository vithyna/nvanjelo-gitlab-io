---

title: 'namespace rivet::hepdatapatches::TPC_1988_I262143'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::TPC_1988_I262143



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1tpc__1988__i262143/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
