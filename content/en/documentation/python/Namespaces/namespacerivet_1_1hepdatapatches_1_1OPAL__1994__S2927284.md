---

title: 'namespace rivet::hepdatapatches::OPAL_1994_S2927284'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::OPAL_1994_S2927284



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1opal__1994__s2927284/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
