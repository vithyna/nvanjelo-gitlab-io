---

title: 'namespace rivet::hepdatapatches::CLEO_1985_I205668'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::CLEO_1985_I205668



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1cleo__1985__i205668/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
