---

title: 'namespace rivet::hepdatapatches::BABAR_2005_S6181155'

description: "[Documentation update required.]"

---

# rivet::hepdatapatches::BABAR_2005_S6181155



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[patch](/documentation/python/namespaces/namespacerivet_1_1hepdatapatches_1_1babar__2005__s6181155/#function-patch)**(path path, ao ao) |


## Functions Documentation

### function patch

```python
def patch(
    path path,
    ao ao
)
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
