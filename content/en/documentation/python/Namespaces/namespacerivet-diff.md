---

title: 'namespace rivet-diff'

description: "[Documentation update required.]"

---

# rivet-diff



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[printINFO](/documentation/python/namespaces/namespacerivet-diff/#function-printinfo)**(msg msg) |
| def | **[printERROR](/documentation/python/namespaces/namespacerivet-diff/#function-printerror)**(msg msg) |
| def | **[allRivetAnalyses](/documentation/python/namespaces/namespacerivet-diff/#function-allrivetanalyses)**(env env) |
| def | **[getRivetEnv](/documentation/python/namespaces/namespacerivet-diff/#function-getrivetenv)**(rivetPath rivetPath) |
| def | **[checkAnalysis](/documentation/python/namespaces/namespacerivet-diff/#function-checkanalysis)**(oldRivetEnv oldRivetEnv, newRivetEnv newRivetEnv, analysisName analysisName, hepmcgzFile hepmcgzFile) |
| def | **[main](/documentation/python/namespaces/namespacerivet-diff/#function-main)**() |


## Functions Documentation

### function printINFO

```python
def printINFO(
    msg msg
)
```


### function printERROR

```python
def printERROR(
    msg msg
)
```


### function allRivetAnalyses

```python
def allRivetAnalyses(
    env env
)
```


### function getRivetEnv

```python
def getRivetEnv(
    rivetPath rivetPath
)
```


### function checkAnalysis

```python
def checkAnalysis(
    oldRivetEnv oldRivetEnv,
    newRivetEnv newRivetEnv,
    analysisName analysisName,
    hepmcgzFile hepmcgzFile
)
```


### function main

```python
def main()
```






-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
