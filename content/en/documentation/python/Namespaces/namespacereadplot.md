---

title: 'namespace readplot'

description: "[Documentation update required.]"

---

# readplot



## Functions

|                | Name           |
| -------------- | -------------- |
| def | **[sorted](/documentation/python/namespaces/namespacereadplot/#function-sorted)**(iterable iterable, cmp cmp =None, key key =None, reverse reverse =None)<br>Make "sorted" a builtin function on Python < 2.4.  |
| def | **[plotinfo](/documentation/python/namespaces/namespacereadplot/#function-plotinfo)**(aname aname)<br>Get list of plots for each analysis.  |

## Attributes

|                | Name           |
| -------------- | -------------- |
| | **[tmp](/documentation/python/namespaces/namespacereadplot/#variable-tmp)** <br>TODO: Why don't these tests work within 'make'?  |
| | **[pybuild](/documentation/python/namespaces/namespacereadplot/#variable-pybuild)**  |
| list | **[dirs](/documentation/python/namespaces/namespacereadplot/#variable-dirs)**  |
| | **[path](/documentation/python/namespaces/namespacereadplot/#variable-path)**  |
| | **[anadirs](/documentation/python/namespaces/namespacereadplot/#variable-anadirs)**  |


## Functions Documentation

### function sorted

```python
def sorted(
    iterable iterable,
    cmp cmp =None,
    key key =None,
    reverse reverse =None
)
```

Make "sorted" a builtin function on Python < 2.4. 

### function plotinfo

```python
def plotinfo(
    aname aname
)
```

Get list of plots for each analysis. 


## Attributes Documentation

### variable tmp

```python
tmp =  set();
```

TODO: Why don't these tests work within 'make'? 

Make "set" a builtin type on Python < 2.4 


### variable pybuild

```python
pybuild =  os.path.abspath(os.path.join(os.getcwd(), "..", "pyext", "build"));
```


### variable dirs

```python
list dirs =  [];
```


### variable path

```python
path;
```


### variable anadirs

```python
anadirs =  glob.glob(os.path.join(os.getcwd(), "..", "src", "Analyses", "*", ".libs"));
```





-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
