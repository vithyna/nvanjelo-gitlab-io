---

title: 'class simplehtml::HTML'

description: "[Documentation update required.]"

---

# simplehtml::HTML



 [More...](#detailed-description)

Inherits from object

Inherited by [simplehtml.XHTML](/documentation/python/classes/classsimplehtml_1_1xhtml/)

## Public Functions

|                | Name           |
| -------------- | -------------- |
| def | **[text](/documentation/python/classes/classsimplehtml_1_1html/#function-text)**(self self, text text, escape escape =True) |
| def | **[raw_text](/documentation/python/classes/classsimplehtml_1_1html/#function-raw-text)**(self self, <a href="/documentation/python/classes/classsimplehtml_1_1html/#function-text">text</a> text) |

## Public Attributes

|                | Name           |
| -------------- | -------------- |
| | **[newline_default_on](/documentation/python/classes/classsimplehtml_1_1html/#variable-newline-default-on)**  |

## Detailed Description

```python
class simplehtml::HTML;
```




```
Easily generate HTML.

>>> print HTML('html', 'some text')
<html>some text</html>
>>> print HTML('html').p('some text')
<html><p>some text</p></html>

If a name is not passed in then the instance becomes a container for
other tags that itself generates no tag:

>>> h = HTML()
>>> h.p('text')
>>> h.p('text')
print h
<p>some text</p>
<p>some text</p>```

## Public Functions Documentation

### function text

```python
def text(
    self self,
    text text,
    escape escape =True
)
```




```
Add text to the document. If "escape" is True any characters
special to HTML will be escaped.
```


### function raw_text

```python
def raw_text(
    self self,
    text text
)
```




```
Add raw, unescaped text to the document. This is useful for
explicitly adding HTML code or entities.
```


## Public Attributes Documentation

### variable newline_default_on

```python
static newline_default_on =  set('table ol ul dl'.split());
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
