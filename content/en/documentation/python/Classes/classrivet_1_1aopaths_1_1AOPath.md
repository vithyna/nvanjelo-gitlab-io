---

title: 'class rivet::aopaths::AOPath'

description: "[Documentation update required.]"

---

# rivet::aopaths::AOPath



 [More...](#detailed-description)

Inherits from object, object, object

## Public Functions

|                | Name           |
| -------------- | -------------- |
| def | **[basepath](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/#function-basepath)**(self self, keepref keepref =False) |
| def | **[varpath](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/#function-varpath)**(self self, keepref keepref =False, defaultvarid defaultvarid =None) |
| def | **[binpath](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/#function-binpath)**(self self, keepref keepref =False, defaultbinid defaultbinid =None, defaultvarid defaultvarid =None) |
| def | **[basepathparts](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/#function-basepathparts)**(self self, keepref keepref =False) |
| def | **[dirname](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/#function-dirname)**(self self, keepref keepref =False) |
| def | **[dirnameparts](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/#function-dirnameparts)**(self self, keepref keepref =False) |
| def | **[basename](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/#function-basename)**(self self) |
| def | **[varid](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/#function-varid)**(self self, default default =None) |
| def | **[binid](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/#function-binid)**(self self, default default =None) |
| def | **[isref](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/#function-isref)**(self self) |
| def | **[istmp](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/#function-istmp)**(self self) |
| def | **[israw](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/#function-israw)**(self self) |
| def | **[basepath](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/#function-basepath)**(self self, keepref keepref =False) |
| def | **[varpath](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/#function-varpath)**(self self, keepref keepref =False, defaultvarid defaultvarid =None) |
| def | **[binpath](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/#function-binpath)**(self self, keepref keepref =False, defaultbinid defaultbinid =None, defaultvarid defaultvarid =None) |
| def | **[basepathparts](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/#function-basepathparts)**(self self, keepref keepref =False) |
| def | **[dirname](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/#function-dirname)**(self self, keepref keepref =False) |
| def | **[dirnameparts](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/#function-dirnameparts)**(self self, keepref keepref =False) |
| def | **[basename](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/#function-basename)**(self self) |
| def | **[varid](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/#function-varid)**(self self, default default =None) |
| def | **[binid](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/#function-binid)**(self self, default default =None) |
| def | **[isref](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/#function-isref)**(self self) |
| def | **[istmp](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/#function-istmp)**(self self) |
| def | **[israw](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/#function-israw)**(self self) |
| def | **[basepath](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/#function-basepath)**(self self, keepref keepref =False) |
| def | **[varpath](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/#function-varpath)**(self self, keepref keepref =False, defaultvarid defaultvarid =None) |
| def | **[binpath](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/#function-binpath)**(self self, keepref keepref =False, defaultbinid defaultbinid =None, defaultvarid defaultvarid =None) |
| def | **[basepathparts](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/#function-basepathparts)**(self self, keepref keepref =False) |
| def | **[dirname](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/#function-dirname)**(self self, keepref keepref =False) |
| def | **[dirnameparts](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/#function-dirnameparts)**(self self, keepref keepref =False) |
| def | **[basename](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/#function-basename)**(self self) |
| def | **[varid](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/#function-varid)**(self self, default default =None) |
| def | **[binid](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/#function-binid)**(self self, default default =None) |
| def | **[isref](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/#function-isref)**(self self) |
| def | **[istmp](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/#function-istmp)**(self self) |
| def | **[israw](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/#function-israw)**(self self) |

## Public Attributes

|                | Name           |
| -------------- | -------------- |
| | **[re_aopath](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/#variable-re-aopath)**  |
| | **[origpath](/documentation/python/classes/classrivet_1_1aopaths_1_1aopath/#variable-origpath)**  |

## Detailed Description

```python
class rivet::aopaths::AOPath;
```




```
Object representation of analysis object path structures.

TODO: move to YODA?
```

## Public Functions Documentation

### function basepath

```python
def basepath(
    self self,
    keepref keepref =False
)
```


### function varpath

```python
def varpath(
    self self,
    keepref keepref =False,
    defaultvarid defaultvarid =None
)
```


### function binpath

```python
def binpath(
    self self,
    keepref keepref =False,
    defaultbinid defaultbinid =None,
    defaultvarid defaultvarid =None
)
```


### function basepathparts

```python
def basepathparts(
    self self,
    keepref keepref =False
)
```


### function dirname

```python
def dirname(
    self self,
    keepref keepref =False
)
```


### function dirnameparts

```python
def dirnameparts(
    self self,
    keepref keepref =False
)
```


### function basename

```python
def basename(
    self self
)
```


### function varid

```python
def varid(
    self self,
    default default =None
)
```


### function binid

```python
def binid(
    self self,
    default default =None
)
```


### function isref

```python
def isref(
    self self
)
```


### function istmp

```python
def istmp(
    self self
)
```


### function israw

```python
def israw(
    self self
)
```


### function basepath

```python
def basepath(
    self self,
    keepref keepref =False
)
```


### function varpath

```python
def varpath(
    self self,
    keepref keepref =False,
    defaultvarid defaultvarid =None
)
```


### function binpath

```python
def binpath(
    self self,
    keepref keepref =False,
    defaultbinid defaultbinid =None,
    defaultvarid defaultvarid =None
)
```


### function basepathparts

```python
def basepathparts(
    self self,
    keepref keepref =False
)
```


### function dirname

```python
def dirname(
    self self,
    keepref keepref =False
)
```


### function dirnameparts

```python
def dirnameparts(
    self self,
    keepref keepref =False
)
```


### function basename

```python
def basename(
    self self
)
```


### function varid

```python
def varid(
    self self,
    default default =None
)
```


### function binid

```python
def binid(
    self self,
    default default =None
)
```


### function isref

```python
def isref(
    self self
)
```


### function istmp

```python
def istmp(
    self self
)
```


### function israw

```python
def israw(
    self self
)
```


### function basepath

```python
def basepath(
    self self,
    keepref keepref =False
)
```


### function varpath

```python
def varpath(
    self self,
    keepref keepref =False,
    defaultvarid defaultvarid =None
)
```


### function binpath

```python
def binpath(
    self self,
    keepref keepref =False,
    defaultbinid defaultbinid =None,
    defaultvarid defaultvarid =None
)
```


### function basepathparts

```python
def basepathparts(
    self self,
    keepref keepref =False
)
```


### function dirname

```python
def dirname(
    self self,
    keepref keepref =False
)
```


### function dirnameparts

```python
def dirnameparts(
    self self,
    keepref keepref =False
)
```


### function basename

```python
def basename(
    self self
)
```


### function varid

```python
def varid(
    self self,
    default default =None
)
```


### function binid

```python
def binid(
    self self,
    default default =None
)
```


### function isref

```python
def isref(
    self self
)
```


### function istmp

```python
def istmp(
    self self
)
```


### function israw

```python
def israw(
    self self
)
```


## Public Attributes Documentation

### variable re_aopath

```python
static re_aopath =  re.compile(r"^(/[^\[\]\@\#]+)(\[[A-Za-z\d\._=\s+-]+\])?(#\d+|@[\d\.]+)?$");
```


### variable origpath

```python
origpath;
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
