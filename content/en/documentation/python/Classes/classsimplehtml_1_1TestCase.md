---

title: 'class simplehtml::TestCase'

description: "[Documentation update required.]"

---

# simplehtml::TestCase





Inherits from unittest.TestCase

## Public Functions

|                | Name           |
| -------------- | -------------- |
| def | **[test_empty_tag](/documentation/python/classes/classsimplehtml_1_1testcase/#function-test-empty-tag)**(self self) |
| def | **[test_empty_tag_xml](/documentation/python/classes/classsimplehtml_1_1testcase/#function-test-empty-tag-xml)**(self self) |
| def | **[test_tag_add](/documentation/python/classes/classsimplehtml_1_1testcase/#function-test-tag-add)**(self self) |
| def | **[test_tag_add_no_newline](/documentation/python/classes/classsimplehtml_1_1testcase/#function-test-tag-add-no-newline)**(self self) |
| def | **[test_iadd_tag](/documentation/python/classes/classsimplehtml_1_1testcase/#function-test-iadd-tag)**(self self) |
| def | **[test_iadd_text](/documentation/python/classes/classsimplehtml_1_1testcase/#function-test-iadd-text)**(self self) |
| def | **[test_xhtml_match_tag](/documentation/python/classes/classsimplehtml_1_1testcase/#function-test-xhtml-match-tag)**(self self) |
| def | **[test_empty_tag_unicode](/documentation/python/classes/classsimplehtml_1_1testcase/#function-test-empty-tag-unicode)**(self self) |
| def | **[test_empty_tag_xml_unicode](/documentation/python/classes/classsimplehtml_1_1testcase/#function-test-empty-tag-xml-unicode)**(self self) |
| def | **[test_xhtml_match_tag_unicode](/documentation/python/classes/classsimplehtml_1_1testcase/#function-test-xhtml-match-tag-unicode)**(self self) |
| def | **[test_just_tag](/documentation/python/classes/classsimplehtml_1_1testcase/#function-test-just-tag)**(self self) |
| def | **[test_just_tag_xhtml](/documentation/python/classes/classsimplehtml_1_1testcase/#function-test-just-tag-xhtml)**(self self) |
| def | **[test_xml](/documentation/python/classes/classsimplehtml_1_1testcase/#function-test-xml)**(self self) |
| def | **[test_para_tag](/documentation/python/classes/classsimplehtml_1_1testcase/#function-test-para-tag)**(self self) |
| def | **[test_escape](/documentation/python/classes/classsimplehtml_1_1testcase/#function-test-escape)**(self self) |
| def | **[test_no_escape](/documentation/python/classes/classsimplehtml_1_1testcase/#function-test-no-escape)**(self self) |
| def | **[test_escape_attr](/documentation/python/classes/classsimplehtml_1_1testcase/#function-test-escape-attr)**(self self) |
| def | **[test_subtag_context](/documentation/python/classes/classsimplehtml_1_1testcase/#function-test-subtag-context)**(self self) |
| def | **[test_subtag_direct](/documentation/python/classes/classsimplehtml_1_1testcase/#function-test-subtag-direct)**(self self) |
| def | **[test_subtag_direct_context](/documentation/python/classes/classsimplehtml_1_1testcase/#function-test-subtag-direct-context)**(self self) |
| def | **[test_subtag_no_newlines](/documentation/python/classes/classsimplehtml_1_1testcase/#function-test-subtag-no-newlines)**(self self) |
| def | **[test_add_text](/documentation/python/classes/classsimplehtml_1_1testcase/#function-test-add-text)**(self self) |
| def | **[test_add_text_newlines](/documentation/python/classes/classsimplehtml_1_1testcase/#function-test-add-text-newlines)**(self self) |
| def | **[test_doc_newlines](/documentation/python/classes/classsimplehtml_1_1testcase/#function-test-doc-newlines)**(self self) |
| def | **[test_doc_no_newlines](/documentation/python/classes/classsimplehtml_1_1testcase/#function-test-doc-no-newlines)**(self self) |
| def | **[test_unicode](/documentation/python/classes/classsimplehtml_1_1testcase/#function-test-unicode)**(self self) |
| def | **[test_table](/documentation/python/classes/classsimplehtml_1_1testcase/#function-test-table)**(self self) |

## Public Functions Documentation

### function test_empty_tag

```python
def test_empty_tag(
    self self
)
```


### function test_empty_tag_xml

```python
def test_empty_tag_xml(
    self self
)
```


### function test_tag_add

```python
def test_tag_add(
    self self
)
```


### function test_tag_add_no_newline

```python
def test_tag_add_no_newline(
    self self
)
```


### function test_iadd_tag

```python
def test_iadd_tag(
    self self
)
```


### function test_iadd_text

```python
def test_iadd_text(
    self self
)
```


### function test_xhtml_match_tag

```python
def test_xhtml_match_tag(
    self self
)
```


### function test_empty_tag_unicode

```python
def test_empty_tag_unicode(
    self self
)
```


### function test_empty_tag_xml_unicode

```python
def test_empty_tag_xml_unicode(
    self self
)
```


### function test_xhtml_match_tag_unicode

```python
def test_xhtml_match_tag_unicode(
    self self
)
```


### function test_just_tag

```python
def test_just_tag(
    self self
)
```


### function test_just_tag_xhtml

```python
def test_just_tag_xhtml(
    self self
)
```


### function test_xml

```python
def test_xml(
    self self
)
```


### function test_para_tag

```python
def test_para_tag(
    self self
)
```


### function test_escape

```python
def test_escape(
    self self
)
```


### function test_no_escape

```python
def test_no_escape(
    self self
)
```


### function test_escape_attr

```python
def test_escape_attr(
    self self
)
```


### function test_subtag_context

```python
def test_subtag_context(
    self self
)
```


### function test_subtag_direct

```python
def test_subtag_direct(
    self self
)
```


### function test_subtag_direct_context

```python
def test_subtag_direct_context(
    self self
)
```


### function test_subtag_no_newlines

```python
def test_subtag_no_newlines(
    self self
)
```


### function test_add_text

```python
def test_add_text(
    self self
)
```


### function test_add_text_newlines

```python
def test_add_text_newlines(
    self self
)
```


### function test_doc_newlines

```python
def test_doc_newlines(
    self self
)
```


### function test_doc_no_newlines

```python
def test_doc_no_newlines(
    self self
)
```


### function test_unicode

```python
def test_unicode(
    self self
)
```


### function test_table

```python
def test_table(
    self self
)
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
