---

title: 'class simplehtml::XML'

description: "[Documentation update required.]"

---

# simplehtml::XML



 [More...](#detailed-description)

Inherits from [simplehtml.XHTML](/documentation/python/classes/classsimplehtml_1_1xhtml/), [simplehtml.HTML](/documentation/python/classes/classsimplehtml_1_1html/), object

## Public Attributes

|                | Name           |
| -------------- | -------------- |
| | **[newline_default_on](/documentation/python/classes/classsimplehtml_1_1xml/#variable-newline-default-on)**  |

## Additional inherited members

**Public Attributes inherited from [simplehtml.XHTML](/documentation/python/classes/classsimplehtml_1_1xhtml/)**

|                | Name           |
| -------------- | -------------- |
| | **[empty_elements](/documentation/python/classes/classsimplehtml_1_1xhtml/#variable-empty-elements)**  |

**Public Functions inherited from [simplehtml.HTML](/documentation/python/classes/classsimplehtml_1_1html/)**

|                | Name           |
| -------------- | -------------- |
| def | **[text](/documentation/python/classes/classsimplehtml_1_1html/#function-text)**(self self, text text, escape escape =True) |
| def | **[raw_text](/documentation/python/classes/classsimplehtml_1_1html/#function-raw-text)**(self self, <a href="/documentation/python/classes/classsimplehtml_1_1html/#function-text">text</a> text) |


## Detailed Description

```python
class simplehtml::XML;
```




```
Easily generate XML.

All tags with no contents are reduced to self-terminating tags.
```

## Public Attributes Documentation

### variable newline_default_on

```python
static newline_default_on =  set();
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
