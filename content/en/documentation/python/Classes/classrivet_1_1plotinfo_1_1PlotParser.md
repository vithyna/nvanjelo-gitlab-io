---

title: 'class rivet::plotinfo::PlotParser'

description: "[Documentation update required.]"

---

# rivet::plotinfo::PlotParser



 [More...](#detailed-description)

Inherits from object, object, object

## Public Functions

|                | Name           |
| -------------- | -------------- |
| def | **[getSection](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/#function-getsection)**(self self, section section, hpath hpath) |
| def | **[getSections](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/#function-getsections)**(self self, sections sections, hpath hpath) |
| def | **[getHeaders](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/#function-getheaders)**(self self, hpath hpath) |
| def | **[getSpecial](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/#function-getspecial)**(self self, hpath hpath) |
| def | **[getSpecials](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/#function-getspecials)**(self self, hpath hpath) |
| def | **[getHistogramOptions](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/#function-gethistogramoptions)**(self self, hpath hpath) |
| def | **[isEndMarker](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/#function-isendmarker)**(self self, line line, blockname blockname) |
| def | **[isComment](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/#function-iscomment)**(self self, line line) |
| def | **[updateHistoHeaders](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/#function-updatehistoheaders)**(self self, hist hist) |
| def | **[getSection](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/#function-getsection)**(self self, section section, hpath hpath) |
| def | **[getSections](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/#function-getsections)**(self self, sections sections, hpath hpath) |
| def | **[getHeaders](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/#function-getheaders)**(self self, hpath hpath) |
| def | **[getSpecial](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/#function-getspecial)**(self self, hpath hpath) |
| def | **[getSpecials](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/#function-getspecials)**(self self, hpath hpath) |
| def | **[getHistogramOptions](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/#function-gethistogramoptions)**(self self, hpath hpath) |
| def | **[isEndMarker](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/#function-isendmarker)**(self self, line line, blockname blockname) |
| def | **[isComment](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/#function-iscomment)**(self self, line line) |
| def | **[updateHistoHeaders](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/#function-updatehistoheaders)**(self self, hist hist) |
| def | **[getSection](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/#function-getsection)**(self self, section section, hpath hpath) |
| def | **[getSections](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/#function-getsections)**(self self, sections sections, hpath hpath) |
| def | **[getHeaders](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/#function-getheaders)**(self self, hpath hpath) |
| def | **[getSpecial](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/#function-getspecial)**(self self, hpath hpath) |
| def | **[getSpecials](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/#function-getspecials)**(self self, hpath hpath) |
| def | **[getHistogramOptions](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/#function-gethistogramoptions)**(self self, hpath hpath) |
| def | **[isEndMarker](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/#function-isendmarker)**(self self, line line, blockname blockname) |
| def | **[isComment](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/#function-iscomment)**(self self, line line) |
| def | **[updateHistoHeaders](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/#function-updatehistoheaders)**(self self, hist hist) |

## Public Attributes

|                | Name           |
| -------------- | -------------- |
| | **[pat_begin_block](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/#variable-pat-begin-block)**  |
| | **[pat_begin_name_block](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/#variable-pat-begin-name-block)**  |
| | **[pat_end_block](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/#variable-pat-end-block)**  |
| | **[pat_comment](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/#variable-pat-comment)**  |
| | **[pat_property](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/#variable-pat-property)**  |
| | **[pat_property_opt](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/#variable-pat-property-opt)**  |
| | **[pat_path_property](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/#variable-pat-path-property)**  |
| dictionary | **[pat_paths](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/#variable-pat-paths)**  |
| def | **[getPlot](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/#variable-getplot)** <br>Alias.  |
| | **[addfiles](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/#variable-addfiles)**  |
| | **[plotpaths](/documentation/python/classes/classrivet_1_1plotinfo_1_1plotparser/#variable-plotpaths)**  |

## Detailed Description

```python
class rivet::plotinfo::PlotParser;
```




```
Reads Rivet's .plot files and determines which attributes to apply to each histo path.
```

## Public Functions Documentation

### function getSection

```python
def getSection(
    self self,
    section section,
    hpath hpath
)
```




```
Get a section for a histogram from a .plot file.

Parameters
----------
section : ('PLOT'|'SPECIAL'|'HISTOGRAM')
    The section that should be extracted.
hpath : str
    The histogram path, i.e. /AnalysisID/HistogramID .

TODO:
 * Caching! The result of the lookup is not cached so every call requires a file to be searched for and opened.
```


### function getSections

```python
def getSections(
    self self,
    sections sections,
    hpath hpath
)
```




```
Get all sections for a histogram from a .plot file.

Parameters
----------
section : ('SPECIAL')
    The section that should be extracted. Only Specials allowed to occur multiple times.
hpath : str
    The histogram path, i.e. /AnalysisID/HistogramID .

TODO:
 * Caching! The result of the lookup is not cached so every call requires a file to be searched for and opened.
```


### function getHeaders

```python
def getHeaders(
    self self,
    hpath hpath
)
```




```
Get the plot headers for histogram hpath.

This returns the PLOT section.

Parameters
----------
hpath : str
    The histogram path, i.e. /AnalysisID/HistogramID .

Returns
-------
plot_section : dict
    The dictionary usually contains the 'Title', 'XLabel' and
    'YLabel' properties of the respective plot.

See also
--------
:meth:`getSection`
```


### function getSpecial

```python
def getSpecial(
    self self,
    hpath hpath
)
```




```
Get a SPECIAL section for histogram hpath.

The SPECIAL section is only available in a few analyses.

Parameters
----------
hpath : str
    Histogram path. Must have the form /AnalysisID/HistogramID .

See also
--------
:meth:`getSection`
```


### function getSpecials

```python
def getSpecials(
    self self,
    hpath hpath
)
```




```
Get all SPECIAL sections for histogram hpath.

The SPECIAL section is only available in a few analyses.

Parameters
----------
hpath : str
    Histogram path. Must have the form /AnalysisID/HistogramID .

See also
--------
:meth:`getSections`
```


### function getHistogramOptions

```python
def getHistogramOptions(
    self self,
    hpath hpath
)
```




```
Get a HISTOGRAM section for histogram hpath.

The HISTOGRAM section is only available in a few analyses.

Parameters
----------
hpath : str
    Histogram path. Must have the form /AnalysisID/HistogramID .

See also
--------
:meth:`getSection`
```


### function isEndMarker

```python
def isEndMarker(
    self self,
    line line,
    blockname blockname
)
```


### function isComment

```python
def isComment(
    self self,
    line line
)
```


### function updateHistoHeaders

```python
def updateHistoHeaders(
    self self,
    hist hist
)
```


### function getSection

```python
def getSection(
    self self,
    section section,
    hpath hpath
)
```




```
Get a section for a histogram from a .plot file.

Parameters
----------
section : ('PLOT'|'SPECIAL'|'HISTOGRAM')
    The section that should be extracted.
hpath : str
    The histogram path, i.e. /AnalysisID/HistogramID .

TODO:
 * Caching! The result of the lookup is not cached so every call requires a file to be searched for and opened.
```


### function getSections

```python
def getSections(
    self self,
    sections sections,
    hpath hpath
)
```




```
Get all sections for a histogram from a .plot file.

Parameters
----------
section : ('SPECIAL')
    The section that should be extracted. Only Specials allowed to occur multiple times.
hpath : str
    The histogram path, i.e. /AnalysisID/HistogramID .

TODO:
 * Caching! The result of the lookup is not cached so every call requires a file to be searched for and opened.
```


### function getHeaders

```python
def getHeaders(
    self self,
    hpath hpath
)
```




```
Get the plot headers for histogram hpath.

This returns the PLOT section.

Parameters
----------
hpath : str
    The histogram path, i.e. /AnalysisID/HistogramID .

Returns
-------
plot_section : dict
    The dictionary usually contains the 'Title', 'XLabel' and
    'YLabel' properties of the respective plot.

See also
--------
:meth:`getSection`
```


### function getSpecial

```python
def getSpecial(
    self self,
    hpath hpath
)
```




```
Get a SPECIAL section for histogram hpath.

The SPECIAL section is only available in a few analyses.

Parameters
----------
hpath : str
    Histogram path. Must have the form /AnalysisID/HistogramID .

See also
--------
:meth:`getSection`
```


### function getSpecials

```python
def getSpecials(
    self self,
    hpath hpath
)
```




```
Get all SPECIAL sections for histogram hpath.

The SPECIAL section is only available in a few analyses.

Parameters
----------
hpath : str
    Histogram path. Must have the form /AnalysisID/HistogramID .

See also
--------
:meth:`getSections`
```


### function getHistogramOptions

```python
def getHistogramOptions(
    self self,
    hpath hpath
)
```




```
Get a HISTOGRAM section for histogram hpath.

The HISTOGRAM section is only available in a few analyses.

Parameters
----------
hpath : str
    Histogram path. Must have the form /AnalysisID/HistogramID .

See also
--------
:meth:`getSection`
```


### function isEndMarker

```python
def isEndMarker(
    self self,
    line line,
    blockname blockname
)
```


### function isComment

```python
def isComment(
    self self,
    line line
)
```


### function updateHistoHeaders

```python
def updateHistoHeaders(
    self self,
    hist hist
)
```


### function getSection

```python
def getSection(
    self self,
    section section,
    hpath hpath
)
```




```
Get a section for a histogram from a .plot file.

Parameters
----------
section : ('PLOT'|'SPECIAL'|'HISTOGRAM')
    The section that should be extracted.
hpath : str
    The histogram path, i.e. /AnalysisID/HistogramID .

TODO:
 * Caching! The result of the lookup is not cached so every call requires a file to be searched for and opened.
```


### function getSections

```python
def getSections(
    self self,
    sections sections,
    hpath hpath
)
```




```
Get all sections for a histogram from a .plot file.

Parameters
----------
section : ('SPECIAL')
    The section that should be extracted. Only Specials allowed to occur multiple times.
hpath : str
    The histogram path, i.e. /AnalysisID/HistogramID .

TODO:
 * Caching! The result of the lookup is not cached so every call requires a file to be searched for and opened.
```


### function getHeaders

```python
def getHeaders(
    self self,
    hpath hpath
)
```




```
Get the plot headers for histogram hpath.

This returns the PLOT section.

Parameters
----------
hpath : str
    The histogram path, i.e. /AnalysisID/HistogramID .

Returns
-------
plot_section : dict
    The dictionary usually contains the 'Title', 'XLabel' and
    'YLabel' properties of the respective plot.

See also
--------
:meth:`getSection`
```


### function getSpecial

```python
def getSpecial(
    self self,
    hpath hpath
)
```




```
Get a SPECIAL section for histogram hpath.

The SPECIAL section is only available in a few analyses.

Parameters
----------
hpath : str
    Histogram path. Must have the form /AnalysisID/HistogramID .

See also
--------
:meth:`getSection`
```


### function getSpecials

```python
def getSpecials(
    self self,
    hpath hpath
)
```




```
Get all SPECIAL sections for histogram hpath.

The SPECIAL section is only available in a few analyses.

Parameters
----------
hpath : str
    Histogram path. Must have the form /AnalysisID/HistogramID .

See also
--------
:meth:`getSections`
```


### function getHistogramOptions

```python
def getHistogramOptions(
    self self,
    hpath hpath
)
```




```
Get a HISTOGRAM section for histogram hpath.

The HISTOGRAM section is only available in a few analyses.

Parameters
----------
hpath : str
    Histogram path. Must have the form /AnalysisID/HistogramID .

See also
--------
:meth:`getSection`
```


### function isEndMarker

```python
def isEndMarker(
    self self,
    line line,
    blockname blockname
)
```


### function isComment

```python
def isComment(
    self self,
    line line
)
```


### function updateHistoHeaders

```python
def updateHistoHeaders(
    self self,
    hist hist
)
```


## Public Attributes Documentation

### variable pat_begin_block

```python
static pat_begin_block =  re.compile(r'^(#*\s*)?BEGIN (\w+) ?(\S+)?');
```


### variable pat_begin_name_block

```python
static pat_begin_name_block =  re.compile(r'^(#*\s*)?BEGIN (\w+) ?(\S+)? ?(\w+)?');
```


### variable pat_end_block

```python
static pat_end_block =    re.compile(r'^(#*\s*)?END (\w+)');
```


### variable pat_comment

```python
static pat_comment =  re.compile(r'^\s*#|^\s*$');
```


### variable pat_property

```python
static pat_property =  re.compile(r'^(\w+?)\s*=\s*(.*)$');
```


### variable pat_property_opt

```python
static pat_property_opt =  re.compile('^ReplaceOption\[(\w+=\w+)\]=(.*)$');
```


### variable pat_path_property

```python
static pat_path_property =  re.compile(r'^(\S+?)::(\w+?)=(.*)$');
```


### variable pat_paths

```python
static dictionary pat_paths =  {};
```


### variable getPlot

```python
static def getPlot =  getHeaders;
```

Alias. 

### variable addfiles

```python
addfiles;
```


### variable plotpaths

```python
plotpaths;
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
