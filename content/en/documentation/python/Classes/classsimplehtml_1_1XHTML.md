---

title: 'class simplehtml::XHTML'

description: "[Documentation update required.]"

---

# simplehtml::XHTML



 [More...](#detailed-description)

Inherits from [simplehtml.HTML](/documentation/python/classes/classsimplehtml_1_1html/), object

Inherited by [simplehtml.XML](/documentation/python/classes/classsimplehtml_1_1xml/)

## Public Attributes

|                | Name           |
| -------------- | -------------- |
| | **[empty_elements](/documentation/python/classes/classsimplehtml_1_1xhtml/#variable-empty-elements)**  |

## Additional inherited members

**Public Functions inherited from [simplehtml.HTML](/documentation/python/classes/classsimplehtml_1_1html/)**

|                | Name           |
| -------------- | -------------- |
| def | **[text](/documentation/python/classes/classsimplehtml_1_1html/#function-text)**(self self, text text, escape escape =True) |
| def | **[raw_text](/documentation/python/classes/classsimplehtml_1_1html/#function-raw-text)**(self self, <a href="/documentation/python/classes/classsimplehtml_1_1html/#function-text">text</a> text) |

**Public Attributes inherited from [simplehtml.HTML](/documentation/python/classes/classsimplehtml_1_1html/)**

|                | Name           |
| -------------- | -------------- |
| | **[newline_default_on](/documentation/python/classes/classsimplehtml_1_1html/#variable-newline-default-on)**  |


## Detailed Description

```python
class simplehtml::XHTML;
```




```
Easily generate XHTML.
```

## Public Attributes Documentation

### variable empty_elements

```python
static empty_elements =  set('base meta link hr br param img area input col \
        colgroup basefont isindex frame'.split());
```


-------------------------------

Updated on 2022-08-07 at 20:46:08 +0100
